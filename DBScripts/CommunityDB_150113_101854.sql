-- Goal [pkg4x#ent1z]
create table `goal` (
   `oid`  integer  not null,
   `title`  varchar(255),
   `completion_date`  date,
  primary key (`oid`)
);


-- Goal_Community User [rel1z]
alter table `goal`  add column  `community_user_user_id`  integer;
alter table `goal`   add index fk_goal_community_user (`community_user_user_id`), add constraint fk_goal_community_user foreign key (`community_user_user_id`) references `community_user` (`oid`);


-- Goal_Action Type [rel2z]
create table `goal_action_type` (
   `goal_oid`  integer not null,
   `action_type_oid`  integer not null,
  primary key (`goal_oid`, `action_type_oid`)
);
alter table `goal_action_type`   add index fk_goal_action_type_goal (`goal_oid`), add constraint fk_goal_action_type_goal foreign key (`goal_oid`) references `goal` (`oid`);
alter table `goal_action_type`   add index fk_goal_action_type_action_typ (`action_type_oid`), add constraint fk_goal_action_type_action_typ foreign key (`action_type_oid`) references `action_type` (`oid`);



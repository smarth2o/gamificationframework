CREATE DATABASE  IF NOT EXISTS `community` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `community`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: community
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action_instance`
--

DROP TABLE IF EXISTS `action_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_instance` (
  `oid` int(11) NOT NULL,
  `executor` varchar(255) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `score` decimal(19,2) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `rank_oid` int(11) DEFAULT NULL,
  `action_type_oid` int(11) DEFAULT NULL,
  `object_key` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `idx_action_instance_rank` (`rank_oid`),
  KEY `idx_action_instance_action_typ` (`action_type_oid`),
  CONSTRAINT `fk_action_instance_action_type` FOREIGN KEY (`action_type_oid`) REFERENCES `action_type` (`oid`),
  CONSTRAINT `fk_action_instance_rank` FOREIGN KEY (`rank_oid`) REFERENCES `community_user` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_instance`
--

LOCK TABLES `action_instance` WRITE;
/*!40000 ALTER TABLE `action_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `action_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `action_instance_action_area_vi`
--

DROP TABLE IF EXISTS `action_instance_action_area_vi`;
/*!50001 DROP VIEW IF EXISTS `action_instance_action_area_vi`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `action_instance_action_area_vi` (
  `oid` tinyint NOT NULL,
  `der_attr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `action_instance_daily_vi`
--

DROP TABLE IF EXISTS `action_instance_daily_vi`;
/*!50001 DROP VIEW IF EXISTS `action_instance_daily_vi`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `action_instance_daily_vi` (
  `action_type_oid` tinyint NOT NULL,
  `date` tinyint NOT NULL,
  `daily_occurrence` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `action_instance_name_view`
--

DROP TABLE IF EXISTS `action_instance_name_view`;
/*!50001 DROP VIEW IF EXISTS `action_instance_name_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `action_instance_name_view` (
  `oid` tinyint NOT NULL,
  `der_attr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `action_type`
--

DROP TABLE IF EXISTS `action_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_type` (
  `oid` int(11) NOT NULL,
  `check_time_elapsed` tinyint(1) DEFAULT NULL,
  `milliseconds_time_elapsed` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `repeatable` tinyint(1) DEFAULT NULL,
  `score` decimal(19,2) DEFAULT NULL,
  `reputation` tinyint(1) DEFAULT NULL,
  `participation` tinyint(1) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `gamified_application_oid` int(11) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `thematic_area_oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `idx_action_type_gamified_appli` (`gamified_application_oid`),
  KEY `fk_action_type_thematic_area` (`thematic_area_oid`),
  CONSTRAINT `fk_action_type_thematic_area` FOREIGN KEY (`thematic_area_oid`) REFERENCES `thematic_area` (`oid`),
  CONSTRAINT `fk_action_type_gamified_applic` FOREIGN KEY (`gamified_application_oid`) REFERENCES `gamified_application` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_type`
--

LOCK TABLES `action_type` WRITE;
/*!40000 ALTER TABLE `action_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `action_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badge_action`
--

DROP TABLE IF EXISTS `badge_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badge_action` (
  `badge_type_oid` int(11) NOT NULL,
  `action_type_oid` int(11) NOT NULL,
  PRIMARY KEY (`badge_type_oid`,`action_type_oid`),
  KEY `idx_badge_action_badge_type` (`badge_type_oid`),
  KEY `idx_badge_action_action_type` (`action_type_oid`),
  CONSTRAINT `fk_badge_action_action_type` FOREIGN KEY (`action_type_oid`) REFERENCES `action_type` (`oid`),
  CONSTRAINT `fk_badge_action_badge_type` FOREIGN KEY (`badge_type_oid`) REFERENCES `badge_type` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badge_action`
--

LOCK TABLES `badge_action` WRITE;
/*!40000 ALTER TABLE `badge_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `badge_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badge_instance`
--

DROP TABLE IF EXISTS `badge_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badge_instance` (
  `oid` int(11) NOT NULL,
  `score` decimal(19,2) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rank_oid` int(11) DEFAULT NULL,
  `badge_type_oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `idx_badge_instance_rank` (`rank_oid`),
  KEY `idx_badge_instance_badge_type` (`badge_type_oid`),
  CONSTRAINT `fk_badge_instance_badge_type` FOREIGN KEY (`badge_type_oid`) REFERENCES `badge_type` (`oid`),
  CONSTRAINT `fk_badge_instance_rank` FOREIGN KEY (`rank_oid`) REFERENCES `community_user` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badge_instance`
--

LOCK TABLES `badge_instance` WRITE;
/*!40000 ALTER TABLE `badge_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `badge_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badge_type`
--

DROP TABLE IF EXISTS `badge_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badge_type` (
  `oid` int(11) NOT NULL,
  `area` varchar(255) DEFAULT NULL,
  `needed_score` decimal(19,2) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `hd_image` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `importance` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `checked_image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `hd_checked_image` varchar(255) DEFAULT NULL,
  `sort_number` int(11) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `image_2` varchar(255) DEFAULT NULL,
  `imageblob` longblob,
  `hd_image_2` varchar(255) DEFAULT NULL,
  `hd_imageblob` longblob,
  `checked_image_2` varchar(255) DEFAULT NULL,
  `checked_imageblob` longblob,
  `hd_checked_image_2` varchar(255) DEFAULT NULL,
  `hd_checked_imageblob` longblob,
  `thematic_area_oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `fk_badge_type_thematic_area` (`thematic_area_oid`),
  CONSTRAINT `fk_badge_type_thematic_area` FOREIGN KEY (`thematic_area_oid`) REFERENCES `thematic_area` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badge_type`
--

LOCK TABLES `badge_type` WRITE;
/*!40000 ALTER TABLE `badge_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `badge_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `badgeimportancebyuser`
--

DROP TABLE IF EXISTS `badgeimportancebyuser`;
/*!50001 DROP VIEW IF EXISTS `badgeimportancebyuser`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `badgeimportancebyuser` (
  `badge_instance` tinyint NOT NULL,
  `user` tinyint NOT NULL,
  `nickname_area` tinyint NOT NULL,
  `importance` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `badgetype_sortco`
--

DROP TABLE IF EXISTS `badgetype_sortco`;
/*!50001 DROP VIEW IF EXISTS `badgetype_sortco`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `badgetype_sortco` (
  `oid` tinyint NOT NULL,
  `der_attr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `bundle_data`
--

DROP TABLE IF EXISTS `bundle_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bundle_data` (
  `oid` int(11) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bundle_data`
--

LOCK TABLES `bundle_data` WRITE;
/*!40000 ALTER TABLE `bundle_data` DISABLE KEYS */;
INSERT INTO `bundle_data` (`oid`, `key`, `locale`, `message`) VALUES (1,'longDescription','en',NULL),(2,'publishingDate','en',''),(3,'YouGotShareFacebook','en','You got credits because you shared on Facebook '),(4,'Edit','es',NULL),(5,'PowerIndex.From','en','from'),(6,'CompareValidationRule.gteq.field','es','<li>the entered value should be greater than or equal to the value of ${otherFieldName} field</li>'),(7,'Instructions','it',''),(8,'Previous Activity','fr',NULL),(9,'Course Students Number','es',NULL),(10,'error.validation.exception','es','<li>The following exception occurred during form validation: {0}</li>'),(11,'encodedtitle','it',''),(12,'LikeValidationRule.endsWith.field','fr','<li>the entered value should end with the value of ${otherFieldName} field</li>'),(13,'go to homepage','it','Panoramica'),(14,'Add Level','es',''),(15,'Latest topics','it','Nuove Domande'),(17,'Type','es',NULL),(18,'activationDate','fr',NULL),(19,'minVersion','en',NULL),(20,'description (search engine)','es',NULL),(21,'BooleanValidation','fr','<li>the entered value must be a boolean (${pattern}) </li>'),(22,'XsdTypeValidationRule.total.digits','es','<li>the entered value must have at most ${digits} total digits</li>'),(23,'licenseServerOperatingSyste','es',NULL),(24,'Common Data','fr',NULL),(25,'HQ.ReputationTab','es','ReputaciÃ³n'),(26,'CertificateFinded','en',NULL),(27,'error.security','fr','A security exception occurred: {0}'),(28,'vote','it',''),(29,'HQ.UserDashboard.Private','en','My Dashboard'),(30,'HQ.ReputationTab','fr',NULL),(31,'value','it',''),(32,'Last Events','en',NULL),(34,'History Diagram','it',''),(35,'getting-start-link','fr',NULL),(36,'productid','es',NULL),(37,'editStatus','es',NULL),(38,'ModuleName','es',NULL),(39,'icon','en',NULL),(40,'CropImageUnit.noImage','es',NULL),(41,'User last downloads','en','User last downloads'),(42,'Positions','en',NULL),(44,'n.VoteUp','fr',NULL),(45,'YouGotSignUp','fr',NULL),(46,'Store Attachment','fr',NULL),(47,'user','en',NULL),(48,'All','fr',NULL),(49,'LinkParticipation','es',''),(50,'plugins','en',NULL),(51,'ManagementEvent','it',''),(53,'Areegeografiche','en',NULL),(54,'XsdTypeValidationRule.invalid.float','fr','<li>Invalid Float</li>'),(55,'Link11','es',NULL),(56,'Scroller.Of','es','of'),(57,'Area','it',''),(58,'descriptionTmp','es',NULL),(59,'LikeValidationRule.endsWith.field','en','<li>the entered value should end with the value of ${otherFieldName} field</li>'),(60,'TypeValidationRule.error','it','<li>il valore inserito deve essere nel formato ${pattern}</li>'),(61,'tempScreenshots','en',NULL),(62,'Manage Area','fr',NULL),(63,'Link206','es',NULL),(64,'getting-start','it','Come iniziare'),(65,'YouGotApproveAnswer','fr',NULL),(66,'isoCode','it',''),(67,'User last topics','fr',NULL),(68,'ManagementEvent','es',NULL),(69,'DateValidation','en','<li>the entered value should have the format ${pattern}</li>'),(70,'go-to-store','es','Ir al Store'),(71,'users','es','usuarios'),(72,'activeExplicit','it',''),(73,'HQ.UsersList.AnyArea','es','Eliminar filtro zona'),(74,'Overall Participation','fr',NULL),(75,'valideType.error','fr',NULL),(76,'Add Badge','es',NULL),(77,'HQ.TotalCredits','es','CrÃ©ditos total '),(78,'lastVersionUpdateTimestamp','fr',NULL),(79,'updateTimestamp','fr',NULL),(80,'checkCode','fr',NULL),(81,'Query.Next','es','next'),(82,'content','es',NULL),(83,'HierarchicalIndex.Previous','en','previous'),(84,'HQ.YourProfile.NoData','es','El usuario no proporcionÃ³ sus datos'),(85,'Badge Instance','fr',NULL),(86,'noItemsFound','es',NULL),(87,'username','es',''),(88,'GroupName','en',NULL),(89,'Importance','it',''),(90,'tag','fr',NULL),(91,'HQ.ErrorLogin','fr',NULL),(93,'Manage Level','en',''),(94,'Department','es',NULL),(95,'HQ.YourLastAction','es','Su Ãºltimas acciÃ³n'),(96,'HQ.store.vote','es',NULL),(97,'Chunks Text','es',NULL),(98,'community.goToDashboard','it','Vai alla Dashboard dell\'utente'),(99,'HQ.NotPublicProfile','en','Private Profile'),(100,'HQ.Message','es',NULL),(101,'datamodified','es',NULL),(103,'Edit Text','fr',NULL),(104,'invioEffettuato','fr',NULL),(105,'ValueLengthValidationRule.eq','en','<li>the entered value length must be ${length} characters</li>'),(106,'units','fr',NULL),(107,'Your last topics','en','Your last topics'),(108,'Badge Selected','en',''),(109,'moduleDomainName','es',NULL),(110,'View','es',NULL),(111,'Company','es',NULL),(112,'question','fr',NULL),(113,'accepted','en',''),(114,'invioEffettuato','es',NULL),(115,'IntegerValidation','fr','<li>the entered value must be an integer</li>'),(116,'HQ.UsersList.Users','es','Usuario y Ranking'),(117,'Areas','en',''),(118,'Set','fr',NULL),(119,'Update','it',''),(120,'CompareValidationRule.gt','it','<li>il valore inserito deve essere maggiore di ${value}</li>'),(121,'thumbnailblob','en',NULL),(122,'KB Action','es',NULL),(123,'Area of Site','es',''),(124,'PowerIndex.Previous','it','precedente'),(125,'Scroller.To','es','to'),(126,'PowerIndex.Jump','it','vai a'),(127,'Link49','es',NULL),(128,'HQ.UsersList.Profile','en','Profile'),(129,'YouGotsetData','en','You setted'),(131,'Query.To','fr','to'),(132,'oid2','es',NULL),(134,'Nickname','fr',NULL),(135,'Reputation','fr',NULL),(136,'Order','en',NULL),(138,'Store Badge','en','Store Badge'),(139,'XsdTypeValidationRule.invalid.pattern','es','<li>the entered value has an invalid format (${pattern})</li>'),(140,'Headquarter','es',NULL),(141,'MandatoryValidationRule.error','fr','<li>mandatory field</li>'),(142,'Certifications history','en','Certifications history'),(143,'Certifications','it','Certificazioni'),(144,'errors.footer','en','</ul>'),(145,'QueryUnit.wrongGrouping','en','All the displayed attributes must be used as grouping or aggregate function'),(146,'emptyUnitMessage','es','No Results'),(147,'Enable','it',''),(148,'JobStatus.JobGroup','es','Group'),(150,'Certification','fr',NULL),(151,'`key`words (search engine)','es',NULL),(152,'Add Level','it',''),(153,'Forum Badge Title','it',''),(154,'HierarchicalIndex.Jump','en','jump to'),(155,'HQ.NotPublicProfile','fr',NULL),(156,'Reputation','en','Reputation'),(157,'Active','en',''),(158,'Review2','fr',NULL),(159,'PowerIndex.NoItemsShownOf','en','no items shown of'),(160,'Teacher Name','it',''),(161,'Link24','es',NULL),(162,'ModuleName','it',''),(163,'Choose the Display Sorting','fr',NULL),(164,'Badge Selected','es',''),(166,'Serialtable','es',NULL),(167,'`key`','fr',NULL),(168,'Certifications history','es','Certificaciones ganadas'),(169,'Headquarter','fr',NULL),(170,'duration','en',NULL),(171,'Scrool Actions','en',NULL),(172,'Scroller.From','es','from'),(174,'Query.Previous','es','previous'),(175,'Link95','es',NULL),(176,'BirthDate','it',''),(177,'Add','fr',NULL),(180,'Store Attachment','it',''),(181,'Countrytable','en',NULL),(182,'History actions','en',NULL),(183,'tempIconblob','fr',NULL),(184,'City','en','City'),(185,'retries','en',NULL),(186,'confirmForceUpdate','it',''),(187,'Scroller.Jump','es','jump to'),(188,'LikeValidationRule.contains','es','<li>the entered value should contain ${value}</li>'),(189,'Teacher LastName','fr',NULL),(190,'Your last downloads','es','Sus Ãºltimos descargas'),(191,'XsdTypeValidationRule.fraction.digits','fr','<li>the entered value must have at most ${digits} fraction digits</li>'),(192,'Small Photo','es',NULL),(193,'Check private','en',NULL),(194,'Rank Participation Overall','es',''),(195,'certification','es',NULL),(196,'companypartner','es',NULL),(197,'family','es',NULL),(198,'User latest','es',''),(199,'units','en',NULL),(200,'Link1','es',NULL),(202,'Link5','es',NULL),(203,'DateValidation','it','<li>il valore inserito deve essere nel formato ${pattern}</li>'),(204,'LMS','fr',NULL),(205,'CompareValidationRule.lteq','en','<li>the entered value should be smaller than or equal to ${value}</li>'),(207,'multiselectionfield.deselectAll','it','Deseleziona tutti'),(208,'Show Pdf','fr',NULL),(209,'ActionForm2','it',''),(210,'BirthDate','es',NULL),(211,'DateValidation','fr','<li>the entered value should have the format ${pattern}</li>'),(212,'HQ.CertificateTab','en','Certificates'),(213,'confirmNoteDeletion','es',NULL),(214,'CertificateForm','it',''),(215,'Activationtable','it',''),(217,'thumbnailblob','fr',NULL),(218,'file','es',NULL),(219,'confirmForceUpdate','fr',NULL),(220,'YouGotShareTwitter','it','Hai ottenuto punti per aver condiviso su Twitter'),(221,'sendEmail','en',NULL),(222,'HQ.Back','fr',NULL),(223,'emptyUnitMessage','fr','No Results'),(224,'Creation Date','en',''),(225,'Create','fr',NULL),(226,'Link145','en',NULL),(227,'releaseType','it',''),(228,'Filter by region','es','Filtrar por regiÃ³n'),(229,'CompareValidationRule.eq','it','<li> il valore inserito deve essere uguale a ${value}</li>'),(230,'Query.Next','it','successivo'),(231,'Question','fr',NULL),(232,'Monthly Message','es',NULL),(233,'IntegerValidation','es','<li>the entered value must be an integer</li>'),(234,'HQ.YourProfile.Certificate','es',''),(235,'HQ.Credits','it','punti'),(236,'Geographical Area','es','Zona GeogrÃ¡fica'),(237,'note','it',''),(239,'YouGotPostSubscription','fr',NULL),(240,'EMailValidationRule.error','es','<li>invalid email address</li>'),(241,'YouGotPostSubscription','en','You got credits for your subscription'),(242,'serialNumber3','fr',NULL),(243,'CompareValidationRule.gteq.field','fr','<li>the entered value should be greater than or equal to the value of ${otherFieldName} field</li>'),(244,'ActionForm2','fr',NULL),(245,'Partecipation','es',NULL),(246,'universitycc','it',''),(247,'companycc','fr',NULL),(248,'Area Badge','en',''),(249,'YouGotPublishStore','es','Tienes crÃ©ditos porque publicaste'),(250,'JobStatus.JobId','en','ID'),(251,'Add Area','es',''),(252,'CompareValidationRule.lteq','es','<li>the entered value should be smaller than or equal to ${value}</li>'),(253,'Mobile','it',NULL),(255,'Certification Badge Title','es',NULL),(256,'YouGotAnswerVoteup','es','Tienes crÃ©ditos porque su respuesta fue votada hasta'),(257,'serialNumber','fr',NULL),(258,'HQ.YourProfile.Developer','es',''),(259,'HQ.Back','es',NULL),(261,'List of Actions','fr',NULL),(262,'generationDate','es',NULL),(263,'Partecipation','en','Partecipation Score'),(264,'YouGotReadingWiki','fr',NULL),(265,'DecimalValidation','fr','<li>the entered value must be a number</li>'),(266,'moduleDomainName','it',NULL),(267,'HQ.store.authorBy','es',NULL),(269,'author','fr',NULL),(270,'Rank Participation Monthly','it',''),(271,'HQ.ManageProfile','es',NULL),(272,'HQ.NotFromSite','en','You can\'t see this profile, go to the home page and login.'),(274,'Review','es',NULL),(275,'HQ.NicknameNull','it',NULL),(276,'text','fr',NULL),(277,'HQ.CreditsTab','en','Credits'),(278,'Country','it',NULL),(279,'supportedVersion','fr',NULL),(282,'Tag','fr',NULL),(283,'Community User','it',NULL),(284,'Search bar','es',NULL),(285,'Review2','it',NULL),(286,'MandatoryValidationRule.error','it','<li>campo obbligatorio</li>'),(287,'UserOf','en',NULL),(288,'error.validation.exception','it','<li>Si è verificata la seguente eccezioni durante la validazione della form: {0}</li>'),(289,'error.button.not.pressed','fr','<li>Please press a button for form submission</li>'),(290,'Update Badge','es',NULL),(291,'userId','it',''),(292,'Teacher Name','en',NULL),(293,'Scroller.First','fr','first'),(294,'Set','it',NULL),(295,'Certifications','es','Certificaciones'),(296,'HQ.HomePage','es',NULL),(297,'HQ.YourProfile.ProfileUnknown','es','Perfil Desconocido'),(299,'Query.Previous','en','previous'),(300,'releaseType','en',''),(301,'active','es',''),(303,'Type','fr',NULL),(304,'Latest topics','es','Ãšltimos temas'),(305,'User Data','it',''),(306,'HQ.YourProfile.Developer','it',''),(307,'Certification','es',NULL),(308,'configuration','fr',NULL),(309,'XsdTypeValidationRule.max.inclusive','fr','<li>the entered value should be smaller than or equal to ${value}</li>'),(310,'minVersion','it',NULL),(311,'LikeValidationRule.endsWith.field','it','<li>il valore inserito deve finire con il valore del campo ${otherFieldName}</li>'),(312,'Process.metadata.outdated','it',NULL),(313,'city','en',''),(314,'JobStatus.TriggerGroup','fr','Group'),(315,'Locale','it',NULL),(316,'Level of Certificate','it',''),(317,'NoWorkItemsFound','it',''),(318,'Scroller.NoItemsShownOf','en','no items shown of'),(319,'HQ.is','it','è'),(320,'Teacher Name','es',NULL),(321,'Rating','it',NULL),(322,'HQ.PersonalData','it',NULL),(323,'sortOrder','es',NULL),(324,'noNews','it',NULL),(325,'New Badge','it',NULL),(326,'JobStatus.JobGroup','it','Group'),(327,'productid','it',NULL),(328,'No results','en','No results found'),(329,'HQ.Message','en',''),(330,'`key`','it',NULL),(331,'Your last topics','fr',NULL),(332,'Sort Badges Area','es',NULL),(333,'Type of Action','en',''),(334,'JobStatus.TriggerGroup','it','Group'),(335,'Forum','fr',NULL),(336,'MostimportantCertificate','it',NULL),(337,'Delete All','it',NULL),(338,'Modify Level','it',NULL),(339,'iconblob','it',NULL),(340,'Big Photo','fr',NULL),(341,'HQ.YourProfile.Private','es','Su perfil es privado'),(342,'Store Attachment','en',''),(344,'HierarchicalIndex.To','it','a'),(345,'HQ.PersonalCertificates','es',NULL),(346,'JobStatus.TriggerDescription','es','Description'),(347,'HQ.YourProfile.NoDeveloper','en',''),(348,'Rank Participation Overall','fr',NULL),(349,'Scroller.NoItemsShownOf','it','Nessun elemento visualizzato su'),(350,'Confirm Finish','fr',NULL),(351,'User last topics','en','User last topics'),(352,'Sort Number','en',''),(353,'LowLevel','en',''),(354,'Query.Next','fr','next'),(355,'Region','en',''),(356,'articleoid','fr',NULL),(357,'List of Areas','en',''),(358,'Go to ranking','en','Go to Leaderboard'),(359,'view','es',NULL),(360,'YouGotApprovedAnswer','it','Hai ottenuto punti per l\'approvazione della tua risposta'),(361,'notNotesFound','es',NULL),(362,'XsdTypeValidationRule.max.exclusive','en','<li>the entered value should be smaller than ${value}</li>'),(363,'Company Email','fr',NULL),(364,'DemoUsers','it',''),(365,'PowerIndex.To','es','to'),(366,'tempDescription','en',''),(367,'HQ.YouAre','en','You are'),(368,'Latest Published Store Components','es','Ãšltimos Componentes en el Store'),(369,'JobStatus.JobGroup','en','Group'),(370,'tempScreenshotsblob','es',NULL),(371,'Public','es',NULL),(372,'Add Level','fr',NULL),(373,'YouGotAnswerVoteup','fr',NULL),(374,'Province','es',NULL),(375,'lastModified','es',NULL),(376,'Green Check','en',''),(377,'OID','en',''),(379,'Query.NoItemsShownOf','en','no items shown of'),(380,'XsdTypeValidationRule.invalid.decimal','it','<li>Numero non valido</li>'),(381,'Modify Actions','it',NULL),(382,'Authorization','it',NULL),(383,'Link','es',NULL),(384,'noProcessInstancesFound','es',NULL),(385,'`key`word','fr',NULL),(386,'Process.metadata.outdated','en',''),(387,'CertificationCRM','fr',NULL),(388,'averageVote','en',NULL),(389,'errors.footer','es','</ul>'),(390,'answerOf','en','The answer'),(391,'JobStatus.JobDescription','en','Description'),(392,'Modify Badge Area','es',NULL),(393,'CollectionValidationRule.notIn','it','<li>il valore inserito non deve essere uno tra: ${values}</li>'),(394,'maxVersion','it',''),(395,'selectionfield.noselection','it','Nessuna selezione'),(396,'plugins','es',NULL),(397,'HQ.CertificateTab','es',NULL),(398,'licenseServerOperatingSyste','fr',NULL),(400,'Partecipation','it',''),(401,'expirationDate','fr',NULL),(402,'go-to-lms','fr',NULL),(403,'tags','fr',NULL),(404,'Forum Badge','es','Forum Badge'),(405,'JobStatus.TriggerNextFireTimestamp','en','Next Fire Timestamp'),(406,'all actions','fr',NULL),(407,'units','es',NULL),(408,'Vat Number','en',NULL),(409,'LMS','it',''),(410,'HQ.UserDashboard','it',''),(411,'answerOf','es',NULL),(412,'Dashboard','it',''),(413,'community.goToDashboard','en','Go to User\'s Dashboard'),(414,'sampleProject','fr',NULL),(415,'HQ.CertificateTab','it',''),(416,'LikeValidationRule.notEndsWith.field','es','<li>the entered value should not end with the value of ${otherFieldName} field</li>'),(417,'HierarchicalIndex.Of','it','di'),(418,'Go to account','it','Vai al tuo Account'),(419,'XsdTypeValidationRule.min.exclusive','it','<li>il valore inserito deve essere maggiore di ${value}</li>'),(420,'fromPortal','es',NULL),(423,'error.check.minChecked','es','<li>check at least {1} items</li>'),(424,'Latest downloads','en','Latest Components'),(425,'XsdTypeValidationRule.invalid.enum','es','<li>Invalid enumeration value (${values})</li>'),(426,'Set the Icons','it',''),(427,'Public Profile','fr',NULL),(428,'Modify Level','es',''),(429,'Administration','es',NULL),(430,'updateTimestamp','es',NULL),(431,'ratingtag','fr',NULL),(433,'HQ.CreditsTab','it','Punti'),(434,'HQ.YourLastAction','en','Your last action'),(435,'licenseServerVersion','es',NULL),(436,'PowerIndex.Jump','en','jump to'),(437,'Forum Badge','en','Forum Badge'),(440,'CompareValidationRule.gteq.field','en','<li>the entered value should be greater than or equal to the value of ${otherFieldName} field</li>'),(441,'Forum Level','es','Nivel Forum'),(442,'HQ.NoForumActivity','fr',NULL),(443,'PowerIndex.Of','es','of'),(445,'Store Level','en','Store Level'),(446,'oid','it',''),(447,'language','en',NULL),(448,'TypeValidationRule.error','en','<li>the entered value should have the format ${pattern}</li>'),(449,'UserName','es',''),(450,'TimestampValidation','es','<li>the entered value should be in the format ${pattern}</li>'),(451,'Latest Published Learning Objects','fr',NULL),(452,'Link145','es',NULL),(453,'icon','it',''),(454,'editStatus','fr',NULL),(455,'nickname','en',NULL),(456,'store','it',''),(457,'Phone Number','fr',NULL),(458,'ToolVersion','es',NULL),(459,'Query.Last','fr','last'),(460,'City','fr',NULL),(461,'Store Attachment','es',NULL),(462,'HQ.UsersList.Users','it',''),(463,'area','es',''),(464,'Delete All','fr',NULL),(465,'Role','es',NULL),(466,'templates','en',NULL),(467,'Nickname','en','Nickname'),(468,'custom description','fr',NULL),(469,'LikeValidationRule.notEndsWith.field','fr','<li>the entered value should not end with the value of ${otherFieldName} field</li>'),(470,'YouGotShareTwitter','en','You got points for share on Twitter'),(471,'Community User','en',NULL),(473,'Sort Number','it',''),(474,'int','en','Only Integer Number'),(475,'licenseServerOperatingSyste','it',''),(476,'BooleanValidation','it','<li>il valore inserito deve essere di tipo booleano (${pattern}) </li>'),(477,'XsdTypeValidationRule.invalid.float','en','<li>Invalid Float</li>'),(478,'JobStatus.JobDescription','fr','Description'),(479,'error.check.maxChecked','es','<li>check at most {1} items</li>'),(480,'SortCombination','fr',NULL),(481,'CertificateFinded','fr',NULL),(482,'Labels Text','fr',NULL),(483,'YouGotAnswerApproved','fr',NULL),(484,'Locale','es',NULL),(485,'LinkParticipation','it',''),(486,'fromPortal','it',NULL),(487,'Action Type','fr',NULL),(488,'HierarchicalIndex.To','en','to'),(489,'Select a region','es','Seleccionar una regiÃ³n'),(490,'tempScreenshotsblob','en',NULL),(491,'QueryUnit.wrongGrouping','fr','All the displayed attributes must be used as grouping or aggregate function'),(492,'updateTimestamp','it',NULL),(493,'Company Type','it',NULL),(494,'MandatoryValidationRule.error','es','<li>mandatory field</li>'),(495,'HQ.YourProfile.ProfileUnknown','it',''),(496,'resourceType','it',NULL),(497,'int','es','Only Integer Number'),(498,'objectId','fr',NULL),(499,'Monthly Participation','fr',NULL),(500,'latest-badges','es','Ãšltimos Badge'),(501,'note','en',NULL),(502,'User overall position','es',''),(503,'answerOf','it','di'),(504,'Areas','fr',NULL),(505,'CollectionValidationRule.notIn.query','fr','<li>the entered value is included in the list of forbidden values</li>'),(506,'KB Action','it',''),(507,'Scroller.Last','en','last'),(508,'error.check.minChecked','it','<li>selezionare almeno {1} elementi</li>'),(509,'UserName','fr',NULL),(510,'Manage actions','it',NULL),(511,'Forum Actions','it',NULL),(512,'WROWNERID','en',NULL),(513,'Store Item','es',NULL),(514,'Instructions','es',NULL),(515,'Action Type','en',''),(516,'Most Important Badges','es',NULL),(517,'Company Certified','en',NULL),(518,'Website','en','Website'),(519,'HQ.HomePage','it',NULL),(520,'UserName','en',''),(521,'Company Name','it',NULL),(523,'name','fr',NULL),(524,'HQ.UserOf','it','di'),(525,'YouGotShareGooglePlus','es','Tienes crÃ©ditos cuando lo compartiste en Google+'),(526,'userPartner','fr',NULL),(527,'Description','fr',NULL),(528,'XsdTypeValidationRule.invalid.decimal','es','<li>Invalid Decimal</li>'),(529,'store','fr',NULL),(530,'LikeValidationRule.startsWith.field','es','<li>the entered value should begin with the value of ${otherFieldName} field</li>'),(531,'LikeValidationRule.startsWith.field','it','<li>il valore inserito deve iniziare con il valore del campo ${otherFieldName}</li>'),(532,'LikeValidationRule.endsWith','es','<li>the entered value should end with ${value}</li>'),(533,'notAttachmentsFound','es',NULL),(534,'text','it',NULL),(535,'languageCode','es',NULL),(536,'sequence','it',NULL),(537,'university','fr',NULL),(538,'GroupName','fr',NULL),(539,'errors.header','en','<ul>'),(540,'HQ.YourProfile.NoDeveloper','es',''),(542,'Needed Score','fr',NULL),(543,'User Data','es',NULL),(544,'HQ.View','it',NULL),(545,'Modify Badge','en',NULL),(546,'Disable','it',NULL),(547,'LikeValidationRule.startsWith','fr','<li>the entered value should begin with ${value}</li>'),(548,'SortCombination','en',NULL),(549,'YouGotSignUp','it','Hai completato la registrazione'),(550,'Link','it',NULL),(551,'YouGotCoursePassed','it','Hai ottenuto punti per aver passato il corso'),(552,'areasort','fr',NULL),(553,'error.empty','en','{0}'),(554,'CompareValidationRule.neq','fr','<li>the entered value should be different from ${value}</li>'),(555,'HQ.YourProfile.ProfileUnknown','fr',NULL),(556,'licenseServerVersion','en',NULL),(557,'longDescription','es',NULL),(558,'version','it',NULL),(559,'`key`word','es',NULL),(560,'tempName','en',NULL),(561,'longDescription','fr',NULL),(562,'YouGotShareFacebook','fr',NULL),(563,'userOid','es',''),(565,'JobStatus.JobDescription','it','Description'),(566,'go to homepage','fr',NULL),(567,'description','it',NULL),(568,'List of Levels','en',''),(569,'Action Instance','fr',NULL),(570,'Levels of the Area:','it',''),(571,'CompareValidationRule.lteq.field','it','<li>il valore inserito deve essere minore o uguale al valore del campo ${otherFieldName}</li>'),(572,'Private Message','fr',NULL),(573,'XsdTypeValidationRule.invalid.date','en','<li>Invalid Date</li>'),(574,'CheckedItemsValidationRule.eq','es','<li>${itemCount} items checked required</li>'),(575,'Flow38','es',NULL),(576,'Flow37','es',''),(577,'DemoUsers','fr',NULL),(578,'Bio','fr',NULL),(579,'Flow39','es',NULL),(582,'Latest Published Store Components','it','Nuovi Componenti Pubblicati'),(584,'store','en','Store'),(585,'Role','it',NULL),(586,'sequence','en',''),(587,'Mandatory3','es','Choose an area'),(588,'Geographical Area','it',NULL),(589,'Mandatory4','es','Insert a title'),(592,'Query.First','en','first'),(593,'templates','es',NULL),(596,'Mandatory5','es','Insert a needed score'),(597,'Mandatory6','es','Insert the importance'),(598,'Partner','en','Partner'),(599,'go to monthly','en','See Full Leaderboard'),(600,'HQ.slider.error','it',NULL),(601,'HQ.Of','it','di'),(602,'confirmAttachmentDeletion','en',''),(603,'HQ.RankingOverall','es','Clasifica General'),(604,'XsdTypeValidationRule.invalid.boolean','en','<li>Invalid Boolean</li>'),(605,'Certification Score','it',''),(606,'Common Data','es',NULL),(608,'universitycc','en',''),(609,'My certificate','es',NULL),(610,'Vat Number','it',NULL),(611,'answerdate','en',''),(612,'getting-start-link','es','Ir a la GuÃ­a de Usuario'),(613,'Public','en',''),(614,'HQ.UserDashboard.AllActions','it','Azioni totali'),(615,'HQ.UserDashboard ','es',NULL),(616,'YouGotLogin','en','You got credits for your login'),(617,'address','es',NULL),(618,'HQ.Credits','en','credits'),(619,'Query.NoItemsShownOf','fr','no items shown of'),(620,'descriptionTmp','it',NULL),(621,'Flow46','es',NULL),(623,'Disable','fr',NULL),(624,'LikeValidationRule.notContains.field','es','<li>the entered value should not contain the value of ${otherFieldName} field</li>'),(626,'confirmNoteDeletion','en',''),(628,'Flow42','es',NULL),(629,'Course Title','es',NULL),(632,'Flow41','es',NULL),(633,'Flow40','es',NULL),(634,'Modify','fr',NULL),(636,'valideType.error','en',''),(637,'LoginHQ','it',NULL),(638,'Confirm Finish','es',NULL),(639,'Store Badge','fr',NULL),(640,'Last Badge','it',''),(641,'Latest downloads','es','Ãšltimos Componentes'),(642,'Query.Jump','es','jump to'),(643,'Positions','fr',NULL),(644,'Resource Type','fr',NULL),(645,'Manage icons','it',NULL),(646,'City','it',NULL),(648,'YouGotShareTwitter','es','Tienes crÃ©ditos porque compartiste en Twitter'),(650,'Flow65','es',NULL),(651,'NotPublicProfile.Private','es',NULL),(652,'Modify Actions','en',''),(653,'multiselectionfield.deselectAll','en','Deselect All'),(654,'Flow64','es',NULL),(655,'Disable','en',''),(656,'templates','fr',NULL),(657,'History Certificates','en',''),(658,'Sort Number','fr',NULL),(659,'vote','en',''),(660,'Getting Started','fr',NULL),(661,'PowerIndex.Next','fr','next'),(662,'error.button.not.pressed','en','<li>Please press a button for form submission</li>'),(663,'Query.Of','en','of'),(664,'HQ.YourLastAction','it','Ultima azione'),(665,'TimeValidation','it','<li>il valore inserito deve essere nel formato ${pattern}</li>'),(666,'generationDate','en',''),(667,'Link','en',''),(668,'confirmAttachmentDeletion','it',NULL),(669,'templatesblob','it',NULL),(670,'error.security','es','A security exception occurred: {0}'),(671,'Choose the Display Sorting','es',NULL),(672,'ValueLengthValidationRule.neq','es','<li>the entered value length must be different from ${length} characters</li>'),(673,'Serialtable','it',NULL),(674,'HQ.YourBadges','it','Badge Acquisiti'),(676,'YouGotApprovedAnswer','en','You got points for your answer was approved'),(677,'YouGotMonthly','en','You got monthly points'),(678,'Group','it',NULL),(679,'no my actions','en','You never compute actions'),(680,'author','it',NULL),(681,'Answer','es',NULL),(682,'forum','it',NULL),(683,'Badge Instance','en',''),(684,'Dashboard','en',''),(685,'XsdTypeValidationRule.invalid.length','it','<li>il valore deve essere lungo ${length} caratteri</li>'),(686,'DemoUsers','en',''),(687,'sampleProjectblob','en',''),(688,'LikeValidationRule.startsWith.field','fr','<li>the entered value should begin with the value of ${otherFieldName} field</li>'),(689,'notAttachmentsFound','en',''),(690,'Welcome','es',NULL),(691,'Monthly Participation','es',''),(692,'Action Instance','it',''),(693,'HQ.YourProfile.Professional','es',''),(694,'Query.From','en','from'),(695,'Level Badge','fr',NULL),(696,'sortByLevel','es','Filtrar por Nivel de Badge'),(697,'User overall position','it',''),(698,'XsdTypeValidationRule.invalid.time','es','<li>Invalid Time</li>'),(699,'error.check.eqChecked','it','<li>selezionare {1} elementi</li>'),(700,'at','it',NULL),(701,'MostimportantCertificate','es',NULL),(702,'userPartner','es',''),(703,'Save','it',NULL),(704,'Language','fr',NULL),(705,'error.stacktrace','es','{0}'),(706,'Query.Jump','en','jump to'),(707,'activeExplicit','fr',NULL),(708,'by','it',NULL),(709,'HQ.HomePage','fr',NULL),(711,'YouGotDownloadStore','es','Tienes crÃ©ditos que han descargado'),(712,'author','es',NULL),(713,'Add area','fr',NULL),(714,'image','en',''),(715,'History actions','es',''),(716,'Province','fr',NULL),(717,'LikeValidationRule.notStartsWith','it','<li>il valore inserito non deve iniziare con ${value}</li>'),(718,'HQ.ErrorLogin','es',NULL),(720,'CompareValidationRule.gt.field','it','<li>il valore inserito deve essere maggiore al valore del campo ${otherFieldName}</li>'),(721,'HQ.YourProfile.NoData','it',''),(722,'Link49','en',''),(723,'confirmRollback','it',NULL),(724,'Store Badge Title','fr',NULL),(725,'YouGotCoursePassed','es','Tienes crÃ©ditos porque usted pasÃ³ el curso'),(726,'Manage Action','es',NULL),(729,'Generic Message','es',NULL),(730,'HQ.UsersList.AnyLevel','it','Reset'),(731,'Your last downloads','en','Your last downloads'),(732,'Chunks Text','it',NULL),(733,'CaptchaValidationRule.error','it',' <li>Il valore inserito non corrisponde a quello visualizzato</li>'),(734,'Set the Icons','fr',NULL),(735,'tempThumbnail','it',NULL),(736,'List of Badges','en',''),(737,'error.stacktrace','fr','{0}'),(739,'view','fr',NULL),(740,'multiselectionfield.selectAll','en','Select All'),(741,'Last Badge','fr',NULL),(742,'LikeValidationRule.contains.field','it','<li>il valore inserito deve contenere il valore del campo ${otherFieldName}</li>'),(743,'ordinal','es',NULL),(744,'Company Name','es',NULL),(745,'companycc','it',NULL),(746,'confirmNoteDeletion','it',NULL),(747,'HQ.UsersList.SearchField','fr',NULL),(748,'Modify Level','en',''),(749,'CompareValidationRule.lt','it','<li>il valore inserito deve essere minore di ${value}</li>'),(750,'timestamp2','es',NULL),(751,'LikeValidationRule.contains.field','en','<li>the entered value should contain the value of ${otherFieldName} field</li>'),(752,'SortCombination','it',NULL),(753,'YouGotAnswerVoteup','it','Hai ottenuto punti per il voto alla tua risposta'),(754,'HierarchicalIndex.NoItemsShownOf','en','no items shown of'),(755,'Edit','fr',NULL),(756,'Manage Level','fr',NULL),(757,'BigPhoto','it',NULL),(758,'Review','fr',NULL),(759,'YouGotMonthly','fr',NULL),(760,'Contact','it',NULL),(761,'Link95','en',''),(764,'Store Badge','it',NULL),(765,'No results','it',NULL),(766,'datamodified','en',''),(767,'History actions','it','Azioni compiute'),(768,'CertificationCRM','es',NULL),(769,'my badges','es',NULL),(770,'searchUser','fr',NULL),(771,'XsdTypeValidationRule.invalid.date','es','<li>Invalid Date</li>'),(772,'maxVersion','fr',NULL),(773,'DateValidation','es','<li>the entered value should have the format ${pattern}</li>'),(774,'XsdTypeValidationRule.positive.value','it','<li>Deve essere un valore intero positivo</li>'),(775,'HQ.YourProfile.Developer','en',''),(776,'PowerIndex.From','it','da'),(777,'LikeValidationRule.notContains.field','en','<li>the entered value should not contain the value of ${otherFieldName} field</li>'),(778,'YouGotApproveAnswer','it','Hai ottenuto punti per aver approvato una risposta'),(779,'tempIcon','en',''),(780,'Scroller.Next','fr','next'),(781,'CollectionValidationRule.in.query','en','<li>the entered value is not included in the list of valid values</li>'),(782,'HQ.UsersList.FilterArea','it','Filtra per compentenza'),(784,'Teacher','en',''),(786,'Partecipation down','en',''),(787,'Add area','it',''),(788,'YouGotApproveAnswer','es','Tienes crÃ©ditos porque usted ha aprobado la respuesta'),(789,'Monthly Partecipation down','fr',NULL),(790,'Industry Type','en',''),(791,'Vat Number','fr',NULL),(792,'YouGotReviewStore','en','You got points because you made a review'),(793,'Store Badge Title','en',''),(794,'CollectionValidationRule.in.query','it','<li>il valore inserito non è incluso nei valori permessi</li>'),(796,'Oid','en',''),(797,'TagStore','es',NULL),(798,'Chunks','es',NULL),(800,'Teacher LastName','it',''),(801,'resourceType','es',NULL),(802,'XsdTypeValidationRule.invalid.timestamp','en','<li>Invalid Timestamp</li>'),(803,'Chunks','fr',NULL),(804,'sortOrder','it',NULL),(805,'Certificates','en','Certificates'),(806,'FoundationYear','it',NULL),(807,'EMailValidationRule.error','it','<li>indirizzo email non valido</li>'),(809,'HQ.NotFromSite','it',NULL),(810,'no my actions','es',NULL),(812,'answer','es',NULL),(813,'tempIconblob','es',NULL),(814,'Fax','en',''),(815,'reissues','es',NULL),(816,'HQ.YouAre','es','Usted es'),(817,'HQ.PersonalData','fr',NULL),(818,'userPartnerCert','es',''),(819,'CertificationCRM','en',''),(820,'HD Checked Image','fr',NULL),(821,'Profile','it',''),(822,'EMailValidationRule.error','en','<li>invalid email address</li>'),(823,'error.check.eqChecked','es','<li>{1} items checked required</li>'),(824,'Link24','en',''),(825,'go-to-lms','it','Vai al Learn'),(826,'ToolVersion','it',NULL),(827,'searchUser','en','Filter by name'),(828,'HQ.UsersList.Profile','es','Perfil'),(829,'HQ.RankingOverall','fr',NULL),(830,'HQ.RankNoResults','en','No users in this rank with this properties.'),(831,'XsdTypeValidationRule.min.inclusive','it','<li>il valore inserito deve essere maggiore o uguale a ${value}</li>'),(832,'downloadCount','it',NULL),(833,'List of Levels','es',''),(834,'Manage icons','en',''),(835,'KB Badge Title','it',''),(836,'HQ.YourBadges','en','See your badges '),(837,'Update Badge','en',''),(839,'XsdTypeValidationRule.invalid.pattern','en','<li>the entered value has an invalid format (${pattern})</li>'),(841,'HQ.Answer','en','Answers'),(842,'level','it','livello'),(843,'Clear','it','Reset'),(844,'Scroller.From','fr','from'),(845,'Course Students Number','en',''),(846,'LikeValidationRule.notContains.field','fr','<li>the entered value should not contain the value of ${otherFieldName} field</li>'),(847,'HQ.UserDashboard.Private','fr',NULL),(848,'YouGotLogin','es','Tienes crÃ©ditos para su inicio de sesiÃ³n'),(849,'HD Checked Image','en',''),(850,'Action Area','it',''),(851,'objectId','en',''),(852,'XsdTypeValidationRule.total.digits','fr','<li>the entered value must have at most ${digits} total digits</li>'),(853,'Community Participation','it',''),(854,'Badge:','fr',NULL),(855,'Filter by badge type','es','Filtrar por tipo de badge'),(856,'HQ.UsersList.AnyLevel','fr',NULL),(858,'Query.To','en','to'),(859,'company','it',NULL),(860,'Link11','en',''),(861,'confirmForceUpdate','es',NULL),(862,'userPartner','it',NULL),(863,'XsdTypeValidationRule.min.length','it','<li>il valore deve essere lungo almeno ${length} caratteri</li>'),(864,'tempTags','fr',NULL),(865,'HD Image','fr',NULL),(866,'Scroller.Jump','en','jump to'),(867,'content','it',NULL),(868,'Area of Site','en',''),(869,'activeExplicit','en',''),(870,'levelCertificate','en',''),(871,'of','it','di'),(872,'Enable','es',NULL),(873,'HQ.ParticipationTab','it','Partecipazione'),(874,'nickname','it',NULL),(875,'tags','es',NULL),(876,'approved','es',NULL),(877,'CheckedItemsValidationRule.max','fr','<li>check at most ${itemCount} items</li>'),(878,'YouGotCoursePassed','en','You got points to have passed the course'),(879,'Administration','it',NULL),(880,'Areatomail','fr',NULL),(881,'Certification Badge Title','it',''),(882,'screenshots','fr',NULL),(883,'error.security','it','Impossibile accedere: {0}'),(884,'Big Photo','en',''),(885,'XsdTypeValidationRule.invalid.boolean','it','<li>Booleano non valido</li>'),(886,'tempName','es',NULL),(887,'User latest','en',''),(888,'Area Badge','es',''),(889,'Level of Certificate','en',''),(890,'Labels','fr',NULL),(891,'PowerIndex.First','it','primo'),(892,'XsdTypeValidationRule.invalid.timestamp','fr','<li>Invalid Timestamp</li>'),(893,'HQ.RankCompanyNoResults','es',NULL),(894,'MandatoryIfTrue.error','it',NULL),(895,'Credit history','en','Points and Rewards'),(896,'HQ.YourProfile.PrintPdf','fr',NULL),(897,'FloatValidation','en','<li>the entered value must be a float</li>'),(899,'Bio','es',NULL),(900,'Filter','fr',NULL),(901,'Teacher','it',NULL),(902,'XsdTypeValidationRule.invalid.date','fr','<li>Invalid Date</li>'),(903,'View','it',NULL),(904,'userPartnerCert','fr',NULL),(905,'Monthly Partecipation User','fr',NULL),(906,'confirmForceUpdate','en',''),(908,'YouGotAnswerTopic','es','Tienes crÃ©ditos por su respuesta a la pregunta'),(909,'CompareValidationRule.eq.field','en','<li>the entered value should be equal to the value of ${otherFieldName} field</li>'),(910,'User overall position','fr',NULL),(911,'Link','fr',NULL),(912,'confirmDeletion','it',NULL),(913,'error.empty','es','{0}'),(914,'universitycc','fr',NULL),(915,'Labels Text','es',NULL),(916,'reviewCount','en',NULL),(917,'rejectionReason','es',NULL),(918,'community.users','fr',NULL),(919,'no my actions','fr',NULL),(920,'area','fr',NULL),(921,'XsdTypeValidationRule.min.exclusive','es','<li>the entered value should be greater than ${value}</li>'),(922,'NotPublicProfile.Message','en','This user set his profile as Private'),(923,'HQ.store.vote','en','Rating'),(924,'XsdTypeValidationRule.invalid.length','en','<li>the entered value length must be ${length} characters</li>'),(925,'Article','fr',NULL),(926,'YouGotVoteStore','en','You got credits because you vote up'),(927,'Small Photo','en','Small Photo'),(928,'XsdTypeValidationRule.fraction.digits','it','<li>il valore deve avere al massimo ${digits} cifre decimali</li>'),(929,'History Diagram','en',NULL),(930,'Type of Action','es',NULL),(931,'expirationDate','en',''),(932,'approved','it',NULL),(933,'Store Badge Title','es',NULL),(934,'YouGotReadingWiki','it','Hai ottenuto punti per aver letto'),(935,'useridentifier','es',NULL),(937,'HQ.YourProfile.At','fr',NULL),(938,'contactoid','en',NULL),(939,'CompareValidationRule.lteq','fr','<li>the entered value should be smaller than or equal to ${value}</li>'),(940,'WROWNERID','es',NULL),(941,'Involved Actions Associated','it',NULL),(942,'XsdTypeValidationRule.invalid.pattern','fr','<li>the entered value has an invalid format (${pattern})</li>'),(943,'link','fr',NULL),(944,'XsdTypeValidationRule.positive.value','en','<li>Must be a positive value</li>'),(945,'getting-start-link','it','Come iniziare'),(946,'Forum Actions','fr',NULL),(947,'confirmRollback','es',NULL),(948,'HQ.YourBadges','fr',NULL),(949,'emptyUnitMessage','it','Nessun risultato'),(950,'serialNumber3','es',NULL),(951,'Modify Badge Level','it',''),(952,'LikeValidationRule.notStartsWith.field','es','<li>the entered value should not begin with the value of ${otherFieldName} field</li>'),(954,'HQ.UsersMonthly','fr',NULL),(955,'Text Chunk','fr',NULL),(956,'HQ.store.download','it',NULL),(957,'status','fr',NULL),(958,'Latest Badge','fr',NULL),(959,'styleProject','it',NULL),(960,'units','it',NULL),(961,'Needed Score','en',NULL),(962,'HQ.no.badges','en','No badges'),(963,'Most Important Badges','fr',NULL),(964,'LMS','en','Learn'),(965,'plugins','it',NULL),(966,'Importance','fr',NULL),(967,'New Area','es',NULL),(968,'voteCount','fr',NULL),(969,'serialNumber3','it',NULL),(970,'User Data','en',NULL),(971,'my badges','fr',NULL),(972,'nickname','fr',NULL),(973,'Forum Actions','es',''),(974,'Public Profile','it',''),(975,'getting-start-link','en','Go to Getting started'),(976,'questiondate','it',NULL),(977,'my badges','it',NULL),(978,'Enable','en',NULL),(979,'Go to account','es','Ir a Cuenta'),(980,'forum','fr',NULL),(981,'Company Type','fr',NULL),(982,'Action Instance','en',''),(983,'Public Profile','es',NULL),(984,'Modify Badge Level','en',''),(985,'Add area','es',''),(986,'HQ.NoStoreActivity','it',NULL),(987,'CheckedItemsValidationRule.min','fr','<li>check at least ${itemCount} items</li>'),(988,'PowerIndex.Last','en','last'),(989,'Go to ranking','es','Ir al Ranking'),(990,'HQ.YourProfile.Certificate','it',''),(991,'activeExplicit','es',NULL),(992,'Internal','it',NULL),(993,'emptyUnitMessage','en','No Results'),(994,'LoginHQ','fr',NULL),(995,'noProcessesFound','it',NULL),(996,'CompareValidationRule.lteq.field','en','<li>the entered value should be smaller than or equal to the value of ${otherFieldName} field</li>'),(997,'FormNotEmptyValidationRule.error','fr','<li>at least one field must be filled</li>'),(998,'tempDescription','fr',NULL),(999,'url','en',NULL),(1000,'editionoid','en',NULL),(1001,'Add Area','fr',NULL),(1002,'Answer','en','Answer'),(1003,'CropImageUnit.noImage','en',NULL),(1004,'my badges','en','My Badges'),(1005,'HQ.UserDashboard.Public','it','Dashboard'),(1006,'Partecipation','fr',NULL),(1007,'OID','es',NULL),(1008,'role','es',NULL),(1009,'HQ.CreditsTab','es','CrÃ©ditos'),(1010,'alias','fr',NULL),(1011,'BirthDate','en',''),(1012,'Webratio Activation','fr',NULL),(1013,'Logout','es',''),(1014,'Partner','fr',NULL),(1015,'BigPhoto','en',NULL),(1016,'Scroller.To','it','a'),(1017,'company','en',NULL),(1019,'Image','en',NULL),(1020,'Title','fr',NULL),(1021,'PowerIndex.From','es','from'),(1022,'QueryUnit.wrongExtAttributeCondition','en','One or more conditions are based on attributes outside the table space'),(1023,'All','it',NULL),(1024,'Latest Badge','es','Ãšltimos Badge'),(1025,'ValueLengthValidationRule.min','es','<li>the entered value length must be at least ${length} characters</li>'),(1026,'sortByLevel','en','Filter by Badge Level'),(1027,'New Level','es',''),(1028,'User','fr',NULL),(1029,'id','it',NULL),(1030,'Choose the Display Sorting','it',NULL),(1031,'Level','fr',NULL),(1032,'error.check.maxChecked','en','<li>check at most {1} items</li>'),(1033,'Badge:','it',NULL),(1035,'Country','en','Country'),(1036,'HQ.YourProfile.Professional','fr',NULL),(1037,'New Level','fr',NULL),(1038,'lms','en','LMS'),(1039,'Certificates','fr',NULL),(1040,'multiselectionfield.selectAll','it','Seleziona tutti'),(1041,'YouGotActiving','en','Activing'),(1042,'Your last downloads','fr',NULL),(1043,'CreditCardValidationRule.error','it','<li>numero di carta di credito non valido</li>'),(1044,'LikeValidationRule.contains','it','<li>il valore inserito deve contenere ${value}</li>'),(1045,'Type of Badge','fr',NULL),(1046,'Image','fr',NULL),(1048,'Module','en',NULL),(1049,'NoWorkItemsFound','fr',NULL),(1050,'serialNumber','es',NULL),(1051,'Chunks Text','fr',NULL),(1053,'Scroller.Last','es','last'),(1055,'HQ.YourProfile.At','it','presso'),(1056,'editionoid','fr',NULL),(1057,'area','en',''),(1058,'Modify Area','it',NULL),(1059,'Partecipation up','fr',NULL),(1060,'List of Actions','en',NULL),(1061,'error.check.minChecked','en','<li>check at least {1} items</li>'),(1062,'lastVersionUpdateTimestamp','it',''),(1063,'Scroller.Next','it','successivo'),(1064,'Big Photo','it',NULL),(1065,'oid2','it',NULL),(1066,'HQ.UserOf','es','Usuarios de'),(1067,'valideType.error','es',NULL),(1068,'timestamp','en',NULL),(1069,'HQ.UsersList.FilterLevel','en','Filter by minimum level'),(1070,'notNotesFound','en',NULL),(1072,'Certification Badge','es',NULL),(1073,'`key`word','it',NULL),(1074,'Filter','it',NULL),(1075,'HD Checked Image','it',NULL),(1076,'errors.footer','it','</ul>'),(1077,'HQ.rank.monthly','it','Top Users'),(1078,'HierarchicalIndex.Next','en','next'),(1079,'Nickname_Crm','es',NULL),(1080,'sortOrder','en',NULL),(1082,'All Reputation Actions','it',''),(1083,'HQ.YourProfile.Professor','fr',NULL),(1084,'confirmAttachmentDeletion','es',NULL),(1085,'HQ.UsersList.FilterLevel','fr',NULL),(1086,'HQ.UsersMonthly','es','Top users'),(1087,'voteCount','es',NULL),(1088,'tempIcon','fr',NULL),(1089,'tempThumbnailblob','fr',NULL),(1090,'language','es',NULL),(1091,'HQ.YourBadges','es','Ver Tus Badges'),(1092,'Text Chunk','en',NULL),(1093,'First Name','es',NULL),(1094,'YouBecome','es','Usted se convierte en'),(1095,'confirmCancelProcess','en',NULL),(1096,'Process.metadata.invalid','fr',NULL),(1097,'Manage Badge','en',NULL),(1098,'tempDescription','es',NULL),(1099,'XsdTypeValidationRule.negative.value','en','<li>Must be a negative value</li>'),(1100,'Manage Area','en',''),(1101,'view','it',NULL),(1102,'int','fr','Only Integer Number'),(1103,'GroupName','it',NULL),(1104,'Add','it',''),(1105,'date','it',NULL),(1106,'GroupName','es',NULL),(1107,'itemId','en',NULL),(1108,'HQ.UsersList.SearchField','it','Ricerca utente:'),(1109,'Module','fr',NULL),(1110,'HQ.store.update','fr',NULL),(1111,'styleProjectblob','it',NULL),(1112,'YouGotSignUp','en','You sign up'),(1113,'Latest downloads','fr',NULL),(1114,'Process.metadata.invalid','it',NULL),(1116,'levelCertificate','es',''),(1117,'HQ.UserDashboard.Private','it','My Dashboard'),(1118,'Twitter','es',NULL),(1119,'confirmDeletion','es',NULL),(1120,'image','fr',NULL),(1121,'CollectionValidationRule.in','it','<li>il valore inserito deve essere uno tra: ${values}</li>'),(1122,'iconblob','fr',NULL),(1123,'HQ.ErrorLogin','en',NULL),(1124,'deactivationDate','it',NULL),(1125,'CompareValidationRule.neq.field','it','<li>il valore inserito deve essere diverso dal valore del campo ${otherFieldName}</li>'),(1126,'icon','fr',NULL),(1127,'CompareValidationRule.neq.field','fr','<li>the entered value should be different from the value of ${otherFieldName} field</li>'),(1128,'ActionForm','en',''),(1129,'HierarchicalIndex.First','es','first'),(1130,'UserOf','fr',NULL),(1131,'HQ.no.certificates','es',NULL),(1133,'averageVote','it',NULL),(1134,'sampleProject','it',NULL),(1135,'university','en',NULL),(1136,'HD Image','es',NULL),(1137,'HQ.no.badges','fr',NULL),(1138,'CollectionValidationRule.in','en','<li>the entered value must be one of: ${values}</li>'),(1139,'XsdTypeValidationRule.invalid.decimal','fr','<li>Invalid Decimal</li>'),(1141,'ValueLengthValidationRule.eq','it','<li>il valore inserito deve essere lungo ${length} caratteri</li>'),(1142,'description (search engine)','en',''),(1143,'reissues','it',NULL),(1144,'country','fr',NULL),(1145,'Course Students Number','it',NULL),(1146,'note','es',NULL),(1147,'Name','fr',NULL),(1148,'User last downloads','es','Descargas de usuario'),(1149,'postedAnswer','fr',NULL),(1150,'XsdTypeValidationRule.negative.value','es','<li>Must be a negative value</li>'),(1151,'objectId','it',NULL),(1152,'community.goToDashboard','es','Ir al Dashboard del usuario'),(1153,'accepted','it',''),(1154,'N/D','en',''),(1155,'HQ.YourProfile.NoData','en','The User did not provide his data'),(1156,'LikeValidationRule.notStartsWith','fr','<li>the entered value should not begin with ${value}</li>'),(1157,'Certification Badge','fr',NULL),(1158,'retries','es',NULL),(1159,'Fax','es',NULL),(1160,'styleProjectblob','fr',NULL),(1161,'Query.NoItemsShownOf','it','Nessun elemento visualizzato su'),(1162,'ValueLengthValidationRule.neq','en','<li>the entered value length must be different from ${length} characters</li>'),(1163,'ValueLengthValidationRule.max','es','<li>the entered value length must be at most ${length} characters</li>'),(1164,'noProcessInstancesFound','en',''),(1165,'YouGotShareFacebook','es','Tienes crÃ©ditos cuando lo compartiste en Facebook'),(1166,'Query.Of','es','of'),(1167,'HQ.ReputationTab','it','Reputazione'),(1168,'Course Title','en',''),(1169,'HQ.store.download','en','Download'),(1170,'HQ.YourActions','en','Your Actions'),(1171,'Teacher','fr',NULL),(1172,'YouGotActivingClient','en','You setted partner profile'),(1174,'screenshotsblob','en',''),(1175,'Contact','es',NULL),(1176,'Manage icons','es',NULL),(1177,'Show Activity Details','fr',NULL),(1178,'HQ.ManageProfile','en','Go to account'),(1179,'time','es',NULL),(1180,'styleProjectblob','es',NULL),(1181,'Common Data','it',NULL),(1182,'HierarchicalIndex.Of','en','of'),(1183,'UserOf','es',''),(1184,'Latest Forum Topics','en','Latest Forum Topics'),(1185,'Monthly Form','fr',NULL),(1186,'HQ.RankNoResults','it','Nessun utente in classifica con queste proprietà'),(1187,'userId','fr',NULL),(1188,'List of Areas','es',''),(1189,'Private Message','en',''),(1191,'Community Participation','es',''),(1192,'Add Badge','fr',NULL),(1193,'sampleProject','es',NULL),(1194,'Store Item','en',''),(1195,'Province','it',NULL),(1197,'HQ.UsersList.GoTo','it','Lista Utenti'),(1198,'HierarchicalIndex.Jump','it','vai a'),(1199,'languageCode','it',NULL),(1200,'FloatValidation','it','<li>il valore inserito deve essere di tipo float</li>'),(1201,'CollectionValidationRule.in.query','es','<li>the entered value is not included in the list of valid values</li>'),(1202,'Go to participation','en','Go to Leaderboard'),(1203,'Participation','it','Partecipazione'),(1204,'Manage icons','fr',NULL),(1205,'oidquestion','fr',NULL),(1206,'Filter by badge level','it','Filtra per livello di badge'),(1207,'PowerIndex.Next','it','successivo'),(1208,'latest-badges','en','Latest Badges'),(1209,'errors.footer','fr','</ul>'),(1210,'Forum Badge','it',NULL),(1211,'YouGotReviewStore','es','Tienes crÃ©ditos porque hiciste una revisiÃ³n'),(1213,'generationDate','fr',NULL),(1214,'company','es',NULL),(1215,'voteCount','en',''),(1216,'maxVersion','es',NULL),(1217,'releaseNotes','fr',NULL),(1218,'PowerIndex.From','fr','from'),(1219,'levelsort','es',''),(1220,'DatesCertificates','it',NULL),(1221,'Small Photo','it',NULL),(1222,'error.check.eqChecked','fr','<li>{1} items checked required</li>'),(1223,'Search','it','Cerca'),(1224,'All','es',NULL),(1225,'LowLevel','it',''),(1226,'encodedtitle','en',''),(1227,'Search','es',NULL),(1228,'type','es',NULL),(1229,'CheckedItemsValidationRule.eq','fr','<li>${itemCount} items checked required</li>'),(1230,'New Badge','fr',NULL),(1231,'Link145','it',NULL),(1233,'HQ.Of','en','out of'),(1234,'ActionForm2','es',''),(1235,'ValueLengthValidationRule.neq','it','<li>il valore inserito non deve essere lungo ${length} caratteri</li>'),(1236,'TagStore','en',''),(1237,'Number Of Employees','es',''),(1238,'Levels of the Area:','en',''),(1239,'HQ.UsersList.Filter','it','Filtra per compentenza'),(1240,'Company Name','fr',NULL),(1241,'Partecipation up','it',NULL),(1243,'question','en',''),(1244,'confirmRollback','fr',NULL),(1245,'Province','en',''),(1247,'Bundle Data','it',NULL),(1248,'XsdTypeValidationRule.invalid.decimal','en','<li>Invalid Decimal</li>'),(1249,'KB Level','es','Nivel Learn'),(1250,'Company Certified','it',NULL),(1251,'articleoid','it',NULL),(1252,'Query.To','es','to'),(1253,'message','it',NULL),(1254,'n.VoteUp','en',''),(1255,'HQ.PersonalCredits','fr',NULL),(1256,'HierarchicalIndex.From','es','from'),(1257,'PDF Certificate','es',NULL),(1258,'Most Important Badges','it',NULL),(1259,'View','en',''),(1260,'close','es',NULL),(1261,'Language','it',NULL),(1262,'Your last topics','es','Sus Ãºltimos temas'),(1263,'contactoid','es',NULL),(1264,'JobStatus.TriggerNextFireTimestamp','fr','Next Fire Timestamp'),(1265,'CompareValidationRule.gteq','fr','<li>the entered value should be greater than or equal to ${value}</li>'),(1266,'HQ.YourProfile.Student','en','Student'),(1268,'Scroller.First','es','first'),(1269,'YouGotReviewStore','it','Hai ottenuto punti per la review'),(1270,'shortDescription','es',NULL),(1271,'City','es',NULL),(1272,'Your last readings','es','Sus Ãºltimas lecturas'),(1273,'Manage Area','es',''),(1274,'Go to monthly','en','Go to Leaderboard'),(1275,'YouGotShareFacebook','it','Hai ottenuto punti per aver condiviso su Facebook'),(1276,'id','fr',NULL),(1277,'n.User total overall','es',''),(1278,'BooleanValidation','es','<li>the entered value must be a boolean (${pattern}) </li>'),(1279,'Forum Badge Title','fr',NULL),(1280,'Resource Type','es',NULL),(1281,'fileblob','it',NULL),(1283,'Administration','fr',NULL),(1284,'Last Events','fr',NULL),(1285,'question','es',NULL),(1286,'Version','it',NULL),(1287,'serverTimestamp','es',NULL),(1288,'Authorization','es',NULL),(1289,'HQ.UsersList.FilterLevel','es','Filtrar por Nivel mÃ­nimo'),(1290,'List of Areas','it',''),(1291,'PowerIndex.Previous','fr','previous'),(1292,'type','fr',NULL),(1293,'templatesblob','es',NULL),(1294,'Certification Level','en',''),(1295,'YouGotReviewStore','fr',NULL),(1296,'Level','en','Level'),(1297,'oidquestion','it',''),(1298,'Installation','en',''),(1301,'Areatomail','es',''),(1302,'role','fr',NULL),(1304,'Link95','it',''),(1305,'all actions','en',''),(1306,'Areatomail','it',''),(1307,'CompareValidationRule.gt.field','en','<li>the entered value should be greater than the value of ${otherFieldName} field</li>'),(1308,'Query.From','fr','from'),(1309,'Participation Company','fr',NULL),(1310,'HQ.slider.error','es',NULL),(1311,'NoWorkItemsFound','es',NULL),(1312,'Search bar','fr',NULL),(1313,'Check private','it',''),(1314,'HQ.store.authorBy','it',''),(1315,'Newsletter','it',''),(1316,'oidtag','it',''),(1317,'Query.Previous','fr','previous'),(1318,'Public profile','fr',NULL),(1319,'Area of Certificate','en',''),(1320,'disabled','es',NULL),(1321,'Query.To','it','a'),(1322,'Profile','fr',NULL),(1323,'custom description','it',''),(1324,'Badge Type','es',NULL),(1325,'sampleProject','en',''),(1326,'selectionfield.world','es',''),(1327,'Sort Badges Area','fr',NULL),(1328,'reissues','en',''),(1329,'Monthly Message','fr',NULL),(1330,'Password','en','Password'),(1331,'voteCount','it',''),(1332,'Scroller.First','it','primo'),(1333,'Score','it',''),(1334,'Level Badge','en',''),(1335,'Latest Badge','en','Latest Badges'),(1336,'tempThumbnailblob','es',NULL),(1337,'alias','it',''),(1338,'n.User total overall','en',''),(1339,'tempScreenshotsblob','fr',NULL),(1340,'HierarchicalIndex.NoItemsShownOf','it','Nessun elemento visualizzato su'),(1341,'All Reputation Actions','fr',NULL),(1342,'Action Area','fr',NULL),(1343,'tempThumbnailblob','en',NULL),(1344,'Previous Activity','es',NULL),(1345,'Level of Certificate','fr',NULL),(1346,'YouGotLogin','fr',NULL),(1347,'PowerIndex.Of','fr','of'),(1348,'Phone Number','it',''),(1349,'username','en','Username'),(1350,'selectionfield.noselection','fr','No selection'),(1351,'oid2','en',NULL),(1352,'HQ.YourProfile.Professional','en','Professional'),(1353,'Link49','it',''),(1354,'DecimalValidation','es','<li>the entered value must be a number</li>'),(1355,'Go to monthly','it','Vai alla Classifica Generale'),(1356,'oidtag','fr',NULL),(1357,'N/D','fr',NULL),(1358,'Query.NoItemsShownOf','es','no items shown of'),(1359,'Group','es',NULL),(1360,'YouGotVoteStore','it','Hai ottenuto per aver messo \"Mi Piace\"'),(1361,'Last Name','es',NULL),(1362,'Monthly Partecipation User','it',''),(1363,'time','fr',NULL),(1364,'description','es',NULL),(1365,'All badges','fr',NULL),(1366,'oid2','fr',NULL),(1367,'searchUser','es','Filtrar por nombre, empresa o universidad'),(1368,'Tags','fr',NULL),(1369,'Chunks','it',''),(1370,'Certified','en',NULL),(1371,'YouGotCoursePassed','fr',NULL),(1372,'Activationtable','es',''),(1373,'objectId','es',NULL),(1374,'Certifications history','fr',NULL),(1375,'HQ.UserDashboard ','en','User Dashboard '),(1376,'Go to monthly','fr',NULL),(1377,'PDF Certificate','fr',NULL),(1378,'Oid','es',NULL),(1379,'serialNumberoid','it',''),(1380,'Delete All','en',NULL),(1381,'Certification Dictionary','es',NULL),(1382,'Modify Level','fr',NULL),(1383,'Order','es',NULL),(1384,'Green Check','fr',NULL),(1385,'HQ.ErrorLogin','it',''),(1387,'Phone Number','en',NULL),(1388,'confirmDeletion','en',NULL),(1389,'Address','it',''),(1390,'HQ.rank.overall','es','Clasifica General'),(1391,'oidquestion','es',NULL),(1392,'HQ.TotalCredits','it','Punti totali'),(1393,'Type of Action','it',''),(1394,'confirmCancelProcess','it',''),(1395,'CompareValidationRule.gteq','es','<li>the entered value should be greater than or equal to ${value}</li>'),(1396,'error.validation.exception','fr','<li>The following exception occurred during form validation: {0}</li>'),(1397,'Logo','fr',NULL),(1398,'HQ.UsersList.Search','fr',NULL),(1399,'CollectionValidationRule.in.query','fr','<li>the entered value is not included in the list of valid values</li>'),(1400,'Process.metadata.updated','it',''),(1401,'ValueLengthValidationRule.neq','fr','<li>the entered value length must be different from ${length} characters</li>'),(1402,'HQ.ParticipationTab','fr',NULL),(1403,'HQ.PersonalBadges','en','Personal Badges'),(1404,'YouGotActivingClient','it','Hai impostato il profilo partner'),(1405,'itemId','es',NULL),(1406,'averageVote','fr',NULL),(1407,'user','es',NULL),(1408,'HQ.UserDashboard ','it','Dashboard Utente'),(1409,'Manage Level','es',''),(1411,'KB Badge Title','fr',NULL),(1412,'Rating','es',NULL),(1413,'Type of Badge','es',''),(1414,'PowerIndex.To','fr','to'),(1417,'tempTags','es',NULL),(1418,'ordinal','en',NULL),(1419,'levelsort','en',''),(1420,'XsdTypeValidationRule.mandatory','en','<li>mandatory field</li>'),(1421,'SmallPhoto','it',''),(1422,'Partecipation down','es',NULL),(1423,'noItemsFound','en',NULL),(1424,'contactoid','it',''),(1425,'HQ.YourProfile.Professor','it',''),(1426,'Process.metadata.invalid','en',NULL),(1427,'Community Participation','en',''),(1428,'ratingtag','en',NULL),(1429,'JobStatus.TriggerDescription','it','Description'),(1430,'HierarchicalIndex.Jump','fr','jump to'),(1432,'Mandatory6','fr','Insert the importance'),(1433,'Mandatory5','fr','Insert a needed score'),(1434,'Mandatory4','fr','Insert a title'),(1435,'Mandatory3','fr','Choose an area'),(1438,'Rating','fr',NULL),(1439,'LikeValidationRule.startsWith','es','<li>the entered value should begin with ${value}</li>'),(1440,'Green Check','es',NULL),(1441,'Certification Badge Title','fr',NULL),(1442,'Position','es',NULL),(1443,'by','fr',NULL),(1444,'downloadCount','en','Download'),(1445,'FormNotEmptyValidationRule.error','en','<li>at least one field must be filled</li>'),(1446,'Certified','fr',NULL),(1447,'Query.Of','it','di'),(1448,'HQ.PersonalBadges','es',NULL),(1449,'Score','fr',NULL),(1450,'tempTags','en',NULL),(1451,'at','fr',NULL),(1452,'time','it',''),(1453,'companycc','en',''),(1454,'Scroller.NoItemsShownOf','es','no items shown of'),(1455,'CompareValidationRule.neq','it','<li>il valore inserito deve essere diverso da ${value}</li>'),(1456,'questiondate','es',NULL),(1457,'store','es',NULL),(1458,'plugins','fr',NULL),(1459,'XsdTypeValidationRule.fraction.digits','en','<li>the entered value must have at most ${digits} fraction digits</li>'),(1460,'tempName','fr',NULL),(1461,'MandatoryIfTrue.error','en',''),(1462,'userPartner','en','Partner'),(1463,'longDescription','it',''),(1464,'expirationDate','es',NULL),(1465,'HQ.rank.monthly','es','Top users'),(1466,'descriptionTmp','fr',NULL),(1467,'Review','it',''),(1468,'editionoid','es',NULL),(1469,'Newsletter','en',''),(1470,'Position','en','Go to Leaderboard'),(1471,'title','it',''),(1472,'Previous Activity','it',NULL),(1473,'HQ.rank.monthly','en','Top Users'),(1475,'Title','en',''),(1476,'TimeValidation','en','<li>the entered value should be in the format ${pattern}</li>'),(1477,'Process.metadata.outdated','es',NULL),(1479,'Store Level','it',NULL),(1480,'Version','en',''),(1481,'User monthly position','en',''),(1482,'CollectionValidationRule.notIn.query','en','<li>the entered value is included in the list of forbidden values</li>'),(1483,'Query.From','es','from'),(1484,'KB Badge','it','Livello Learn'),(1485,'Course Title','fr',NULL),(1486,'HQ.is','en','is'),(1487,'User last topics','es','Temas de usuario'),(1488,'Monthly Partecipation up','it',''),(1489,'fileblob','fr',NULL),(1491,'latest-badges','it','Ultimi Badges'),(1492,'HQ.GoToMyPosition','fr',NULL),(1493,'FoundationYear','en',''),(1494,'Store','es',NULL),(1497,'Store Level','es','Nivel Store'),(1498,'YouGotShareGooglePlus','en','You got points because you shared on Google+ '),(1500,'CompareValidationRule.gteq','en','<li>the entered value should be greater than or equal to ${value}</li>'),(1501,'New Area','it',NULL),(1502,'Activationtable','fr',NULL),(1503,'Email','es',NULL),(1504,'HQ.Answer','es',NULL),(1505,'XsdTypeValidationRule.max.exclusive','fr','<li>the entered value should be smaller than ${value}</li>'),(1507,'HD Image','en',''),(1508,'disabled','en',''),(1509,'Positions','it',NULL),(1510,'rejectionReason','fr',NULL),(1511,'Bio','it','Biografia'),(1512,'Public','it',NULL),(1513,'HQ.NicknameNull','es','Usted no se ha definido ningun nickname, para ver su perfil pÃºblico volver atrÃ¡s y editar sus datos.'),(1514,'HQ.store.authorBy','fr',NULL),(1515,'YouGotAnswerTopic','en','You got points for your answer at question'),(1516,'fromPortal','en',''),(1517,'First Name','fr',NULL),(1518,'Process.metadata.outdated','fr',NULL),(1519,'levelCertificate','fr',NULL),(1520,'First Name','en','First Name'),(1521,'Vat Number','es',NULL),(1522,'YouGotQuestionVoteup','es','Tienes puntos porque su pregunta fue votada hasta'),(1523,'Mobile','fr',NULL),(1524,'Positions','es',NULL),(1525,'Scroller.Of','it','di'),(1526,'CompareValidationRule.eq','fr','<li>the entered value should be equal to ${value}</li>'),(1527,'city','es',NULL),(1528,'Link145','fr',NULL),(1530,'YouGotDownloadStore','en','You got points to have downloaded '),(1531,'thumbnailblob','it',NULL),(1532,'Query.Of','fr','of'),(1533,'answerdate','fr',NULL),(1534,'Modify Badge Level','es',''),(1535,'KB Action','fr',NULL),(1536,'Level','it','Livello'),(1537,'configuration','it',NULL),(1538,'datamodified','fr',NULL),(1540,'All badges','es','Ver el Ranking completo '),(1541,'tag','es',NULL),(1542,'Postal Code','it',NULL),(1543,'Authorization','fr',NULL),(1544,'Welcome','en','Welcome'),(1545,'imageChecked','es',NULL),(1546,'HQ.UsersList.FilterArea','fr',NULL),(1547,'Rank Participation Monthly','en',''),(1548,'YouGotShareTwitter','fr',NULL),(1549,'NotPublicProfile.Private','it',NULL),(1550,'XsdTypeValidationRule.max.length','fr','<li>the entered value length must be at most ${length} characters</li>'),(1551,'Student','it',NULL),(1552,'HQ.YourProfile.Student','fr',NULL),(1553,'level','es','nivel'),(1554,'Email','en',''),(1555,'List of Actions','it',NULL),(1556,'KB Badge Title','en',''),(1557,'Query.Next','en','next'),(1558,'HQ.NotPublicProfile','es',''),(1559,'Forum Level','fr',NULL),(1560,'LoginHQ','es',NULL),(1562,'CompareValidationRule.eq.field','fr','<li>the entered value should be equal to the value of ${otherFieldName} field</li>'),(1563,'Partecipation up','en',''),(1564,'tags','en',''),(1565,'Newsletter','fr',NULL),(1566,'User last topics','it','Ultime Domande utente'),(1567,'HQ.YourProfile.Public','es','Su perfil es pÃºblico'),(1568,'Pdf','es',NULL),(1569,'BooleanValidation','en','<li>the entered value must be a boolean (${pattern}) </li>'),(1570,'Company','it',NULL),(1571,'sortOrder','fr',NULL),(1572,'Postal Code','es',NULL),(1573,'Company Type','es',NULL),(1574,'userPartnerCert','en','Partner certified'),(1575,'Flow64','fr',NULL),(1576,'oidtag','es',NULL),(1577,'Flow65','fr',NULL),(1578,'LikeValidationRule.notEndsWith.field','en','<li>the entered value should not end with the value of ${otherFieldName} field</li>'),(1579,'error.stacktrace','en','{0}'),(1580,'PowerIndex.Previous','es','previous'),(1581,'HQ.store.update','en','Updated'),(1582,'HQ.NotFromSite','fr',NULL),(1583,'XsdTypeValidationRule.positive.value','fr','<li>Must be a positive value</li>'),(1584,'errors.header','fr','<ul>'),(1585,'ManagementEvent','fr',NULL),(1586,'role','en',''),(1587,'Date','es',NULL),(1588,'Forum Level','en','Forum Level'),(1589,'userPartnerCert','it',NULL),(1590,'PDF Certificate','it',NULL),(1591,'companypartner','fr',NULL),(1592,'Back','fr',NULL),(1593,'Pdf','it',NULL),(1594,'XsdTypeValidationRule.max.length','it','<li>il valore deve essere lungo al massimo ${length} caratteri</li>'),(1595,'My certificate','fr',NULL),(1596,'Administration','en',''),(1597,'searchUser','it','Filtra per nome, azienda o università'),(1598,'Certification','it','Certificazioni'),(1599,'HQ.YourProfile.Work','fr',NULL),(1600,'History certificate','it','Certificazioni acquisite'),(1601,'CompareValidationRule.neq','en','<li>the entered value should be different from ${value}</li>'),(1602,'close','en',''),(1603,'Flow42','fr',NULL),(1605,'Flow40','fr',NULL),(1606,'Flow41','fr',NULL),(1607,'Select a region','fr',NULL),(1608,'Tag','en',''),(1609,'XsdTypeValidationRule.invalid.integer','es','<li>Invalid Integer</li>'),(1611,'Badge Instance','es',NULL),(1613,'Flow46','fr',NULL),(1616,'Link11','it',NULL),(1617,'Serialtable','en',''),(1618,'CompareValidationRule.gt','fr','<li>the entered value should be greater than ${value}</li>'),(1619,'of','fr',NULL),(1621,'ActionForm','es',''),(1622,'lastModified','fr',NULL),(1623,'sortByLevel','it',NULL),(1624,'Badge:','en',''),(1625,'Logo','es',''),(1626,'KB Action','en',''),(1627,'message','es',NULL),(1628,'WROWNERID','it',NULL),(1629,'HQ.YourProfile.Work','it','Lavora'),(1630,'Add Badge','en',''),(1631,'tag','en',''),(1632,'Student','en',''),(1633,'Flow38','fr',NULL),(1634,'level','en',''),(1635,'Flow37','fr',NULL),(1636,'HierarchicalIndex.Next','fr','next'),(1637,'Flow39','fr',NULL),(1638,'Link24','it',NULL),(1639,'Password','es',NULL),(1640,'Department','en',''),(1641,'postedAnswer','en','posted an answer'),(1642,'List of Levels','fr',NULL),(1643,'User monthly position','it',''),(1644,'Scroller.NoItemsShownOf','fr','no items shown of'),(1645,'YouGotAnswerVoteup','en','You got points because your answer was voted up'),(1646,'Participation Company','it',''),(1647,'NotPublicProfile.Private','en','Private Profile'),(1648,'useridentifier','it',''),(1649,'ratingtag','it',NULL),(1650,'Text Chunk','es',NULL),(1651,'Update','en',''),(1652,'MaxLevel','es',''),(1653,'XsdTypeValidationRule.max.inclusive','en','<li>the entered value should be smaller than or equal to ${value}</li>'),(1654,'CertificateFinded','it',NULL),(1655,'Public profile','en',''),(1656,'YouGotAnswerApproved','it','Hai ottenuto punti per l\'approvazione della risposta'),(1657,'version','en',''),(1658,'Scroller.Last','fr','last'),(1659,'tempThumbnail','fr',NULL),(1660,'List of Badges','it',NULL),(1661,'deactivationDate','en',''),(1662,'Monthly Form','it',''),(1663,'XsdTypeValidationRule.invalid.float','es','<li>Invalid Float</li>'),(1664,'congratulations','fr',NULL),(1665,'CompareValidationRule.lt','fr','<li>the entered value should be smaller than ${value}</li>'),(1666,'HQ.YourProfile.PrintPdf','en','Download your certificate'),(1667,'Timestamp','en',''),(1668,'Badge Instance','it',NULL),(1669,'User monthly position','es',''),(1670,'HQ.YourProfile.Student','it',NULL),(1671,'type','en',''),(1672,'Last Events','es',NULL),(1673,'Email','it',NULL),(1674,'Text Chunk','it',NULL),(1675,'Edit labels','es',NULL),(1676,'serverTimestamp','fr',NULL),(1677,'go to homepage','es','Homepage'),(1678,'HQ.NoForumActivity','es','No Forum actividades '),(1679,'XsdTypeValidationRule.negative.value','it','<li>Deve essere un valore intero negativo</li>'),(1680,'latest-badges','fr',NULL),(1681,'shortDescription','fr',NULL),(1682,'activationDate','es',''),(1683,'congratulations','es',NULL),(1684,'Certifications','fr',NULL),(1685,'Filter by badge type','it','Filtra per tipo di badge'),(1686,'Search','en','Search'),(1687,'PowerIndex.Next','en','next'),(1688,'Position','fr',NULL),(1689,'Rank Participation Monthly','fr',NULL),(1690,'Edit Text','it',NULL),(1691,'minVersion','es',NULL),(1692,'CheckedItemsValidationRule.min','es','<li>check at least ${itemCount} items</li>'),(1693,'Filter by region','it','Filtra per regione'),(1694,'XsdTypeValidationRule.invalid.enum','en','<li>Invalid enumeration value (${values})</li>'),(1695,'Query.Last','en','last'),(1696,'Clear','en','Clear'),(1697,'LikeValidationRule.contains.field','es','<li>the entered value should contain the value of ${otherFieldName} field</li>'),(1698,'Link11','fr',NULL),(1699,'List of Badges','fr',NULL),(1700,'by','es',NULL),(1701,'Order','it',NULL),(1702,'userId','en',NULL),(1703,'visibility','es',NULL),(1704,'Select a region','en','Select a country'),(1705,'Monthly Participation','en',''),(1706,'Manage Action','fr',NULL),(1707,'MandatoryIfTrue.error','fr',NULL),(1708,'Geographical Area','en','Geographical Area'),(1709,'releaseType','fr',NULL),(1710,'HQ.Message','fr',NULL),(1711,'Link24','fr',NULL),(1712,'Locale','en',NULL),(1713,'CompareValidationRule.neq.field','en','<li>the entered value should be different from the value of ${otherFieldName} field</li>'),(1714,'MaxLevel','en',''),(1715,'Edit Text','es',NULL),(1716,'HQ.Back','en','Back'),(1717,'HQ.MyDashboard ','it','My Dashboard'),(1718,'sendEmail','fr',NULL),(1719,'Certifications','en','Certifications'),(1720,'CompareValidationRule.lteq.field','fr','<li>the entered value should be smaller than or equal to the value of ${otherFieldName} field</li>'),(1721,'ValueLengthValidationRule.max','fr','<li>the entered value length must be at most ${length} characters</li>'),(1722,'deactivationDate','es',NULL),(1723,'HQ.ReputationTab','en','Reputation'),(1724,'n.User total overall','it',''),(1725,'notNotesFound','fr',NULL),(1726,'Badge Type','fr',NULL),(1727,'HQ.rank.monthly','fr',NULL),(1728,'XsdTypeValidationRule.invalid.pattern','it','<li>formato invalido (${pattern})</li>'),(1729,'Tags','it',NULL),(1730,'error.security','en','A security exception occurred: {0}'),(1731,'HQ.YouAre','fr',NULL),(1732,'XsdTypeValidationRule.invalid.time','fr','<li>Invalid Time</li>'),(1733,'Manage Level','it',''),(1734,'HierarchicalIndex.From','fr','from'),(1735,'New Area','en',''),(1736,'Generic Message','fr',NULL),(1737,'shortDescription','en',NULL),(1738,'Link49','fr',NULL),(1739,'Store Item','it',NULL),(1740,'status','es',NULL),(1741,'Certification Score','fr',NULL),(1742,'Modify Badge Area','it',NULL),(1743,'Monthly Message','en',''),(1744,'city','it',NULL),(1745,'HQ.UsersList.Search','en','Search'),(1746,'Checked Image','it',NULL),(1747,'Certification Dictionary','it',''),(1748,'LikeValidationRule.endsWith.field','es','<li>the entered value should end with the value of ${otherFieldName} field</li>'),(1749,'User last readings','en','User last readings'),(1750,'CompareValidationRule.lt.field','it','<li>il valore inserito deve essere minore al valore del campo ${otherFieldName}</li>'),(1751,'encodedtitle','es',NULL),(1752,'SecondMenuLevel','it',NULL),(1753,'TimeValidation','es','<li>the entered value should be in the format ${pattern}</li>'),(1754,'HQ.UserDashboard.AllActions','fr',NULL),(1755,'HQ.RankCompanyNoResults','fr',NULL),(1756,'PowerIndex.NoItemsShownOf','it','Nessun elemento visualizzato su'),(1757,'HQ.Back','it',NULL),(1758,'Webratio Activation','es',NULL),(1759,'LikeValidationRule.startsWith','en','<li>the entered value should begin with ${value}</li>'),(1760,'CertificateFinded','es',NULL),(1761,'LikeValidationRule.notEndsWith','fr','<li>the entered value should not end with ${value}</li>'),(1762,'Update Badge','fr',NULL),(1763,'at','es',NULL),(1765,'LikeValidationRule.notStartsWith.field','fr','<li>the entered value should not begin with the value of ${otherFieldName} field</li>'),(1766,'Time','it',NULL),(1767,'LowLevel','es',''),(1768,'Logo','en',''),(1769,'duration','fr',NULL),(1770,'Back','es',NULL),(1771,'HQ.UsersList.SearchField','en','Search User:'),(1772,'selectionfield.world','it',NULL),(1773,'TagStore','it',NULL),(1774,'HQ.UsersList.Users','en','Leaderboard'),(1775,'My certificate','en',''),(1776,'Level of Certificate','es',''),(1777,'description','en',''),(1778,'HQ.MyDashboard ','en','My Dashboard'),(1779,'Partecipation down','it',NULL),(1780,'HQ.store.download','es',NULL),(1781,'HQ.UsersList.SearchField','es','Buscar Usuario:'),(1782,'Checked Image','en',''),(1783,'Certification Dictionary','en',''),(1784,'Choose the Display Sorting','en',''),(1785,'fromPortal','fr',NULL),(1786,'version','es',NULL),(1787,'HQ.UsersList.Search','es','Buscar'),(1788,'LikeValidationRule.startsWith','it','<li>il valore inserito deve iniziare con ${value}</li>'),(1789,'HierarchicalIndex.Last','it','ultimo'),(1790,'YouGotPublishStore','it','Hai ottenuto punti per la pubblicazione'),(1791,'releaseType','es',NULL),(1792,'HQ.slider.error','en',''),(1793,'tempIconblob','it',NULL),(1794,'QueryUnit.noDiplayAtrributes','en','No selected display attributes'),(1795,'question','it',NULL),(1796,'screenshotsblob','es',NULL),(1797,'History Certificates','it','Certificazioni acquisite'),(1798,'Scroller.First','en','first'),(1799,'WebRatio Certification','en',''),(1801,'Update Badge','it',NULL),(1802,'HQ.TotalCredits','en','Total points'),(1803,'userId','es',NULL),(1804,'HQ.UserOf','fr',NULL),(1805,'HQ.Credits','fr',NULL),(1806,'Badge:','es',NULL),(1807,'Region','it',NULL),(1808,'PowerIndex.Of','it','di'),(1809,'Monthly Partecipation down','en',''),(1810,'ModuleID','it',NULL),(1811,'CheckedItemsValidationRule.max','es','<li>check at most ${itemCount} items</li>'),(1812,'XsdTypeValidationRule.invalid.boolean','es','<li>Invalid Boolean</li>'),(1813,'Order','fr',NULL),(1814,'LikeValidationRule.contains','en','<li>the entered value should contain ${value}</li>'),(1815,'FormNotEmptyValidationRule.error','it','<li>almeno un campo deve essere riempito</li>'),(1816,'XsdTypeValidationRule.min.length','es','<li>the entered value length must be at least ${length} characters</li>'),(1817,'Show Pdf','en',''),(1818,'YouGotApprovedAnswer','fr',NULL),(1819,'Creation Date','fr',NULL),(1821,'moduleDomainName','en',''),(1822,'disabled','fr',NULL),(1823,'Certified','es',NULL),(1824,'All badges','it','Vai alla Classifica Completa'),(1825,'CheckedItemsValidationRule.eq','it','<li>${itemCount} elementi selezionati richiesti</li>'),(1826,'go-to-store','it','Vai allo Store'),(1827,'oid','en',''),(1828,'HQ.ManageProfile','it',''),(1829,'JobStatus.TriggerId','es','ID'),(1830,'Participation','es','ParticipaciÃ³n'),(1831,'Modify Badge Area','fr',NULL),(1832,'error.check.eqChecked','en','<li>{1} items checked required</li>'),(1833,'Region','fr',NULL),(1834,'Create','en',''),(1835,'go-to-lms','es','Ir al Learn'),(1836,'Getting Started','es','GuÃ­a de Usuario'),(1837,'Version','fr',NULL),(1838,'oid','fr',NULL),(1839,'PowerIndex.To','it','a'),(1840,'HQ.no.badges','it',NULL),(1841,'Department','it',NULL),(1842,'HQ.rank.overall','en','Overall Leaderboard'),(1843,'BigPhoto','fr',NULL),(1844,'Store','fr',NULL),(1845,'Locale','fr',NULL),(1846,'Position','it',NULL),(1847,'Set the Icons','es',NULL),(1848,'History Certificates','es',''),(1849,'HQ.YouAre','it','Sei'),(1851,'HQ.YourActions','fr',NULL),(1852,'Community User','es',NULL),(1853,'HQ.no.certificates','it',NULL),(1854,'Article','es',NULL),(1855,'Add Area','en',''),(1856,'Oid','fr',NULL),(1857,'Company Name','en','Company Name'),(1858,'QueryUnit.wrongGrouping','es','All the displayed attributes must be used as grouping or aggregate function'),(1859,'History Diagram','es',''),(1861,'Password','fr',NULL),(1862,'users','it','utenti'),(1863,'HQ.RankCompanyNoResults','en','No companies in this rank with this properties'),(1864,'CollectionValidationRule.notIn.query','es','<li>the entered value is included in the list of forbidden values</li>'),(1865,'Last Badge','en',''),(1866,'type','it',NULL),(1867,'value','es',NULL),(1868,'JobStatus.JobId','it','ID'),(1869,'Labels','es',NULL),(1870,'date','es',NULL),(1871,'name','it',NULL),(1872,'error.empty','it','{0}'),(1873,'time','en',''),(1874,'HQ.NotPublicProfile','it',''),(1875,'Go to participation','fr',NULL),(1876,'Countrytable','it',NULL),(1877,'visibility','it',NULL),(1878,'LikeValidationRule.notContains.field','it','<li>il valore inserito non deve contenere il valore del campo ${otherFieldName}</li>'),(1879,'levelsort','fr',NULL),(1880,'YouGotActivingClient','es','Usted setted perfil de socio'),(1881,'Group','en',''),(1882,'ActivityInstanceNotAvailable','en',''),(1883,'Common Data','en',''),(1884,'expirationDate','it',NULL),(1886,'answer','it',NULL),(1887,'ajax.computingRequest','fr','Computing request..'),(1888,'Query.Previous','it','precedente'),(1890,'HQ.UsersList.GoTo','en','List of Users'),(1891,'updateTimestamp','en',''),(1892,'XsdTypeValidationRule.invalid.float','it','<li>Numero non valido</li>'),(1893,'Webratio Activation','it',NULL),(1894,'PowerIndex.Jump','fr','jump to'),(1895,'YouGotShareGooglePlus','fr',NULL),(1896,'n.Answer','fr',NULL),(1897,'History badges','es',''),(1898,'YouGotQuestionVoteup','it','Hai ottenuto punti per il voto alla tua domanda'),(1899,'HierarchicalIndex.First','en','first'),(1900,'WROWNERID','fr',NULL),(1901,'HQ.Answer','fr',NULL),(1902,'RegularExpressionValidationRule.error','en','<li>the entered value is not valid</li>'),(1903,'DatesCertificates','en',''),(1904,'CompareValidationRule.lteq','it','<li>il valore inserito deve essere minore o uguale di ${value}</li>'),(1905,'Date','en','Date'),(1906,'NotPublicProfile.Private','fr',NULL),(1907,'WebSite','it','Sito Web'),(1908,'Type of Action','fr',NULL),(1909,'title','en',''),(1910,'thumbnail','en',''),(1911,'HQ.PersonalBadges','it',NULL),(1912,'Nickname','es',NULL),(1915,'CompareValidationRule.neq.field','es','<li>the entered value should be different from the value of ${otherFieldName} field</li>'),(1916,'n.Answer','it',NULL),(1917,'Contact','fr',NULL),(1918,'Rank Participation Overall','en',''),(1919,'Modify Badge Level','fr',NULL),(1920,'CreditCardValidationRule.error','en','<li>invalid credit card number</li>'),(1921,'List of Areas','fr',NULL),(1922,'areasort','es',''),(1923,'Resource Type','it',NULL),(1924,'ActionForm','fr',NULL),(1925,'HQ.YourProfile.Public','fr',NULL),(1926,'HQ.YourLastAction','fr',NULL),(1927,'visibility','en',''),(1928,'New Badge','es',NULL),(1929,'All Reputation Actions','es',''),(1930,'YouGotApproveAnswer','en','You got points because you have approved the answer'),(1931,'Date','it',NULL),(1932,'HQ.store.download','fr',NULL),(1933,'reviewCount','es',NULL),(1934,'of','es','de'),(1936,'User latest','it',''),(1937,'CreditCardValidationRule.error','es','<li>invalid credit card number</li>'),(1938,'Creation Date','es',NULL),(1939,'Latest topics','en','Latest Topics'),(1940,'Labels Text','it',NULL),(1941,'invioEffettuato','it',NULL),(1942,'Language','es',NULL),(1943,'articleOid','en',''),(1945,'link','en',''),(1946,'Certified','it',NULL),(1947,'lastVersionUpdateTimestamp','es',NULL),(1948,'Scroller.Next','en','next'),(1949,'Check private','es',NULL),(1950,'document','it',NULL),(1952,'description (search engine)','it','Cerca'),(1953,'oidquestion','en',''),(1954,'Last Events','it',''),(1955,'lms','es','LMS'),(1956,'tempScreenshots','it',NULL),(1957,'XsdTypeValidationRule.max.length','en','<li>the entered value length must be at most ${length} characters</li>'),(1958,'XsdTypeValidationRule.total.digits','it','<li>il valore deve avere al massimo ${digits} cifre</li>'),(1959,'screenshotsblob','fr',NULL),(1960,'Most Important Badges','en',''),(1961,'Address','en',''),(1963,'Time','en',''),(1964,'LoginHQ','en',''),(1965,'Your last downloads','it','Ultimi tuoi download'),(1966,'Latest Published Learning Objects','it','Nuovi Materiali'),(1968,'tempTags','it',NULL),(1969,'Store Badge','es',NULL),(1970,'HQ.YourProfile.Work','en','Works'),(1971,'Number Of Employees','it',NULL),(1972,'XsdTypeValidationRule.invalid.timestamp','it','<li>Timestamp non valido</li>'),(1973,'HQ.GoToMyPosition','it','Vai alla Mia Posizione'),(1974,'Overall Participation','en',''),(1975,'Action Instance','es',''),(1976,'Participation Company','en',''),(1977,'sampleProjectblob','it',NULL),(1978,'Forum Badge Title','en',''),(1979,'ratingtag','es',NULL),(1980,'YouGotVoteStore','fr',NULL),(1981,'Forum Actions','en',''),(1982,'HQ.YourProfile.Student','es',''),(1983,'Area of Site','it',''),(1987,'Link1','it',NULL),(1988,'History badges','fr',NULL),(1989,'Add level','es',''),(1990,'n.Answer','en',''),(1991,'Link5','it',NULL),(1992,'articleoid','en',''),(1993,'noProcessInstancesFound','fr',NULL),(1994,'NoWorkItemsFound','en',''),(1996,'HQ.UsersList.Search','it','Cerca'),(1998,'HQ.YourProfile.Certificate','en',''),(1999,'tags','it',NULL),(2000,'HQ.is','es','es'),(2001,'Oid','it',NULL),(2002,'ValueLengthValidationRule.min','fr','<li>the entered value length must be at least ${length} characters</li>'),(2003,'Certification Badge Title','en',''),(2004,'Question','es',NULL),(2005,'HQ.PersonalCertificates','fr',NULL),(2006,'XsdTypeValidationRule.min.exclusive','en','<li>the entered value should be greater than ${value}</li>'),(2008,'YouGotReadingWiki','en','You got points because you have read'),(2009,'HQ.View','es',NULL),(2010,'UserName','it',''),(2011,'HQ.UsersList.Users','fr',NULL),(2012,'MaxLevel','fr',NULL),(2013,'lastModified','it',''),(2014,'getting-start','fr',NULL),(2015,'int','it','Only Integer Number'),(2016,'Involved Actions Associated','en',''),(2017,'Image','it',NULL),(2018,'HQ.UsersList.GoTo','es','Lista Usuarios'),(2019,'Role','fr',NULL),(2020,'Show Activity Details','en',''),(2021,'TimestampValidation','en','<li>the entered value should be in the format ${pattern}</li>'),(2022,'IntegerValidation','it','<li>il valore inserito deve essere di tipo intero</li>'),(2023,'value','fr',NULL),(2024,'XsdTypeValidationRule.invalid.integer','it','<li>Intero non valido</li>'),(2025,'HQ.YourProfile.NoDeveloper','it',''),(2026,'CertificateForm','es',NULL),(2027,'tempIcon','es',NULL),(2028,'Filter by region','fr',NULL),(2029,'confirmNoteDeletion','fr',NULL),(2030,'Manage actions','fr',NULL),(2031,'status','en',''),(2032,'N/D','es',NULL),(2033,'TimestampValidation','it','<li>il valore inserito deve essere nel formato ${pattern}</li>'),(2034,'releaseNotes','en',''),(2035,'notAttachmentsFound','fr',NULL),(2036,'SmallPhoto','es',NULL),(2037,'`key`words (search engine)','it','Cerca'),(2038,'noActionBadge','fr',NULL),(2039,'sendEmail','es',NULL),(2040,'Private Message','it',NULL),(2041,'templatesblob','en',''),(2042,'TypeValidationRule.error','fr','<li>the entered value should have the format ${pattern}</li>'),(2043,'Update','es',NULL),(2044,'noProcessesFound','fr',NULL),(2045,'Store','en','Store'),(2046,'sortByLevel','fr',NULL),(2047,'version','fr',NULL),(2048,'CompareValidationRule.gt.field','fr','<li>the entered value should be greater than the value of ${otherFieldName} field</li>'),(2049,'Action Area','es',''),(2050,'role','it',NULL),(2051,'articleOid','fr',NULL),(2052,'HQ.NicknameNull','en','You haven\'t set any nickname, to see your public profile go back and edit your data.'),(2053,'Query.First','it','primo'),(2054,'linkConfirmMessage','it','Continuare?'),(2056,'Search bar','it','Cerca'),(2057,'contactoid','fr',NULL),(2058,'Modify Actions','es',NULL),(2059,'Certification Score','en','Certification Score'),(2060,'Internal','es',NULL),(2061,'Latest reading','en','Latest Learning Objects'),(2062,'HQ.UsersMonthly','it','Top Users'),(2063,'YouGotPublishStore','en','You got points because you published'),(2064,'Countrytable','es',NULL),(2065,'User latest','fr',NULL),(2066,'CreditCardValidationRule.error','fr','<li>invalid credit card number</li>'),(2067,'address','fr',NULL),(2068,'Disable','es',NULL),(2069,'id','es',NULL),(2070,'reviewCount','it',NULL),(2071,'confirmCancelProcess','es',NULL),(2072,'Monthly Partecipation up','en',''),(2073,'Item','it',NULL),(2074,'Dashboard','fr',NULL),(2075,'KB Level','it','Livello Learn'),(2076,'Latest reading','es','Ãšltimos Learning Objects'),(2077,'Importance','en',''),(2078,'HQ.CertificateTab','fr',NULL),(2079,'Tags','en',''),(2080,'Badge Selected','it',NULL),(2082,'Forum Level','it','Livello Forum'),(2083,'document','fr',NULL),(2084,'HierarchicalIndex.Last','es','last'),(2085,'Filter','en',''),(2086,'CaptchaValidationRule.error','es',' <li>the entered value did not match what was displayed</li>'),(2087,'HQ.store.update','it',NULL),(2088,'Modify Badge','es',NULL),(2089,'Areas','es',''),(2090,'Scrool Actions','fr',NULL),(2091,'Hide Activity Details','fr',NULL),(2092,'CompareValidationRule.eq.field','es','<li>the entered value should be equal to the value of ${otherFieldName} field</li>'),(2093,'active','fr',NULL),(2094,'Filter by badge level','es','Filtrar por nivel de badge'),(2095,'close','fr',NULL),(2096,'go-to-store','en','Go to Store'),(2097,'ManagementEvent','en',''),(2098,'YouBecome','en','You become'),(2099,'active','it',''),(2100,'Generic Message','en',''),(2101,'MandatoryValidationRule.error','en','<li>mandatory field</li>'),(2102,'Link5','fr',NULL),(2104,'Link1','fr',NULL),(2105,'certification','en','Certification'),(2106,'Last Badge','es',NULL),(2107,'Your last readings','en','Your last readings'),(2108,'serialNumberoid','fr',NULL),(2109,'licenseServerVersion','it',NULL),(2110,'List of Badges','es',''),(2111,'HQ.RankingOverall','en','Overall Leaderboard'),(2112,'Level Badge','es',''),(2113,'Scrool Actions','it',NULL),(2114,'HierarchicalIndex.Previous','es','previous'),(2115,'Company Certified','es',NULL),(2117,'KB Badge','es',NULL),(2118,'HierarchicalIndex.Of','es','of'),(2119,'multiselectionfield.deselectAll','es','Quitar filtros'),(2120,'CompareValidationRule.lt','es','<li>the entered value should be smaller than ${value}</li>'),(2121,'actionAlreadyInvolved','fr',NULL),(2122,'Create','es',NULL),(2123,'image','es',NULL),(2124,'SecondMenuLevel','es',''),(2125,'YouGotLogin','it','Hai ottenuto punti per aver effettuato l\'accesso'),(2126,'XsdTypeValidationRule.max.inclusive','es','<li>the entered value should be smaller than or equal to ${value}</li>'),(2127,'New Badge','en',''),(2128,'configuration','es',NULL),(2129,'go-to-forum','it','Vai al Forum'),(2130,'datamodified','it',NULL),(2131,'Partecipation User','it',''),(2132,'Scroller.Jump','it','vai a'),(2133,'congratulations','en',''),(2134,'family','it',NULL),(2135,'XsdTypeValidationRule.invalid.enum','fr','<li>Invalid enumeration value (${values})</li>'),(2136,'PowerIndex.NoItemsShownOf','es','no items shown of'),(2137,'Badge Type','en',''),(2138,'CompareValidationRule.eq','es','<li>the entered value should be equal to ${value}</li>'),(2139,'releaseNotes','it',NULL),(2140,'HQ.RankCompanyNoResults','it','Nessuna azienda in classifica con queste proprietà '),(2141,'SortCombination','es',NULL),(2142,'Certification Dictionary','fr',NULL),(2143,'Latest reading','it','Nuovi Materiali'),(2144,'Credit history','fr',NULL),(2145,'Badge Selected','fr',NULL),(2147,'Query.First','es','first'),(2148,'message','en',''),(2149,'date','en',''),(2150,'HQ.ManageProfile','fr',NULL),(2151,'Confirm Finish','it',NULL),(2153,'LikeValidationRule.endsWith','fr','<li>the entered value should end with ${value}</li>'),(2154,'country','it',NULL),(2155,'resourceType','en',''),(2156,'LikeValidationRule.notStartsWith','es','<li>the entered value should not begin with ${value}</li>'),(2157,'MostimportantBadge','en',''),(2158,'CompareValidationRule.gt','es','<li>the entered value should be greater than ${value}</li>'),(2159,'Scroller.Next','es','next'),(2160,'LinkParticipation','fr',NULL),(2161,'Active','it',''),(2162,'JobStatus.JobDescription','es','Description'),(2163,'ValueLengthValidationRule.max','it','<li>il valore inserito deve essere lungo al massimo ${length} caratteri</li>'),(2164,'JobStatus.TriggerId','fr','ID'),(2165,'IntegerValidation','en','<li>the entered value must be an integer</li>'),(2167,'Participation Monthly','it','Partecipazione mensile'),(2168,'HQ.no.certificates','fr',NULL),(2169,'Fax','fr',NULL),(2170,'Store','it',NULL),(2171,'Description','it',NULL),(2172,'WebRatio Certification','fr',NULL),(2173,'Store Item','fr',NULL),(2174,'YouGotActiving','it','Attivazione'),(2175,'YouGotMonthly','it','Hai ottenuto punti mensili'),(2176,'Delete','it',NULL),(2177,'Public','fr',NULL),(2178,'HierarchicalIndex.NoItemsShownOf','fr','no items shown of'),(2179,'languageCode','en',''),(2180,'address','it',NULL),(2181,'postedAnswer','it',NULL),(2182,'CompareValidationRule.lt','en','<li>the entered value should be smaller than ${value}</li>'),(2183,'supportedVersion','it',NULL),(2184,'postalCode','en',''),(2185,'MostimportantBadge','fr',NULL),(2186,'HQ.YourProfile.Certificate','fr',NULL),(2187,'HQ.YourProfile.PrintPdf','it',''),(2188,'activationDate','en',''),(2190,'HQ.UserDashboard.AllActions','es',NULL),(2191,'Manage Badge','it',NULL),(2192,'Name','es',NULL),(2193,'checkCode','en',''),(2194,'nickname','es',NULL),(2195,'Certification Level','fr',NULL),(2196,'error.button.not.pressed','it','<li>Prego premere il bottone di conferma</li>'),(2197,'HQ.YourProfile.NoDeveloper','fr',NULL),(2198,'rejectionReason','en',''),(2199,'RegularExpressionValidationRule.error','fr','<li>the entered value is not valid</li>'),(2200,'errors.header','es','<ul>'),(2201,'Modify Actions','fr',NULL),(2202,'Area of Site','fr',NULL),(2203,'Authorization','en',''),(2204,'Participation','en','Participation'),(2205,'noProcessesFound','es',NULL),(2206,'Add','es',NULL),(2207,'university','it',NULL),(2208,'Teacher','es',NULL),(2209,'HQ.UsersList.GoTo','fr',NULL),(2210,'close','it',NULL),(2213,'Welcome','it','Benvenuto'),(2214,'iconblob','en',''),(2215,'YouGotPostTopic','it','Hai ottenuto punti per aver postato una domanda'),(2216,'Confirm Finish','en',''),(2217,'url','es',NULL),(2218,'Set the Icons','en',''),(2219,'company','fr',NULL),(2220,'CaptchaValidationRule.error','en',' <li>the entered value did not match what was displayed</li>'),(2223,'language','fr',NULL),(2224,'Linkedin','it','LinkedIn'),(2225,'HQ.UserDashboard','en',''),(2226,'Certification Badge','it',''),(2227,'Monthly Message','it',''),(2228,'YouGotPostSubscription','es','Tienes crÃ©ditos por tu suscripciÃ³n'),(2229,'lastVersionUpdateTimestamp','en',''),(2230,'Monthly Form','en',''),(2231,'Password','it','Password'),(2232,'Logout','en','Logout'),(2233,'go-to-forum','es','Ir al Forum'),(2234,'Modify','it',NULL),(2235,'serialNumber3','en',''),(2236,'areasort','it',NULL),(2237,'HierarchicalIndex.To','es','to'),(2238,'licenseServerVersion','fr',NULL),(2240,'New Level','it',NULL),(2241,'Edit labels','en',''),(2242,'getting-start','en','Getting started'),(2243,'ToolVersion','fr',NULL),(2244,'articleoid','es',NULL),(2245,'All badges','en','See Full Leaderboard'),(2246,'Instructions','en',''),(2247,'HQ.UsersList.AnyLevel','en','Clear level filter'),(2248,'Link206','it',NULL),(2249,'Area of Certificate','fr',NULL),(2251,'Serialtable','fr',NULL),(2252,'Area of Certificate','it',''),(2253,'YouBecome','it',NULL),(2254,'PowerIndex.First','fr','first'),(2255,'Logout','fr',NULL),(2256,'Set','en',''),(2257,'name','en',''),(2258,'Add Area','it',''),(2259,'LikeValidationRule.notContains','it','<li>il valore inserito non deve contenere ${value}</li>'),(2260,'postalCode','it',NULL),(2261,'YouGotsetData','es','Usted setted'),(2263,'HD Image','it',NULL),(2264,'Delete','fr',NULL),(2265,'Activationtable','en',''),(2266,'userOid','en',''),(2267,'Question','it',NULL),(2268,'`key`','en',''),(2269,'releaseNotes','es',NULL),(2270,'JobStatus.TriggerDescription','en','Description'),(2271,'Scroller.Previous','fr','previous'),(2272,'Enable','fr',NULL),(2273,'timestamp2','it',NULL),(2274,'CompareValidationRule.lt.field','es','<li>the entered value should be smaller than the value of ${otherFieldName} field</li>'),(2275,'MostimportantBadge','es',NULL),(2276,'CompareValidationRule.gt.field','es','<li>the entered value should be greater than the value of ${otherFieldName} field</li>'),(2277,'YouGotQuestionVoteup','en','You got points because your question was voted up'),(2278,'Internal','fr',NULL),(2280,'Small Photo','fr',NULL),(2281,'custom description','es',NULL),(2282,'Back','en',''),(2283,'duration','es',NULL),(2284,'OID','fr',NULL),(2285,'noActionBadge','en','This Badge has no Actions associated'),(2286,'XsdTypeValidationRule.invalid.boolean','fr','<li>Invalid Boolean</li>'),(2287,'value','en',''),(2288,'publishingDate','fr',NULL),(2289,'HierarchicalIndex.First','it','primo'),(2290,'tempThumbnail','en',''),(2291,'KB Badge','fr',NULL),(2292,'Level','es','Nivel'),(2293,'Question','en',''),(2294,'JobStatus.TriggerGroup','es','Group'),(2295,'YouGotAnswerTopic','it','Hai ottenuto punti per aver risposto ad una domanda'),(2296,'itemId','fr',NULL),(2297,'DatesCertificates','es',NULL),(2298,'Modify Badge Area','en',''),(2299,'Number Of Employees','fr',NULL),(2300,'selectionfield.noselection','es','No selection'),(2301,'BigPhoto','es',NULL),(2302,'go-to-store','fr',NULL),(2305,'LikeValidationRule.notContains','es','<li>the entered value should not contain ${value}</li>'),(2306,'Query.Jump','fr','jump to'),(2307,'resourceType','fr',NULL),(2308,'errors.header','it','<ul>'),(2309,'Image','es',NULL),(2310,'Postal Code','en',''),(2311,'community.users','it','utenti'),(2312,'Group','fr',NULL),(2313,'HQ.UserDashboard.Public','en','Dashboard'),(2314,'HQ.Of','es','de'),(2317,'Link5','en',''),(2318,'Certificates','es',NULL),(2319,'itemId','it',NULL),(2320,'Link1','en',''),(2321,'WebRatio Certification','es',NULL),(2322,'XsdTypeValidationRule.invalid.length','fr','<li>the entered value length must be ${length} characters</li>'),(2323,'LikeValidationRule.notStartsWith','en','<li>the entered value should not begin with ${value}</li>'),(2324,'Public Profile','en',''),(2325,'Clear','es','Borrar filtros'),(2326,'companycc','es',NULL),(2327,'WebSite','es',NULL),(2328,'linkConfirmMessage','en','Continue?'),(2329,'Your last readings','fr',NULL),(2330,'Process.metadata.updated','fr',NULL),(2331,'averageVote','es',NULL),(2332,'HierarchicalIndex.From','it','da'),(2333,'XsdTypeValidationRule.mandatory','it','<li>campo obbligatorio</li>'),(2335,'Hide Activity Details','es',NULL),(2336,'HierarchicalIndex.From','en','from'),(2337,'HQ.YourProfile.At','es','en'),(2338,'User last readings','es','Lecturas de usuario'),(2339,'Forum','it',NULL),(2341,'JobStatus.TriggerDescription','fr','Description'),(2342,'Item','fr',NULL),(2343,'Instructions','fr',NULL),(2344,'MostimportantCertificate','fr',NULL),(2345,'actionAlreadyInvolved','en','For this Area all the available actions have been selected'),(2346,'screenshots','es',NULL),(2348,'HQ.UserDashboard.Public','fr',NULL),(2349,'area','it',NULL),(2350,'Modify Badge','fr',NULL),(2351,'view','en',NULL),(2352,'status','it',NULL),(2353,'Filter by badge level','fr',NULL),(2355,'Twitter','en','Twitter'),(2356,'Manage Badge','es',NULL),(2357,'HQ.UserDashboard.Private','es',NULL),(2358,'User no actions','fr',NULL),(2359,'ValueLengthValidationRule.min','en','<li>the entered value length must be at least ${length} characters</li>'),(2360,'generationDate','it',NULL),(2361,'CropImageUnit.noImage','it',NULL),(2362,'HQ.NoForumActivity','en','No Forum activities'),(2363,'editStatus','en',NULL),(2364,'Forum','en','Forum'),(2365,'Latest Badge','it','Ultimi Badges'),(2366,'YouGotPostSubscription','it','Hai ottenuto punti per la sottoscrizione'),(2367,'Partecipation User','es',NULL),(2368,'Scroller.From','it','da'),(2369,'Latest downloads','it','Nuovi Componenti'),(2370,'confirmAttachmentDeletion','fr',NULL),(2371,'Query.Last','it','ultimo'),(2372,'New Area','fr',NULL),(2373,'Industry Type','it',NULL),(2374,'HQ.PersonalCredits','es','CrÃ©ditos personales'),(2375,'HQ.YourProfile.PrintPdf','es','Descarga tu certificado'),(2376,'languageCode','fr',NULL),(2377,'HQ.YourProfile.Professor','es',''),(2378,'CompareValidationRule.lt.field','fr','<li>the entered value should be smaller than the value of ${otherFieldName} field</li>'),(2379,'Back','it',NULL),(2380,'Timestamp','it',NULL),(2382,'User no actions','en','User no actions'),(2383,'HQ.UsersList.AnyLevel','es','Eliminar filtro de nivel'),(2384,'HierarchicalIndex.Last','en','last'),(2385,'OID','it',NULL),(2387,'TagStore','fr',NULL),(2388,'reissues','fr',NULL),(2389,'No results','fr',NULL),(2390,'FloatValidation','fr','<li>the entered value must be a float</li>'),(2391,'ajax.computingRequest','it','Attendere prego..'),(2392,'Areegeografiche','fr',NULL),(2393,'Manage Action','it',''),(2394,'description','fr',NULL),(2395,'CompareValidationRule.eq','en','<li>the entered value should be equal to ${value}</li>'),(2396,'SmallPhoto','en',NULL),(2397,'FloatValidation','es','<li>the entered value must be a float</li>'),(2398,'Bundle Data','fr',NULL),(2399,'Create','it',''),(2400,'LikeValidationRule.startsWith.field','en','<li>the entered value should begin with the value of ${otherFieldName} field</li>'),(2401,'Go to ranking','it','Vai alla Classifica'),(2402,'YouGotDownloadStore','it','Hai ottenuto punti per il download'),(2403,'History Certificates','fr',NULL),(2404,'YouGotSignUp','es','Usted se inscribe'),(2405,'ValueLengthValidationRule.eq','es','<li>the entered value length must be ${length} characters</li>'),(2407,'XsdTypeValidationRule.max.exclusive','es','<li>the entered value should be smaller than ${value}</li>'),(2408,'HQ.UsersList.FilterArea','es','Filtrar por Especialidad'),(2409,'User last readings','it','ultimi materiali utente'),(2410,'XsdTypeValidationRule.positive.value','es','<li>Must be a positive value</li>'),(2411,'sampleProjectblob','es',NULL),(2412,'userOid','fr',NULL),(2413,'Number Of Employees','en',NULL),(2415,'Monthly Partecipation up','fr',NULL),(2416,'companypartner','en',NULL),(2417,'n.VoteUp','es',NULL),(2418,'Areegeografiche','es',NULL),(2420,'CompareValidationRule.gt','en','<li>the entered value should be greater than ${value}</li>'),(2421,'QueryUnit.wrongExtAttributeCondition','it','Una o più condizioni sono basate su attributi fuori dal table space'),(2422,'deactivationDate','fr',NULL),(2423,'Participation Monthly','fr',NULL),(2424,'descriptionTmp','en',NULL),(2426,'Chunks Text','en',NULL),(2427,'HierarchicalIndex.Next','it','successivo'),(2428,'HQ.Of','fr',NULL),(2429,'Needed Score','it',''),(2430,'XsdTypeValidationRule.min.length','fr','<li>the entered value length must be at least ${length} characters</li>'),(2431,'HQ.YourProfile.Private','en','Your Profile is private'),(2432,'Flow42','en',NULL),(2433,'BirthDate','fr',NULL),(2434,'Flow41','en',NULL),(2438,'n.User total overall','fr',NULL),(2439,'Flow46','en',NULL),(2440,'Module','es',NULL),(2443,'Mandatory3','it','Choose an area'),(2444,'Type of Badge','it',''),(2445,'Mandatory4','it','Insert a title'),(2446,'supportedVersion','es',NULL),(2448,'Levels of the Area:','es',''),(2449,'Mandatory5','it','Insert a needed score'),(2450,'Reputation','it','Reputazione'),(2451,'isoCode','fr',NULL),(2452,'Query.First','fr','first'),(2453,'User last downloads','it','Ultimi download utente'),(2454,'Labels','en',NULL),(2455,'confirmCancelProcess','fr',NULL),(2456,'Flow40','en',NULL),(2457,'Flow37','en',NULL),(2458,'Answer','fr',NULL),(2459,'FoundationYear','fr',NULL),(2460,'screenshots','en',NULL),(2461,'Area Badge','it',''),(2462,'Flow39','en',NULL),(2463,'Flow38','en',NULL),(2465,'HQ.slider.error','fr',NULL),(2466,'TimestampValidation','fr','<li>the entered value should be in the format ${pattern}</li>'),(2467,'ValueLengthValidationRule.max','en','<li>the entered value length must be at most ${length} characters</li>'),(2468,'HQ.YourProfile.ProfileUnknown','en','Profile Unknown'),(2469,'Flow64','en',NULL),(2470,'QueryUnit.wrongExtAttributeCondition','fr','One or more conditions are based on attributes outside the table space'),(2471,'PowerIndex.Last','es','last'),(2472,'Flow65','en',NULL),(2473,'Action Type','es',''),(2474,'timestamp','it',''),(2475,'go to monthly','it','Vai alla Classifica Generale'),(2477,'History badges','en','Badge History'),(2478,'Add area','en',''),(2479,'HQ.UserOf','en','Users of'),(2480,'serialNumber','en',NULL),(2481,'ActivityInstanceNotAvailable','it',''),(2483,'noNews','en','No news'),(2484,'Manage actions','en',NULL),(2485,'error.check.minChecked','fr','<li>check at least {1} items</li>'),(2487,'KB Level','en','Learn Level'),(2488,'Certification Level','es',''),(2489,'thumbnail','it',''),(2490,'ValueLengthValidationRule.min','it','<li>il valore inserito deve essere lungo almeno ${length} caratteri</li>'),(2491,'PowerIndex.Last','fr','last'),(2492,'User Data','fr',NULL),(2493,'isoCode','es',NULL),(2494,'noNews','es',NULL),(2495,'oid22','it',NULL),(2496,'oid23','it',NULL),(2497,'Clear','fr',NULL),(2498,'family','en',NULL),(2499,'Mandatory6','it','Insert the importance'),(2500,'HQ.YourProfile.Private','fr',NULL),(2501,'HQ.rank.overall','fr',NULL),(2502,'PowerIndex.First','es','first'),(2503,'publishingDate','it',NULL),(2504,'Edit','it',NULL),(2506,'SmallPhoto','fr',NULL),(2507,'Forum Badge','fr',NULL),(2508,'ajax.computingRequest','es','Computing request..'),(2509,'PowerIndex.Previous','en','previous'),(2510,'HQ.rank.overall','it','Classifica Generale'),(2511,'sequence','fr',NULL),(2512,'Version','es',NULL),(2513,'User no actions','es','Usuario no tiene acciones'),(2514,'userOid','it',NULL),(2515,'ModuleID','es',NULL),(2516,'Participation Monthly','en','Monthly Participation'),(2517,'postalCode','es',NULL),(2518,'noActionBadge','es',NULL),(2519,'Forum','es','Forum'),(2520,'message','fr',NULL),(2521,'HQ.YourActions','it',NULL),(2523,'XsdTypeValidationRule.mandatory','es','<li>mandatory field</li>'),(2525,'sendEmail','it',NULL),(2526,'CheckedItemsValidationRule.max','en','<li>check at most ${itemCount} items</li>'),(2527,'noItemsFound','fr',NULL),(2528,'QueryUnit.noDiplayAtrributes','it','Nessun display attribute selezionato'),(2529,'HierarchicalIndex.Next','es','next'),(2530,'JobStatus.JobId','es','ID'),(2531,'text','es',NULL),(2532,'thumbnail','es',NULL),(2533,'linkConfirmMessage','es','Continue?'),(2534,'CompareValidationRule.lt.field','en','<li>the entered value should be smaller than the value of ${otherFieldName} field</li>'),(2535,'XsdTypeValidationRule.min.length','en','<li>the entered value length must be at least ${length} characters</li>'),(2536,'PowerIndex.NoItemsShownOf','fr','no items shown of'),(2537,'HQ.GoToMyPosition','en','Go to My Position'),(2538,'CheckedItemsValidationRule.eq','en','<li>${itemCount} items checked required</li>'),(2539,'Manage Action','en',''),(2540,'YouGotsetData','fr',NULL),(2541,'YouGotShareGooglePlus','it','Hai ottenuto punti per aver condiviso su Google+'),(2543,'Level Badge','it',''),(2544,'JobStatus.JobId','fr','ID'),(2545,'CollectionValidationRule.in','fr','<li>the entered value must be one of: ${values}</li>'),(2546,'imageChecked','en',''),(2547,'Public profile','es',''),(2548,'checkCode','it',NULL),(2549,'answerOf','fr',NULL),(2550,'confirmRollback','en',''),(2551,'Last Name','fr',NULL),(2552,'All Reputation Actions','en',''),(2553,'LowLevel','fr',NULL),(2554,'all actions','es',NULL),(2556,'Mandatory3','en','Choose an area'),(2558,'Labels Text','en',''),(2559,'tempScreenshots','es',NULL),(2560,'community.users','es','usuarios'),(2561,'Area of Certificate','es',''),(2562,'JobStatus.TriggerGroup','en','Group'),(2563,'Teacher Name','fr',NULL),(2564,'ActionForm2','en',''),(2565,'selectionfield.world','en','All'),(2566,'Modify','es',NULL),(2567,'HierarchicalIndex.Last','fr','last'),(2568,'CompareValidationRule.eq.field','it','<li> il valore inserito deve essere uguale al valore del campo ${otherFieldName}</li>'),(2569,'lms','it',NULL),(2570,'YouGotPostTopic','en','You got points to have posted question'),(2571,'Postal Code','fr',NULL),(2572,'alias','en',''),(2573,'LikeValidationRule.notEndsWith','it','<li>il valore inserito non deve finire con ${value}</li>'),(2574,'Check private','fr',NULL),(2575,'Company Type','en',''),(2576,'Show Pdf','es',NULL),(2577,'error.validation.exception','en','<li>The following exception occurred during form validation: {0}</li>'),(2578,'sampleProjectblob','fr',NULL),(2579,'Region','es',NULL),(2580,'author','en',''),(2581,'screenshots','it',NULL),(2582,'HQ.UsersList.Filter','fr',NULL),(2583,'Go to monthly','es','Ir al Ranking'),(2584,'Review2','en',''),(2585,'retries','fr',NULL),(2586,'Sort Badges Area','en',''),(2587,'Installation','es',NULL),(2588,'universitycc','es',NULL),(2589,'Delete All','es',NULL),(2590,'Nickname','it',NULL),(2591,'companypartner','it',NULL),(2592,'HQ.YourProfile.Public','en','Your profile is public'),(2593,'icon','es',NULL),(2594,'HQ.no.certificates','en','No certificates'),(2595,'moduleDomainName','fr',NULL),(2596,'encodedtitle','fr',NULL),(2597,'postedAnswer','es',NULL),(2598,'User last downloads','fr',NULL),(2599,'Add Badge','it',''),(2600,'DemoUsers','es',''),(2601,'Fax','it',NULL),(2602,'XsdTypeValidationRule.mandatory','fr','<li>mandatory field</li>'),(2605,'Show Pdf','it',NULL),(2606,'Monthly Partecipation User','es',NULL),(2607,'Monthly Partecipation up','es',NULL),(2608,'LikeValidationRule.notEndsWith','en','<li>the entered value should not end with ${value}</li>'),(2610,'Process.metadata.invalid','es',NULL),(2611,'Course Students Number','fr',NULL),(2612,'error.stacktrace','it','{0}'),(2613,'error.empty','fr','{0}'),(2614,'title','es',NULL),(2615,'Store Badge Title','it',NULL),(2616,'answerdate','es',NULL),(2617,'HQ.GoToMyPosition','es','Ir a Mi PosiciÃ³n'),(2618,'shortDescription','it',NULL),(2619,'HQ.store.update','es',NULL),(2620,'HQ.RankNoResults','fr',NULL),(2621,'List of Actions','es',NULL),(2623,'CollectionValidationRule.in','es','<li>the entered value must be one of: ${values}</li>'),(2624,'HQ.PersonalData','en','Personal Info'),(2625,'Country','fr',NULL),(2626,'HQ.TotalCredits','fr',NULL),(2628,'Filter by badge type','en','Filter by badge type'),(2629,'CollectionValidationRule.notIn','fr','<li>the entered value must not be one of: ${values}</li>'),(2630,'HQ.CreditsTab','fr',NULL),(2631,'users','en','users'),(2632,'XsdTypeValidationRule.fraction.digits','es','<li>the entered value must have at most ${digits} fraction digits</li>'),(2633,'Score','es',NULL),(2634,'forum','es','Forum'),(2635,'YouGotsetData','it','Hai impostato'),(2636,'tag','it',NULL),(2637,'actionAlreadyInvolved','es',''),(2638,'CompareValidationRule.gteq.field','it','<li>il valore inserito deve essere maggiore o uguale al valore del campo ${otherFieldName}</li>'),(2639,'PowerIndex.Of','en','of'),(2640,'Community Participation','fr',NULL),(2641,'Active','fr',NULL),(2642,'Latest Published Learning Objects','es','Ãšltimos Learning Objects'),(2643,'MostimportantCertificate','en',NULL),(2644,'Resource Type','en',NULL),(2645,'List of Levels','it',''),(2646,'Latest topics','fr',NULL),(2647,'go to monthly','es','Ver el Ranking Completo'),(2648,'useridentifier','fr',NULL),(2649,'CompareValidationRule.lteq.field','es','<li>the entered value should be smaller than or equal to the value of ${otherFieldName} field</li>'),(2650,'Checked Image','es',NULL),(2652,'YouGotAnswerTopic','fr',NULL),(2653,'Email','fr',NULL),(2654,'HQ.YourActions','es','Sus acciones'),(2655,'CompareValidationRule.neq','es','<li>the entered value should be different from ${value}</li>'),(2656,'User last readings','fr',NULL),(2657,'ToolVersion','en',NULL),(2658,'ModuleName','fr',NULL),(2659,'tempScreenshots','fr',NULL),(2660,'Certification','en','Certification'),(2661,'YouGotMonthly','es','Tienes crÃ©ditos mensuales'),(2662,'HQ.UserDashboard','es',NULL),(2663,'imageChecked','it',NULL),(2664,'HQ.no.badges','es',NULL),(2665,'Getting Started','it','Come iniziare'),(2666,'Latest Published Learning Objects','en','Latest Learning Objects'),(2667,'Rank Participation Monthly','es',''),(2668,'`key`words (search engine)','fr',NULL),(2669,'Certifications history','it','Certificazioni acquisite'),(2670,'HQ.View','fr',NULL),(2671,'XsdTypeValidationRule.invalid.time','en','<li>Invalid Time</li>'),(2672,'Edit labels','it',NULL),(2673,'Modify Area','fr',NULL),(2674,'username','fr',NULL),(2675,'Title','es',NULL),(2676,'answer','en',NULL),(2677,'Area Badge','fr',NULL),(2678,'CertificateForm','fr',NULL),(2679,'PowerIndex.To','en','to'),(2680,'CollectionValidationRule.notIn','es','<li>the entered value must not be one of: ${values}</li>'),(2681,'History certificate','fr',NULL),(2682,'CollectionValidationRule.notIn.query','it','<li>il valore inserito è incluso nei valori vietati</li>'),(2683,'HQ.PersonalCertificates','it',NULL),(2684,'SecondMenuLevel','en','Community'),(2685,'Time','es',NULL),(2686,'ModuleID','fr',NULL),(2687,'levelCertificate','it',''),(2688,'Monthly Participation','it',''),(2690,'HQ.UsersList.Profile','fr',NULL),(2691,'HierarchicalIndex.NoItemsShownOf','es','no items shown of'),(2692,'Creation Date','it',NULL),(2693,'lastModified','en',NULL),(2694,'LikeValidationRule.contains','fr','<li>the entered value should contain ${value}</li>'),(2695,'Store Level','fr',NULL),(2696,'document','es',NULL),(2698,'Address','es',NULL),(2699,'NotPublicProfile.Message','it',NULL),(2700,'Latest Forum Topics','es','Ãšltimos Temas del Forum'),(2701,'Time','fr',NULL),(2702,'Pdf','en',NULL),(2703,'Website','fr',NULL),(2704,'MandatoryIfTrue.error','es',NULL),(2705,'LikeValidationRule.notEndsWith.field','it','<li>il valore inserito non deve finire con il valore del campo ${otherFieldName}</li>'),(2706,'Bundle Data','es',NULL),(2707,'ModuleID','en',NULL),(2709,'username','it','Username'),(2710,'XsdTypeValidationRule.min.exclusive','fr','<li>the entered value should be greater than ${value}</li>'),(2711,'go to homepage','en','Panoramica'),(2712,'Company','fr',NULL),(2713,'Labels','it',NULL),(2714,'isoCode','en',''),(2715,'timestamp','fr',NULL),(2716,'Headquarter','it','Panoramica'),(2717,'PowerIndex.Jump','es','jump to'),(2718,'HQ.UsersList.FilterArea','en','Filter by Expertise'),(2720,'oid22','en',''),(2721,'HQ.YourProfile.Private','it',''),(2723,'HQ.PersonalData','es',NULL),(2724,'oid23','en',''),(2725,'HierarchicalIndex.Of','fr','of'),(2726,'HQ.is','fr',NULL),(2727,'Save','en',''),(2728,'Importance','es',NULL),(2729,'editionoid','it',NULL),(2730,'XsdTypeValidationRule.max.length','es','<li>the entered value length must be at most ${length} characters</li>'),(2731,'Description','en','Description'),(2733,'ValueLengthValidationRule.eq','fr','<li>the entered value length must be ${length} characters</li>'),(2735,'Internal','en','Internal'),(2736,'My certificate','it',NULL),(2737,'downloadCount','es',NULL),(2738,'New Level','en',''),(2739,'User no actions','it','Nessuna azione compiuta'),(2740,'by','en',''),(2741,'Article','it',NULL),(2742,'serverTimestamp','en',''),(2744,'Modify Badge','it',NULL),(2745,'XsdTypeValidationRule.invalid.timestamp','es','<li>Invalid Timestamp</li>'),(2746,'Edit','en',''),(2747,'Scroller.Jump','fr','jump to'),(2748,'Description','es',NULL),(2749,'Country','es',NULL),(2750,'at','en',''),(2751,'styleProject','fr',NULL),(2752,'HQ.NotFromSite','es','Usted no puede ver este perfil, ir a la pÃ¡gina principal e iniciar sesiÃ³n.'),(2753,'HierarchicalIndex.To','fr','to'),(2754,'HQ.HomePage','en',''),(2755,'JobStatus.TriggerNextFireTimestamp','es','Next Fire Timestamp'),(2756,'LikeValidationRule.contains.field','fr','<li>the entered value should contain the value of ${otherFieldName} field</li>'),(2757,'oid22','es',NULL),(2758,'oid23','es',NULL),(2759,'iconblob','es',NULL),(2760,'HQ.store.authorBy','en','By'),(2761,'HQ.UserDashboard','fr',NULL),(2763,'areasort','en',''),(2764,'YouGotDownloadStore','fr',NULL),(2765,'Type','it',NULL),(2766,'ordinal','it',NULL),(2767,'HQ.View','en','Views'),(2768,'Profile','es',NULL),(2769,'valideType.error','it',NULL),(2770,'note','fr',NULL),(2771,'templatesblob','fr',NULL),(2772,'document','en',''),(2773,'Area','en','Area'),(2774,'country','en',''),(2775,'confirmDeletion','fr',NULL),(2776,'YouGotActiving','fr',NULL),(2777,'HQ.NoStoreActivity','fr',NULL),(2778,'fileblob','es',NULL),(2779,'noActionBadge','it',NULL),(2781,'thumbnail','fr',NULL),(2782,'Query.Last','es','last'),(2783,'HierarchicalIndex.First','fr','first'),(2784,'User overall position','en',''),(2785,'Community User','fr',NULL),(2786,'Search','fr',NULL),(2787,'supportedVersion','en',NULL),(2788,'Overall Participation','es',''),(2789,'Modify','en',NULL),(2790,'Tags','es',NULL),(2791,'HQ.UserDashboard.AllActions','en','All Actions'),(2792,'JobStatus.TriggerId','it','ID'),(2793,'Monthly Form','es',NULL),(2794,'accepted','fr',NULL),(2795,'Manage Area','it',NULL),(2797,'Article','en',NULL),(2798,'articleOid','es',NULL),(2799,'Set','es',NULL),(2800,'Modify Area','en',''),(2801,'Message','it',NULL),(2802,'HQ.RankNoResults','es','No hay usuarios en este rank con estas propiedades.'),(2803,'Timestamp','fr',NULL),(2804,'Partecipation User','en',NULL),(2805,'Monthly Partecipation down','it',''),(2806,'Participation Monthly','es','ParticipaciÃ³n mensual'),(2807,'HQ.Credits','es','crÃ©ditos'),(2808,'Bundle Data','en',NULL),(2809,'XsdTypeValidationRule.invalid.time','it','<li>Time non valido</li>'),(2810,'JobStatus.TriggerId','en','ID'),(2811,'timestamp2','fr',NULL),(2812,'CollectionValidationRule.notIn','en','<li>the entered value must not be one of: ${values}</li>'),(2813,'activationDate','it',''),(2814,'Participation Company','es',''),(2815,'Item','en',NULL),(2816,'PDF Certificate','en',NULL),(2817,'History certificate','en',NULL),(2818,'go to monthly','fr',NULL),(2820,'Website','it','Sito Web'),(2822,'Area','es',''),(2823,'Department','fr',NULL),(2824,'actionAlreadyInvolved','it','Per questa area tutte le azioni disponibili sono state compiute'),(2825,'Add level','fr',NULL),(2826,'EMailValidationRule.error','fr','<li>invalid email address</li>'),(2827,'HQ.YourProfile.Professional','it',''),(2828,'JobStatus.TriggerNextFireTimestamp','it','Next Fire Timestamp'),(2829,'url','fr',NULL),(2830,'LikeValidationRule.notStartsWith.field','en','<li>the entered value should not begin with the value of ${otherFieldName} field</li>'),(2831,'YouGotActivingClient','fr',NULL),(2832,'alias','es',NULL),(2833,'Badge Type','it',NULL),(2834,'articleOid','it',NULL),(2835,'Profile','en',NULL),(2836,'Edit labels','fr',NULL),(2837,'HQ.YourProfile.Developer','fr',NULL),(2838,'Bio','en','Bio'),(2839,'Type','en','Type'),(2840,'Go to account','fr',NULL),(2841,'XsdTypeValidationRule.total.digits','en','<li>the entered value must have at most ${digits} total digits</li>'),(2842,'forum','en','Forum'),(2843,'XsdTypeValidationRule.min.inclusive','es','<li>the entered value should be greater than or equal to ${value}</li>'),(2844,'Partner','es',NULL),(2845,'Linkedin','en','LinkedIn'),(2847,'HierarchicalIndex.Previous','it','precedente'),(2848,'Action Area','en',''),(2849,'History actions','fr',NULL),(2850,'DatesCertificates','fr',NULL),(2851,'Webratio Activation','en',NULL),(2852,'Partecipation down','fr',NULL),(2853,'serialNumber','it',NULL),(2854,'CheckedItemsValidationRule.min','en','<li>check at least ${itemCount} items</li>'),(2855,'XsdTypeValidationRule.min.inclusive','fr','<li>the entered value should be greater than or equal to ${value}</li>'),(2856,'Big Photo','es',NULL),(2857,'Sort Number','es',NULL),(2858,'Countrytable','fr',NULL),(2859,'WebSite','en','Website'),(2860,'language','it',NULL),(2861,'maxVersion','en',NULL),(2862,'Getting Started','en','Getting started'),(2864,'Credit history','es','CrÃ©ditos ganados'),(2865,'Add level','it',''),(2866,'YouGotQuestionVoteup','fr',NULL),(2867,'YouGotApprovedAnswer','es','Tienes crÃ©ditos porque usted ha aprobado la respuesta'),(2868,'HQ.Message','it',NULL),(2869,'title','fr',NULL),(2870,'Go to participation','es','Ir al Ranking'),(2871,'Action Type','it',''),(2872,'n.Answer','es',NULL),(2873,'LikeValidationRule.notStartsWith.field','it','<li>il valore inserito non deve iniziare con il valore del campo ${otherFieldName}</li>'),(2874,'university','es',NULL),(2875,'YouGotPostTopic','fr',NULL),(2876,'community.goToDashboard','fr',NULL),(2877,'CertificateForm','en',NULL),(2878,'HQ.NoForumActivity','it',''),(2879,'HQ.Answer','it',NULL),(2881,'`key`words (search engine)','en',''),(2882,'ordinal','fr',NULL),(2883,'Company Email','es',NULL),(2884,'FormNotEmptyValidationRule.error','es','<li>at least one field must be filled</li>'),(2885,'Levels of the Area:','fr',NULL),(2886,'XsdTypeValidationRule.invalid.length','es','<li>the entered value length must be ${length} characters</li>'),(2887,'linkConfirmMessage','fr','Continue?'),(2889,'error.check.maxChecked','fr','<li>check at most {1} items</li>'),(2890,'CertificationCRM','it',''),(2891,'HierarchicalIndex.Previous','fr','previous'),(2892,'LikeValidationRule.notEndsWith','es','<li>the entered value should not end with ${value}</li>'),(2893,'Chunks','en',NULL),(2894,'downloadCount','fr',NULL),(2895,'TimeValidation','fr','<li>the entered value should be in the format ${pattern}</li>'),(2896,'Module','it',NULL),(2897,'YouGotActiving','es','ActivaciÃ³n'),(2898,'levelsort','it',''),(2899,'YouGotPublishStore','fr',NULL),(2900,'serialNumberoid','es',NULL),(2901,'notNotesFound','it',NULL),(2902,'XsdTypeValidationRule.min.inclusive','en','<li>the entered value should be greater than or equal to ${value}</li>'),(2903,'timestamp','es',NULL),(2904,'Go to account','en','Go to Account'),(2905,'Website','es',NULL),(2906,'Latest Published Store Components','en','Latest Store Components'),(2907,'Name','it',NULL),(2908,'custom description','en',NULL),(2909,'Company','en',NULL),(2910,'CheckedItemsValidationRule.min','it','<li>selezionare almeno ${itemCount} elementi</li>'),(2911,'HQ.ParticipationTab','es','ParticipaciÃ³n'),(2912,'styleProjectblob','en',NULL),(2913,'country','es',NULL),(2914,'of','en','of'),(2915,'family','fr',NULL),(2916,'Generic Message','it',''),(2917,'HQ.NoStoreActivity','es',NULL),(2918,'Latest Forum Topics','it','Nuove Domande del Forum'),(2919,'Pdf','fr',NULL),(2920,'JobStatus.JobGroup','fr','Group'),(2921,'Hide Activity Details','it',''),(2922,'HQ.RankingOverall','it','Classifica Generale'),(2923,'Green Check','it',''),(2924,'Date','fr',NULL),(2926,'Course Title','it',''),(2927,'Link206','fr',NULL),(2928,'HQ.UsersList.Filter','es','Filtrar por Especialidad'),(2929,'LikeValidationRule.notContains','fr','<li>the entered value should not contain ${value}</li>'),(2930,'Welcome','fr',NULL),(2931,'Certification Score','es',NULL),(2932,'Manage Badge','fr',NULL),(2933,'ActionForm','it',''),(2934,'User','es',NULL),(2935,'Partner','it',''),(2936,'error.check.maxChecked','it','<li>selezionare al massimo {1} elemento</li>'),(2937,'Industry Type','es',NULL),(2938,'n.VoteUp','it',''),(2939,'Logo','it',''),(2941,'go-to-forum','en','Go to Forum'),(2942,'tempScreenshotsblob','it',''),(2944,'Last Name','en','Last Name'),(2945,'YouGotReadingWiki','es','Tienes crÃ©ditos porque usted ha leÃ­do'),(2946,'User','en',NULL),(2947,'Active','es',''),(2948,'HQ.UsersList.AnyArea','it','Reset'),(2949,'HQ.PersonalCertificates','en','Personal Certificates'),(2950,'minVersion','fr',NULL),(2951,'answerdate','it',''),(2953,'file','fr',NULL),(2954,'CompareValidationRule.gteq','it','<li>il valore inserito deve essere maggiore o uguale di ${value}</li>'),(2955,'Overall Participation','it',''),(2956,'selectionfield.world','fr',NULL),(2957,'Student','fr',NULL),(2958,'WebSite','fr',NULL),(2959,'no my actions','it','Nessuna azione compiuta'),(2960,'Add','en',''),(2961,'approved','en','was approved'),(2962,'Save','es',NULL),(2963,'thumbnailblob','es',NULL),(2964,'Message','en',''),(2965,'Involved Actions Associated','fr',NULL),(2966,'HQ.UserDashboard.Public','es',NULL),(2967,'LinkParticipation','en',''),(2968,'Link206','en',''),(2969,'Scroller.From','en','from'),(2970,'link','es',NULL),(2972,'tempIconblob','en',''),(2973,'Twitter','fr',NULL),(2974,'HQ.PersonalCredits','en','Personal Points'),(2975,'YouGotAnswerApproved','en','You got points because your answer was approved'),(2976,'useridentifier','en',''),(2977,'Installation','it',''),(2980,'productid','en',''),(2981,'CaptchaValidationRule.error','fr',' <li>the entered value did not match what was displayed</li>'),(2982,'level','fr',NULL),(2983,'Private Message','es',NULL),(2984,'XsdTypeValidationRule.invalid.date','it','<li>Data non valida</li>'),(2985,'Filter','es',''),(2986,'View','fr',NULL),(2987,'Needed Score','es',NULL),(2988,'Credit history','it','Punti & Premi'),(2989,'Scroller.To','fr','to'),(2990,'Phone Number','es',NULL),(2991,'noProcessesFound','en',''),(2992,'reviewCount','fr',NULL),(2993,'XsdTypeValidationRule.invalid.integer','en','<li>Invalid Integer</li>'),(2995,'active','en',''),(2996,'History certificate','es',''),(2997,'error.button.not.pressed','es','<li>Please press a button for form submission</li>'),(2998,'image','it',''),(2999,'vote','es',NULL),(3000,'Participation','fr',NULL),(3001,'oidtag','en',''),(3002,'NotPublicProfile.Message','es',NULL),(3003,'Involved Actions Associated','es',NULL),(3004,'getting-start','es','GuÃ­a de Usuario'),(3005,'Flow42','it',''),(3006,'Flow41','it',''),(3007,'tempIcon','it',''),(3008,'Flow40','it',''),(3009,'HQ.PersonalCredits','it','Punti Personali'),(3011,'Flow46','it',''),(3013,'PowerIndex.Last','it','ultimo'),(3016,'approved','fr',NULL),(3017,'Scroller.Previous','it','precedente'),(3018,'duration','it',''),(3019,'History badges','it','Badges acquisiti'),(3020,'file','it',''),(3021,'questiondate','en',''),(3022,'XsdTypeValidationRule.max.inclusive','it','<li>il valore inserito deve essere minore o uguale a ${value}</li>'),(3023,'HQ.YourProfile.Professor','en','Professor'),(3024,'No results','es',NULL),(3025,'Nickname_Crm','it',''),(3026,'Flow37','it',''),(3027,'Flow38','it',''),(3028,'Flow39','it',''),(3029,'Areas','it',''),(3030,'HQ.UserDashboard ','fr',NULL),(3031,'Flow64','it',''),(3032,'HierarchicalIndex.Jump','es','jump to'),(3034,'questiondate','fr',NULL),(3035,'id','en',''),(3036,'Linkedin','es',NULL),(3037,'Show Activity Details','es',NULL),(3038,'styleProject','es',NULL),(3039,'Company Email','en',''),(3040,'certification','fr',NULL),(3041,'Flow65','it',NULL),(3042,'Title','it',NULL),(3043,'noItemsFound','it',NULL),(3044,'Newsletter','es',NULL),(3045,'checkCode','es',NULL),(3046,'content','en',''),(3047,'Update','fr',NULL),(3048,'Rank Participation Overall','it',''),(3049,'HQ.MyDashboard ','fr',NULL),(3050,'RegularExpressionValidationRule.error','es','<li>the entered value is not valid</li>'),(3052,'licenseServerOperatingSyste','en',''),(3053,'Scroller.Previous','en','previous'),(3054,'multiselectionfield.deselectAll','fr','Deselect All'),(3055,'CheckedItemsValidationRule.max','it','<li>selezionare al massimo ${itemCount} elementi</li>'),(3056,'answer','fr',NULL),(3057,'HQ.PersonalBadges','fr',NULL),(3058,'Company Certified','fr',NULL),(3059,'Certificates','it',NULL),(3060,'ActivityInstanceNotAvailable','fr',NULL),(3061,'Headquarter','en','Overview'),(3062,'description (search engine)','fr',NULL),(3063,'imageChecked','fr',NULL),(3064,'Show Activity Details','it',NULL),(3065,'Latest Published Store Components','fr',NULL),(3066,'Certification Badge','en',''),(3067,'go-to-forum','fr',NULL),(3068,'Message','es',NULL),(3069,'SecondMenuLevel','fr',NULL),(3070,'QueryUnit.noDiplayAtrributes','fr','No selected display attributes'),(3071,'Teacher LastName','en',''),(3072,'user','fr',NULL),(3073,'Delete','es',NULL),(3074,'ModuleName','en',''),(3075,'Process.metadata.updated','es',NULL),(3076,'Scroller.Previous','es','previous'),(3078,'HQ.YourProfile.At','en','at'),(3079,'Student','es',NULL),(3080,'notAttachmentsFound','it',NULL),(3081,'UserOf','it','di'),(3082,'Areegeografiche','it',NULL),(3083,'fileblob','en',''),(3084,'HQ.UsersList.AnyArea','en','Clear area filter'),(3085,'oid22','fr',NULL),(3086,'oid23','fr',NULL),(3087,'HQ.YourProfile.NoData','fr',NULL),(3088,'FoundationYear','es',NULL),(3089,'Company Email','it',NULL),(3090,'Scroller.Of','fr','of'),(3091,'timestamp2','en',''),(3092,'QueryUnit.noDiplayAtrributes','es','No selected display attributes'),(3093,'tempThumbnail','es',NULL),(3094,'name','es',NULL),(3095,'Checked Image','fr',NULL),(3096,'Delete','en',''),(3097,'Tag','es',NULL),(3098,'N/D','it',NULL),(3099,'ajax.computingRequest','en','Computing request..'),(3100,'invioEffettuato','en',''),(3101,'Partecipation User','fr',NULL),(3102,'Previous Activity','en',''),(3103,'HQ.YourProfile.Work','es','Trabaja'),(3104,'Scroller.Of','en','of'),(3106,'XsdTypeValidationRule.invalid.enum','it','<li>Valore non previsto (${values})</li>'),(3107,'First Name','it',NULL),(3108,'user','it',''),(3109,'Forum Badge Title','es',''),(3110,'Monthly Partecipation User','en',''),(3111,'Your last topics','it','Ultime tue Domande'),(3112,'Reputation','es','ReputaciÃ³n'),(3113,'Item','es',NULL),(3114,'HQ.UsersList.FilterLevel','it','Filtra per livello minimo'),(3115,'HQ.store.vote','fr',NULL),(3116,'MaxLevel','it',''),(3117,'Partecipation up','es',NULL),(3118,'XsdTypeValidationRule.max.exclusive','it','<li>il valore inserito deve essere minore di ${value}</li>'),(3119,'XsdTypeValidationRule.negative.value','fr','<li>Must be a negative value</li>'),(3120,'History Diagram','fr',NULL),(3121,'Address','fr',NULL),(3122,'Name','en',''),(3123,'Manage actions','es',NULL),(3124,'QueryUnit.wrongGrouping','it','Tutti i display attribute devono essere utilizzati nel raggruppamento o nella funzione aggregata'),(3125,'productid','fr',NULL),(3126,'XsdTypeValidationRule.invalid.integer','fr','<li>Invalid Integer</li>'),(3127,'User','it',''),(3128,'DecimalValidation','en','<li>the entered value must be a number</li>'),(3129,'Area','fr',NULL),(3130,'HQ.YourProfile.Public','it',''),(3131,'rejectionReason','it',NULL),(3132,'go-to-lms','en','Go to Learn'),(3133,'LikeValidationRule.endsWith','en','<li>the entered value should end with ${value}</li>'),(3134,'retries','it',NULL),(3135,'Hide Activity Details','en',NULL),(3136,'LikeValidationRule.endsWith','it','<li>il valore inserito deve finire con ${value}</li>'),(3137,'Review','en',NULL),(3138,'HQ.store.vote','it',NULL),(3139,'WebRatio Certification','it',''),(3140,'tempName','it',NULL),(3141,'Scroller.Last','it','ultimo'),(3142,'HQ.UsersList.AnyArea','fr',NULL),(3143,'Query.From','it','da'),(3144,'LikeValidationRule.notContains','en','<li>the entered value should not contain ${value}</li>'),(3145,'text','en',NULL),(3146,'selectionfield.noselection','en','no selection'),(3147,'congratulations','it',NULL),(3148,'configuration','en',NULL),(3149,'HQ.NoStoreActivity','en','No Store activities'),(3150,'Nickname_Crm','fr',NULL),(3151,'Industry Type','fr',NULL),(3152,'multiselectionfield.selectAll','fr','Select All'),(3153,'TypeValidationRule.error','es','<li>the entered value should have the format ${pattern}</li>'),(3154,'Contact','en',NULL),(3155,'Add level','en',''),(3156,'Scroller.To','en','to'),(3157,'`key`','es',NULL),(3158,'lms','fr',NULL),(3159,'HD Checked Image','es',NULL),(3160,'QueryUnit.wrongExtAttributeCondition','es','One or more conditions are based on attributes outside the table space'),(3161,'Teacher LastName','es',NULL),(3162,'tempDescription','it',NULL),(3163,'Go to ranking','fr',NULL),(3164,'YouGotVoteStore','es','Tienes crÃ©ditos porque usted votÃ³ hasta'),(3165,'Language','en',NULL),(3166,'KB Level','fr',NULL),(3167,'PowerIndex.First','en','first'),(3169,'content','fr',NULL),(3170,'Add Level','en',''),(3171,'Latest Forum Topics','fr',NULL),(3172,'Installation','fr',NULL),(3173,'Mandatory6','en','Insert the importance'),(3174,'Linkedin','fr',NULL),(3175,'Mandatory4','en','Insert a title'),(3176,'Mandatory5','en','Insert a needed score'),(3177,'Areatomail','en',NULL),(3178,'NotPublicProfile.Message','fr',NULL),(3180,'address','en',NULL),(3181,'Scrool Actions','es',NULL),(3182,'YouBecome','fr',NULL),(3183,'Query.Jump','it','vai a'),(3184,'User monthly position','fr',NULL),(3185,'oid','es',NULL),(3186,'multiselectionfield.selectAll','es','Seleccionar todo'),(3187,'certification','it','Certificazioni'),(3188,'YouGotAnswerApproved','es','Tienes crÃ©ditos porque su respuesta fue aprobada'),(3189,'HQ.NicknameNull','fr',NULL),(3191,'Answer','it',NULL),(3192,'Last Name','it','Cognome'),(3193,'ActivityInstanceNotAvailable','es',NULL),(3194,'disabled','it',NULL),(3195,'date','fr',NULL),(3196,'file','en',NULL),(3197,'sequence','es',NULL),(3198,'HQ.UsersList.Filter','en','Filter by Expertise'),(3199,'community.users','en','users'),(3200,'vote','fr',NULL),(3201,'url','it',NULL),(3202,'styleProject','en',NULL),(3203,'Modify Area','es',NULL),(3204,'Process.metadata.updated','en',NULL),(3205,'screenshotsblob','it',NULL),(3206,'noNews','fr',NULL),(3207,'postalCode','fr',NULL),(3208,'publishingDate','es',NULL),(3210,'editStatus','it',NULL),(3211,'visibility','fr',NULL),(3212,'templates','it',''),(3213,'Your last readings','it','ultimi articoli letti'),(3214,'users','fr',NULL),(3215,'KB Badge','en','Learn Badge'),(3216,'Twitter','it',''),(3217,'HQ.ParticipationTab','en','Participation'),(3218,'Dashboard','es',NULL),(3219,'accepted','es',''),(3220,'All','en','All'),(3221,'link','it',''),(3222,'Nickname_Crm','en','Nickname'),(3223,'HQ.MyDashboard ','es',NULL),(3224,'Search bar','en',''),(3225,'Go to participation','it','Vai alla Classifica Generale'),(3226,'Sort Badges Area','it',''),(3227,'DecimalValidation','it','<li>il valore inserito deve essere di tipo numerico</li>'),(3228,'YouGotPostTopic','es','Tienes crÃ©ditos para haber publicado pregunta'),(3229,'Role','en',NULL),(3230,'HQ.UsersMonthly','en','Top Users'),(3231,'Latest reading','fr',NULL),(3232,'tempThumbnailblob','it',''),(3233,'LMS','es','Learn'),(3235,'KB Badge Title','es',NULL),(3236,'noProcessInstancesFound','it',''),(3238,'MostimportantBadge','it',''),(3239,'Tag','it',''),(3240,'serverTimestamp','it',''),(3241,'serialNumberoid','en',NULL),(3242,'Rating','en',NULL),(3243,'Score','en',NULL),(3244,'`key`word','en',NULL),(3245,'Filter by region','en','Filter by country'),(3246,'Geographical Area','fr',NULL),(3247,'CropImageUnit.noImage','fr',NULL),(3248,'Timestamp','es',NULL),(3249,'all actions','it',''),(3250,'Filter by badge level','en','Filter by badge level'),(3251,'Review2','es',NULL),(3252,'city','fr',NULL),(3253,'Mobile','en',NULL),(3254,'PowerIndex.Next','es','next'),(3255,'Public profile','it',''),(3256,'Mobile','es',NULL),(3257,'Certification Level','it',''),(3258,'Filter by badge type','fr',NULL),(3259,'RegularExpressionValidationRule.error','it','<li>il valore inserito non è valido</li>'),(3261,'Save','fr',NULL),(3262,'Logout','it','Logout'),(3263,'Type of Badge','en',''),(3264,'Message','fr',NULL),(3265,'Link95','fr',NULL),(3266,'Edit Text','en',NULL),(3267,'Monthly Partecipation down','es',NULL),(3270,'Select a region','it','Seleziona una regione'),(3272,'HQ.UsersList.Profile','it','Profilo'),(3275,'12_topMonthly','it','Top User di Dicembre'),(3276,'11_topMonthly','it','Top User di Novembre'),(3277,'10_topMonthly','it','Top User di Ottobre'),(3278,'09_topMonthly','it','Top User di Settembre'),(3279,'08_topMonthly','it','Top User di Agosto'),(3280,'07_topMonthly','it','Top User di Luglio'),(3281,'06_topMonthly','it','Top User di Giugno'),(3282,'05_topMonthly','it','Top User di Maggio'),(3283,'04_topMonthly','it','Top User di Aprile'),(3284,'03_topMonthly','it','Top User di Marzo'),(3285,'02_topMonthly','it','Top User di Febbraio'),(3286,'01_topMonthly','it','Top User di Gennaio'),(3287,'12_topMonthly','en','December Top Users'),(3288,'11_topMonthly','en','November Top Users'),(3289,'10_topMonthly','en','October Top Users'),(3290,'09_topMonthly','en','September Top Users'),(3291,'08_topMonthly','en','August Top Users'),(3292,'07_topMonthly','en','July Top Users'),(3293,'06_topMonthly','en','June Top Users'),(3294,'05_topMonthly','en','May Top Users'),(3295,'04_topMonthly','en','April Top Users'),(3296,'03_topMonthly','en','March Top Users'),(3297,'02_topMonthly','en','February Top Users'),(3298,'01_topMonthly','en','January Top Users'),(3299,'12_topMonthly','es','Top Usuarios de Diciembre '),(3300,'12_topMonthly','es','Top Usuarios de Diciembre '),(3301,'11_topMonthly','es','Top Usuarios de Noviembre'),(3302,'10_topMonthly','es','Top Usuarios de Octubre'),(3303,'09_topMonthly','es','Top Usuarios de Septiembre'),(3304,'07_topMonthly','es','Top Usuarios de Julio'),(3305,'06_topMonthly','es','Top Usuarios de Junio'),(3306,'05_topMonthly','es','Top Usuarios de Mayo'),(3307,'04_topMonthly','es','Top Usuarios de Abril'),(3308,'03_topMonthly','es','Top Usuarios de Marzo'),(3309,'02_topMonthly','es','Top Usuarios de Febrero'),(3310,'01_topMonthly','es','Top Usuarios de Enero'),(3311,'12_Monthly','it','Classifica di Dicembre'),(3312,'11_Monthly','it','Classifica di Novembre'),(3313,'10_Monthly','it','Classifica di Ottobre'),(3314,'09_Monthly','it','Classifica di Settembre'),(3315,'08_Monthly','it','Classifica di Agosto'),(3316,'07_Monthly','it','Classifica di Luglio'),(3317,'06_Monthly','it','Classifica di Giugno'),(3318,'05_Monthly','it','Classifica di Maggio'),(3319,'04_Monthly','it','Classifica di Aprile'),(3320,'03_Monthly','it','Classifica di Marzo'),(3321,'02_Monthly','it','Classifica di Febbraio'),(3322,'01_Monthly','it','Classifica di Gennaio'),(3323,'12_Monthly','en','December Ranking'),(3324,'11_Monthly','en','November Ranking'),(3325,'10_Monthly','en','October Ranking'),(3326,'09_Monthly','en','September Ranking'),(3327,'08_Monthly','en','August Ranking'),(3328,'07_Monthly','en','July Ranking'),(3329,'06_Monthly','en','June Ranking'),(3330,'05_Monthly','en','May Ranking'),(3331,'04_Monthly','en','April Ranking'),(3332,'03_Monthly','en','March Ranking'),(3333,'02_Monthly','en','February Ranking'),(3334,'01_Monthly','en','January Ranking'),(3335,'12_Monthly','es','RÃ¡nking de Diciembre'),(3336,'12_Monthly','es','RÃ¡nking de Diciembre'),(3337,'11_Monthly','es','RÃ¡nking de Noviembre'),(3338,'10_Monthly','es','RÃ¡nking de Octubre'),(3339,'09_Monthly','es','RÃ¡nking de Septiembre'),(3340,'08_Monthly','es','RÃ¡nking de Agosto'),(3341,'07_Monthly','es','RÃ¡nking de Julio'),(3342,'06_Monthly','es','RÃ¡nking de Junio'),(3343,'05_Monthly','es','RÃ¡nking de Mayo'),(3344,'04_Monthly','es','RÃ¡nking de Abril'),(3345,'03_Monthly','es','RÃ¡nking de Marzo'),(3346,'02_Monthly','es','RÃ¡nking de Febrero'),(3347,'01_Monthly','es','RÃ¡nking de Enero '),(3348,'08_topMonthly','es','Top Usuarios de Agosto'),(3349,'Help','es',NULL),(3350,'n.User 7 Days filtered','it',NULL),(3351,'Points','en','Points'),(3352,'Assign new reward','fr',NULL),(3353,'Event','es',NULL),(3354,'08_topMonthly','fr',NULL),(3355,'`key`word','fr',NULL),(3356,'Action Name','es',NULL),(3357,'Manage Containers','es',NULL),(3358,'Notification','en',NULL),(3359,'YouGotWebinar','en','You got points to have attended the webinar:'),(3360,'Add reward','fr',NULL),(3361,'Edit reward','es',NULL),(3362,'Newsletter Area','fr',NULL),(3363,'HQ.rank.7Days.home','en','7 Days Leaderboard'),(3364,'History points','fr',NULL),(3365,'Delivery Date','es',NULL),(3366,'ValueLengthValidationRule.blob.min','it','<li>dimensione file errata (>${length})</li>'),(3367,'Next Rewards','en',''),(3368,'Community Users Area','it',NULL),(3369,'Ok','it',NULL),(3370,'Community Participation Seven Days','it',NULL),(3371,'LeaderBoards','it',NULL),(3372,'Edit Mail','en',NULL),(3373,'Add container','en',NULL),(3374,'Availability','it',NULL),(3375,'Users','fr',NULL),(3376,'Edit reward','en',''),(3377,'Alias','en',NULL),(3378,'Insert Actions','it',NULL),(3379,'Flow106','en',NULL),(3380,'Actions','fr',NULL),(3381,'Flow97','it',NULL),(3382,'Actions History','es','Todo las AcciÃ³nes'),(3383,'ValueLengthValidationRule.blob.neq','es','<li>file size not allowed (${length})</li>'),(3384,'Status','fr',NULL),(3385,'New Reward','en',''),(3386,'New reward','it',NULL),(3387,'n.User 7 Days filtered','es',''),(3388,'Flow122','en',NULL),(3389,'Participation 7 Days','fr',NULL),(3390,'Events','fr',NULL),(3391,'Flow120','en',NULL),(3392,'Container','fr',NULL),(3393,'ValueLengthValidationRule.blob.eq','en','<li>wrong file size (${length})</li>'),(3394,'New Reward','es',NULL),(3395,'Monthly Points','en',NULL),(3396,'Newsletter Preview','it',NULL),(3397,'Error','es',NULL),(3398,'Container','es',NULL),(3399,'Presences','fr',NULL),(3400,'Creation date','es',NULL),(3401,'Iso Code','es',NULL),(3402,'Delivery date','es',NULL),(3403,'Footer','es',NULL),(3404,'Assign new action','en',NULL),(3405,'`key`word','en',NULL),(3406,'HQ.Ranking7Days','fr',NULL),(3407,'Alias','fr',NULL),(3408,'Rewards Area','en','Rewards Info'),(3409,'community.possible.reward','fr',NULL),(3410,'Manage Containers','fr',NULL),(3411,'Executor','fr',NULL),(3412,'Notification LeaderBoards','en',NULL),(3413,'Ok','en',NULL),(3414,'Total Credit','fr',NULL),(3415,'Manage Rewards','it',''),(3416,'Rank 7 Days','es',''),(3417,'Code','en',NULL),(3418,'Flow127','en',NULL),(3419,'email','en',NULL),(3420,'community.slider.step3title','es',NULL),(3421,'mail template','en',NULL),(3422,'Flow148','fr',NULL),(3423,'counter7DaysLeaderboard','es','en la clasifica de los ultimos 7 dÃ­as'),(3424,'Flow143','fr',NULL),(3425,'Overall Position','fr',NULL),(3426,'Delivery Date','en',NULL),(3427,'Needed Points','en',NULL),(3428,'Status','en',NULL),(3429,'Executor','es',NULL),(3430,'Newsletter Events','it',NULL),(3431,'Manage Rewards','es',''),(3432,'Text','fr',NULL),(3433,'n.User 7 Days filtered','en',NULL),(3434,'Leaderboards','en','Leaderboards'),(3435,'Reward','en',''),(3436,'Useroid','en',NULL),(3437,'Flow150','fr',NULL),(3438,'Preview','it',NULL),(3439,'forum.action.points','it',''),(3440,'Preview','en',NULL),(3441,'Language Code','en',NULL),(3442,'community.comingsoon','fr',NULL),(3443,'HQ.rank.7Days','it','Classifica degli Ultimi 7 Giorni'),(3444,'Leaderboards','fr',NULL),(3445,'Available','en',NULL),(3446,'community.slider.step2title','fr',NULL),(3447,'community.spent.points','en','Spent Points'),(3448,'Facebook','fr',NULL),(3449,'ValueLengthValidationRule.blob.min','es','<li>wrong file size (>${length})</li>'),(3450,'Delivery Date','it',NULL),(3451,'Reward Type','es',NULL),(3452,'YouGotClassroom','fr',NULL),(3453,'mail template','fr',NULL),(3454,'Flow122','fr',NULL),(3455,'Event','fr',NULL),(3456,'Flow127','fr',NULL),(3457,'Users list','fr',NULL),(3458,'useroid','it',NULL),(3459,'Subject','it',NULL),(3460,'Redirect','fr',NULL),(3461,'Download Certificates History','fr',NULL),(3462,'Event','en',NULL),(3463,'Text','it',NULL),(3464,'Seven Days Position','fr',NULL),(3465,'Flow97','fr',NULL),(3466,'View Actions History','es',''),(3467,'10_topMonthly','fr',NULL),(3468,'New reward','fr',NULL),(3469,'Needed points','it','Punti necessari'),(3470,'Action Name','en',NULL),(3471,'Rewards','it','Premi'),(3472,'New action','es',NULL),(3473,'7 Days Message','it',NULL),(3474,'mail template','it',NULL),(3475,'Notification Monthly','en',NULL),(3476,'HTMLMandatory.error','it',NULL),(3477,'Certification Area','fr',NULL),(3478,'community.all.rewards','es',''),(3479,'HTMLMandatory.error','fr',NULL),(3480,'Containers','en',NULL),(3481,'Participation 7 Days','es',''),(3482,'n.User overall','it',NULL),(3483,'06_topMonthly','fr',NULL),(3484,'forum.action.points','es',''),(3485,'Newsletter Events','en',NULL),(3486,'community.possible.reward','en','Your possible reward'),(3487,'Preview','es',NULL),(3488,'useroid','fr',NULL),(3489,'community.spent.points','it','Punti spesi'),(3490,'Needed Points','fr',NULL),(3491,'useroid','en',NULL),(3492,'Text Mail','fr',NULL),(3493,'counter7DaysLeaderboard','en','in 7 days leaderboard'),(3494,'Total Credit','en',NULL),(3495,'Containers','es',NULL),(3496,'ValueLengthValidationRule.blob.max','it','<li>dimensione file massima superata (${length})</li>'),(3497,'community.slider.step1title','it','Iscriviti alla community'),(3498,'Rewards History','en','Rewards History'),(3499,'Credits Spent','es',NULL),(3500,'Flow106','it',NULL),(3501,'From','fr',NULL),(3502,'Flow106','fr',NULL),(3503,'Newsletter Preview','fr',NULL),(3504,'Points','es','Puntos'),(3505,'community.slider.step3p2','it','e servizi concreti'),(3506,'community.slider.step3p1','it','e spendili in prodotti'),(3507,'Facebook','it',NULL),(3508,'Participation Overall','es',NULL),(3509,'Flow120','fr',NULL),(3510,'counterOverallLeaderboard','fr',NULL),(3511,'02_Monthly','fr',NULL),(3512,'Reward','it',NULL),(3513,'Credits Spent','fr',NULL),(3514,'forum.action.points','fr',NULL),(3515,'Creation date','fr',NULL),(3516,'community.available.points','it','Punti disponibili'),(3517,'Participation Seven Days','it',NULL),(3518,'Monthly Points','it',''),(3519,'7 Days Message','fr',NULL),(3520,'Needed points','en',NULL),(3521,'Community Participation Seven Days','fr',NULL),(3522,'Rewards History','es','Regalos Ganados'),(3523,'Seven Days Participation','en',NULL),(3524,'Edit Container','fr',NULL),(3525,'YouGotWebinar','it',NULL),(3526,'community.slider.step1p1','it','ed entra in contatto '),(3527,'community.slider.step1p3','it','di tutto il mondo'),(3528,'community.slider.step1p2','it',''),(3529,'Seven Days Position','es',NULL),(3530,'community.slider.step2p2','it','tra gli sviluppatori'),(3531,'Seven Days Position','it',NULL),(3532,'community.slider.step2p1','it','e guadagna reputazione'),(3533,'Reward Instance','en',''),(3534,'set area','es',NULL),(3535,'community.slider.step1p1','en','and connect'),(3536,'Overall Position','es',NULL),(3537,'community.slider.step1p3','en','around the world!'),(3538,'community.slider.step1p2','en',''),(3539,'Action Name','it',NULL),(3540,'Facebook','es',NULL),(3541,'Alias','es',NULL),(3542,'Users','es',NULL),(3543,'community.slider.step2p1','en','and build your reputation'),(3544,'ValueLengthValidationRule.blob.min','fr','<li>wrong file size (>${length})</li>'),(3545,'community.slider.step2p2','en','among developers'),(3546,'Flow127','it',NULL),(3547,'community.total.points','es','Puntos Totales'),(3548,'Flow122','it',NULL),(3549,'Gamification Area','it',NULL),(3550,'Community Participation Seven Days','en',NULL),(3551,'community.slider.step2title','it','Scala le classifiche'),(3552,'community.slider.step3p1','en','and use them for products'),(3553,'community.slider.step3p2','en','and concrete services'),(3554,'Footer','en',NULL),(3555,'ValueLengthValidationRule.blob.eq','es','<li>wrong file size (${length})</li>'),(3556,'09_Monthly','fr',NULL),(3557,'community.comingsoon','it',NULL),(3558,'HQ.rank.7Days.home','it','Classifica degli Ultimi 7 Giorni'),(3559,'Flow120','it',NULL),(3560,'Actions History','en','Actions History'),(3561,'community.slider.step3p1','fr',NULL),(3562,'community.slider.step3p2','fr',NULL),(3563,'Iso Code','fr',NULL),(3564,'LeaderBoards','fr',NULL),(3565,'`key`word','es',NULL),(3566,'community.slider.step1p2','fr',NULL),(3567,'community.slider.step1p1','fr',NULL),(3568,'community.slider.step1p3','fr',NULL),(3569,'Go Back','es',NULL),(3570,'community.slider.step2p1','fr',NULL),(3571,'Reward Title','es',NULL),(3572,'community.slider.step2title','es',NULL),(3573,'Gamification Area','en',NULL),(3574,'community.slider.step2p2','fr',NULL),(3575,'Actions','en',NULL),(3576,'HQ.rank.7Days.home','fr',NULL),(3577,'ValueLengthValidationRule.blob.neq','it','<li>dimensione file non permessa (${length})</li>'),(3578,'New Container','en',NULL),(3579,'Flow150','it',NULL),(3580,'YouGotClassroom','en','You got points to have attended the classroom training:'),(3581,'set area','it',NULL),(3582,'Header','fr',NULL),(3583,'New action','en',NULL),(3584,'n.User 7 Days Total','fr',NULL),(3585,'Photo','es',NULL),(3586,'Leaderboards','es',NULL),(3587,'HQ.Ranking7Days','en','7 Days Leaderboard'),(3588,'Notification LeaderBoards','es',NULL),(3589,'ValueLengthValidationRule.blob.neq','fr','<li>file size not allowed (${length})</li>'),(3590,'Footer','it',NULL),(3591,'Text Mail','it',NULL),(3592,'Status','es',NULL),(3593,'Newsletter Area','es',NULL),(3594,'Redirect','es',NULL),(3595,'Flow148','it',NULL),(3596,'Available','es',NULL),(3597,'New Container','es',NULL),(3598,'Flow143','it',NULL),(3599,'Participation Overall','en',NULL),(3600,'Actions History','it','Tutte le Azioni'),(3601,'Manage Rewards','en',''),(3602,'Text','en',NULL),(3603,'Attention','es',NULL),(3604,'HQ.rank.7Days.home','es','Clasifica de los Ultimos 7 DÃ­as'),(3605,'Reward Type','fr',NULL),(3606,'01_topMonthly','fr',NULL),(3607,'Reward','es',NULL),(3608,'Mails','es',NULL),(3609,'community.all.rewards','en','All rewards that you can riceive:'),(3610,'Credits Available','it',NULL),(3611,'Code','es',NULL),(3612,'View Rewards History','it',''),(3613,'Monthly Points','fr',NULL),(3614,'Useroid','es',NULL),(3615,'community.spent.points','es','Puntos Gastados'),(3616,'Monthly Points','es',''),(3617,'Assign new reward','en',''),(3618,'Notification LeaderBoards','fr',NULL),(3619,'n.User overall','fr',NULL),(3620,'Mail','fr',NULL),(3621,'Upload Actions File','it',NULL),(3622,'Community Users Area','en',NULL),(3623,'Body','fr',NULL),(3624,'Seven Days Participation','it',NULL),(3625,'YouGotClassroom','it',NULL),(3626,'View Rewards History','es',''),(3627,'Photo','en',NULL),(3628,'Credits Available','en',NULL),(3629,'noRewards','es',''),(3630,'Newsletter Events','es',NULL),(3631,'Actions History','fr',NULL),(3632,'n.User overall','en',NULL),(3633,'09_topMonthly','fr',NULL),(3634,'Needed Points','es',''),(3635,'Presences','es',NULL),(3636,'Edit Mail','it',NULL),(3637,'Alias','it',NULL),(3638,'Action','it',NULL),(3639,'Rewards Area','it','Info Premi'),(3640,'12_Monthly','fr',NULL),(3641,'Language Code','es',NULL),(3642,'Users list','es',NULL),(3643,'Go Back','en','Go Back'),(3644,'06_Monthly','fr',NULL),(3645,'Possible Rewards','fr',NULL),(3646,'Action Name','fr',NULL),(3647,'Subject','es',NULL),(3648,'Rewards','es',''),(3649,'To','es',NULL),(3650,'Body','en',NULL),(3651,'community.total.points','fr',NULL),(3652,'Total Credit','es',NULL),(3653,'community.slider.step1title','es',NULL),(3654,'noRewards','it',''),(3655,'History points','es',''),(3656,'Notification LeaderBoards','it',NULL),(3657,'Delivery date','it',NULL),(3658,'community.comingsoon','es',NULL),(3659,'Rank 7 Days','fr',NULL),(3660,'History points','it',''),(3661,'Participation Seven Days','en',NULL),(3662,'Photo','it',NULL),(3663,'Language Code','fr',NULL),(3664,'Action','en',NULL),(3665,'userDash','fr',NULL),(3666,'Events','en',NULL),(3667,'Flow97','es',''),(3668,'Add reward','es',NULL),(3669,'userDash','it',NULL),(3670,'Possible Rewards','en',''),(3671,'Needed points','es',''),(3672,'Google','fr',NULL),(3673,'Users','it',NULL),(3674,'Manage Containers','en',NULL),(3675,'LeaderBoards','es',NULL),(3676,'counterOverallLeaderboard','it','nella classifica generale'),(3677,'07_topMonthly','fr',NULL),(3678,'Next Rewards','it',''),(3679,'set area','fr',NULL),(3680,'Events','it',NULL),(3681,'community.comingsoon','en','Coming soon'),(3682,'Notification','it',NULL),(3683,'05_Monthly','fr',NULL),(3684,'Delivery Date','fr',NULL),(3685,'Credits Available','es',NULL),(3686,'File Exel','es',NULL),(3687,'Availability','es',NULL),(3688,'Header','es',NULL),(3689,'Google','en','Google+'),(3690,'Google','it',NULL),(3691,'Edit Mail','fr',NULL),(3692,'Certification Area','en',NULL),(3693,'Action','fr',NULL),(3694,'Seven Days Participation','fr',NULL),(3695,'Text','es',NULL),(3696,'Presences','it',NULL),(3697,'Availability','fr',NULL),(3698,'7 Days Message','es',''),(3699,'Users list','en',NULL),(3700,'Mails','it',NULL),(3701,'Delivery date','fr',NULL),(3702,'Container','it',NULL),(3703,'04_Monthly','fr',NULL),(3704,'LeaderBoards','en',NULL),(3705,'ValueLengthValidationRule.blob.max','en','<li>maximum file size exceeded (${length})</li>'),(3706,'Credits Spent','en',NULL),(3707,'Add container','fr',NULL),(3708,'Flow148','es',NULL),(3709,'Gamification Area','fr',NULL),(3710,'Notification','fr',NULL),(3711,'Flow143','es',NULL),(3712,'ValueLengthValidationRule.blob.neq','en','<li>file size not allowed (${length})</li>'),(3713,'Header','it',NULL),(3714,'Flow150','es',NULL),(3715,'Rewards History','it','Premi Acquisiti'),(3716,'Participation Seven Days','fr',NULL),(3717,'ValueLengthValidationRule.blob.max','fr','<li>maximum file size exceeded (${length})</li>'),(3718,'Possible Rewards','es',''),(3719,'Edit Mail','es',NULL),(3720,'mail template','es',NULL),(3721,'Community Users Area','es',NULL),(3722,'Reward Type','it',NULL),(3723,'From','it',NULL),(3724,'Available','it',NULL),(3725,'community.spent.points','fr',NULL),(3726,'n.User overall','es',NULL),(3727,'File Exel','fr',NULL),(3728,'Text Mail','es',NULL),(3729,'Upload Actions File','en',NULL),(3730,'Text Mail','en',NULL),(3731,'From','en',NULL),(3732,'Newsletter Area','en',NULL),(3733,'Language Code','it',NULL),(3734,'HTMLMandatory.error','en',NULL),(3735,'community.slider.step2p1','es',NULL),(3736,'Download Certificates History','es',NULL),(3737,'Preview','fr',NULL),(3738,'community.available.points','en','Available Points'),(3739,'From','es',NULL),(3740,'Edit reward','it',NULL),(3741,'Reward Instance','it',NULL),(3742,'Rewards History','fr',NULL),(3743,'Error','fr',NULL),(3744,'Reward Title','en',''),(3745,'user oid','en',NULL),(3746,'set area','en',NULL),(3747,'community.total.points','it','Punti totali'),(3748,'community.all.rewards','it',''),(3749,'Rewards Area','fr',NULL),(3750,'Needed points','fr',NULL),(3751,'Presences','en',NULL),(3752,'community.available.points','es','Puntos Disponibles'),(3753,'Slider','es',NULL),(3754,'Error','en',NULL),(3755,'Header','en',NULL),(3756,'Executor','it',NULL),(3757,'counterOverallLeaderboard','en','in overall leaderboard'),(3758,'Go Back','it','Indietro'),(3759,'Participation Seven Days','es',NULL),(3760,'Rewards Area','es',''),(3761,'community.slider.step3title','it','Guadagna punti'),(3762,'community.slider.step2title','en','Rise leaderboards'),(3763,'community.possible.reward','it',NULL),(3764,'Attention','it',NULL),(3765,'File Exel','en',NULL),(3766,'Assign new reward','it',NULL),(3767,'Edit Container','en',NULL),(3768,'Add container','es',NULL),(3769,'Mails','fr',NULL),(3770,'noRewards','fr',NULL),(3771,'Community Participation Seven Days','es',NULL),(3772,'userDash','es',NULL),(3773,'Newsletter Preview','es',NULL),(3774,'counterOverallLeaderboard','es','en la clasifica general'),(3775,'Executor','en',NULL),(3776,'n.User 7 Days Total','it',NULL),(3777,'ValueLengthValidationRule.blob.eq','it','<li>dimensione file errata (${length})</li>'),(3778,'Newsletter Area','it',NULL),(3779,'ValueLengthValidationRule.blob.eq','fr','<li>wrong file size (${length})</li>'),(3780,'community.all.rewards','fr',NULL),(3781,'04_topMonthly','fr',NULL),(3782,'Leaderboards','it',NULL),(3783,'user oid','it',NULL),(3784,'View Actions History','it',''),(3785,'community.slider.step3title','en','Gain points'),(3786,'07_Monthly','fr',NULL),(3787,'View Rewards History','en',''),(3788,'community.slider.step3p1','es',NULL),(3789,'community.slider.step3p2','es',NULL),(3790,'Insert Actions','es',NULL),(3791,'Ok','es',NULL),(3792,'New Container','fr',NULL),(3793,'Assign new reward','es',NULL),(3794,'03_Monthly','fr',NULL),(3795,'New action','fr',NULL),(3796,'Next Rewards','fr',NULL),(3797,'Points','fr',NULL),(3798,'01_Monthly','fr',NULL),(3799,'Body','it',NULL),(3800,'Upload Actions File','es',NULL),(3801,'Help','fr',NULL),(3802,'Overall Position','it',NULL),(3803,'Add reward','en',''),(3804,'Slider','it',NULL),(3805,'HQ.Ranking7Days','it','Classifica degli Ultimi 7 Giorni'),(3806,'Subject','en',NULL),(3807,'Useroid','it',NULL),(3808,'Credits Spent','it',NULL),(3809,'Error','it',NULL),(3810,'Body','es',NULL),(3811,'New Reward','fr',NULL),(3812,'community.total.points','en','Total gained Points'),(3813,'HQ.rank.7Days','es','Clasifica de los Ultimos 7 DÃ­as'),(3814,'Mail','it',NULL),(3815,'email','it',NULL),(3816,'New Reward','it',NULL),(3817,'Users list','it',NULL),(3818,'Action','es',NULL),(3819,'user oid','es',NULL),(3820,'My Points','fr',NULL),(3821,'community.slider.step1title','fr',NULL),(3822,'To','en',NULL),(3823,'New reward','en',''),(3824,'Gamification Area','es',NULL),(3825,'HQ.Ranking7Days','es','Clasifica de los Ultimos 7 DÃ­as'),(3826,'Event','it',NULL),(3827,'7 Days Message','en',NULL),(3828,'Rewards','en','Rewards'),(3829,'Creation date','en',NULL),(3830,'Slider','en',NULL),(3831,'useroid','es',NULL),(3832,'Slider','fr',NULL),(3833,'Reward Instance','fr',NULL),(3834,'Reward','fr',NULL),(3835,'Edit reward','fr',NULL),(3836,'Seven Days Position','en',NULL),(3837,'community.possible.reward','es',NULL),(3838,'Containers','fr',NULL),(3839,'View Actions History','en',NULL),(3840,'Add reward','it',NULL),(3841,'community.available.points','fr',NULL),(3842,'Rewards','fr',NULL),(3843,'Insert Actions','en',NULL),(3844,'Points','it','Punti'),(3845,'Edit Container','es',NULL),(3846,'Rank 7 Days','it',NULL),(3847,'Certification Area','it',NULL),(3848,'Download Certificates History','it',NULL),(3849,'Container','en',NULL),(3850,'View Rewards History','fr',NULL),(3851,'08_Monthly','fr',NULL),(3852,'Creation date','it',NULL),(3853,'Containers','it',NULL),(3854,'Certification Area','es',NULL),(3855,'n.User 7 Days filtered','fr',NULL),(3856,'Assign new action','es',NULL),(3857,'Add container','it',NULL),(3858,'Rank 7 Days','en',NULL),(3859,'ValueLengthValidationRule.blob.min','en','<li>wrong file size (>${length})</li>'),(3860,'Newsletter Events','fr',NULL),(3861,'Redirect','it',NULL),(3862,'Notification Monthly','es',NULL),(3863,'HQ.rank.7Days','en','7 Days Leaderboard'),(3864,'Participation 7 Days','en',NULL),(3865,'Flow97','en',NULL),(3866,'Actions','it',NULL),(3867,'`key`word','it',NULL),(3868,'Mail','en',NULL),(3869,'To','fr',NULL),(3870,'Availability','en',NULL),(3871,'community.slider.step2p2','es',NULL),(3872,'community.slider.step3title','fr',NULL),(3873,'New action','it',NULL),(3874,'Reward Title','fr',NULL),(3875,'email','es',NULL),(3876,'My Points','es',''),(3877,'Available','fr',NULL),(3878,'Reward Instance','es',NULL),(3879,'Total Credit','it',NULL),(3880,'Code','it',NULL),(3881,'New reward','es',NULL),(3882,'n.User 7 Days Total','en',NULL),(3883,'Insert Actions','fr',NULL),(3884,'Go Back','fr',NULL),(3885,'Help','it',NULL),(3886,'Reward Title','it',NULL),(3887,'Manage Rewards','fr',NULL),(3888,'Community Users Area','fr',NULL),(3889,'user oid','fr',NULL),(3890,'File Exel','it',NULL),(3891,'Notification Monthly','fr',NULL),(3892,'02_topMonthly','fr',NULL),(3893,'Subject','fr',NULL),(3894,'Manage Containers','it',NULL),(3895,'Flow148','en',NULL),(3896,'Code','fr',NULL),(3897,'05_topMonthly','fr',NULL),(3898,'Events','es',NULL),(3899,'Assign new action','it',NULL),(3900,'ValueLengthValidationRule.blob.max','es','<li>maximum file size exceeded (${length})</li>'),(3901,'community.slider.step1p1','es',NULL),(3902,'Flow150','en',NULL),(3903,'community.slider.step1p2','es',NULL),(3904,'New Container','it',NULL),(3905,'Attention','en',NULL),(3906,'Notification Monthly','it',NULL),(3907,'counter7DaysLeaderboard','fr',NULL),(3908,'Notification','es',NULL),(3909,'03_topMonthly','fr',NULL),(3910,'YouGotClassroom','es',NULL),(3911,'Upload Actions File','fr',NULL),(3912,'Participation 7 Days','it',NULL),(3913,'My Points','it',''),(3914,'community.slider.step1title','en','Join the Community'),(3915,'12_topMonthly','fr',NULL),(3916,'Flow120','es',NULL),(3917,'Flow143','en',NULL),(3918,'Flow122','es',NULL),(3919,'11_topMonthly','fr',NULL),(3920,'Possible Rewards','it',''),(3921,'To','it',NULL),(3922,'Assign new action','fr',NULL),(3923,'Newsletter Preview','en',NULL),(3924,'Facebook','en','Facebook'),(3925,'Flow127','es',''),(3926,'HQ.rank.7Days','fr',NULL),(3927,'YouGotWebinar','fr',NULL),(3928,'userDash','en',NULL),(3929,'HTMLMandatory.error','es',NULL),(3930,'Google','es',NULL),(3931,'Participation Overall','fr',NULL),(3932,'Redirect','en',NULL),(3933,'Next Rewards','es',''),(3934,'Iso Code','en',NULL),(3935,'Edit Container','it',NULL),(3936,'counter7DaysLeaderboard','it','nella classifica degli ultimi 7 giorni'),(3937,'Attention','fr',NULL),(3938,'Participation Overall','it',NULL),(3939,'Delivery date','en',NULL),(3940,'Status','it',NULL),(3941,'YouGotWebinar','es',NULL),(3942,'n.User 7 Days Total','es',''),(3943,'Ok','fr',NULL),(3944,'View Actions History','fr',NULL),(3945,'Photo','fr',NULL),(3946,'Footer','fr',NULL),(3947,'email','fr',NULL),(3948,'Reward Type','en',''),(3949,'My Points','en','My Points'),(3950,'Actions','es',NULL),(3951,'11_Monthly','fr',NULL),(3952,'Useroid','fr',NULL),(3953,'History points','en','Points & Rewards'),(3954,'Credits Available','fr',NULL),(3955,'Iso Code','it',NULL),(3956,'10_Monthly','fr',NULL),(3957,'Users','en',NULL),(3958,'Overall Position','en',NULL),(3959,'community.slider.step1p3','es',NULL),(3960,'Mails','en',NULL),(3961,'Mail','es',NULL),(3962,'Needed Points','it','Punti necessari'),(3963,'Seven Days Participation','es',NULL),(3964,'Flow106','es',NULL),(3965,'Help','en',NULL),(3966,'forum.action.points','en','Answer at this question and get 70 points!'),(3967,'Download Certificates History','en',NULL),(3968,'noRewards','en','No Rewards'),(3969,'Badge','en','Badge'),(3970,'See Your Rewards','en','See Your Rewards'),(3971,'View Reward','en','View Reward'),(3972,'Reward Details','en','Reward Details'),(3973,'cannotGetRewards','en','You don\'t have enough points to get this reward'),(3974,'Needed Points','en','Needed Points'),(3975,'Get Reward','en','Get Reward'),(3976,'forum.goToDashboard','en','Go To User\'s Dashboard'),(3977,'English','en','English'),(3978,'Italian','en','Italian'),(3979,'English','it','Inglese'),(3980,'Italian','it','Italiano'),(3981,'cannotGetRewards','it','Non hai abbastanza punti per prendere il premio'),(3982,'community.possible.reward','it','Ecco un premio che puoi ottenere'),(3983,'See Your Rewards','it','Guarda i premi');
/*!40000 ALTER TABLE `bundle_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_data`
--

DROP TABLE IF EXISTS `common_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_data` (
  `oid` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `hd_image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_data`
--

LOCK TABLES `common_data` WRITE;
/*!40000 ALTER TABLE `common_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `common_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_user`
--

DROP TABLE IF EXISTS `community_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_user` (
  `oid` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `forum_level` int(11) DEFAULT NULL,
  `small_photo` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `public_profile` tinyint(1) DEFAULT NULL,
  `geographical_area` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `big_photo` varchar(255) DEFAULT NULL,
  `bio` text,
  `linkedin` varchar(255) DEFAULT NULL,
  `certification_level` int(11) DEFAULT NULL,
  `kb_level` int(11) DEFAULT NULL,
  `store_level` int(11) DEFAULT NULL,
  `participation_monthly` decimal(19,2) DEFAULT NULL,
  `forum_badge` varchar(255) DEFAULT NULL,
  `certification_badge` varchar(255) DEFAULT NULL,
  `kb_badge` varchar(255) DEFAULT NULL,
  `store_badge` varchar(255) DEFAULT NULL,
  `kb_badge_title` varchar(255) DEFAULT NULL,
  `store_badge_title` varchar(255) DEFAULT NULL,
  `forum_badge_title` varchar(255) DEFAULT NULL,
  `certification_badge_title` varchar(255) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `participation` decimal(19,2) DEFAULT NULL,
  `credit` decimal(19,2) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `google` varchar(255) DEFAULT NULL,
  `iso_code` varchar(255) DEFAULT NULL,
  `small_photo_2` varchar(255) DEFAULT NULL,
  `small_photoblob` longblob,
  `big_photo_2` varchar(255) DEFAULT NULL,
  `big_photoblob` longblob,
  `registration_date` datetime DEFAULT NULL,
  PRIMARY KEY (`oid`),
  CONSTRAINT `fk_rank_usertable` FOREIGN KEY (`oid`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_user`
--

LOCK TABLES `community_user` WRITE;
/*!40000 ALTER TABLE `community_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `community_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `community_user_credits_availab`
--

DROP TABLE IF EXISTS `community_user_credits_availab`;
/*!50001 DROP VIEW IF EXISTS `community_user_credits_availab`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `community_user_credits_availab` (
  `oid` tinyint NOT NULL,
  `der_attr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `community_user_credits_spent_v`
--

DROP TABLE IF EXISTS `community_user_credits_spent_v`;
/*!50001 DROP VIEW IF EXISTS `community_user_credits_spent_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `community_user_credits_spent_v` (
  `oid` tinyint NOT NULL,
  `der_attr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `containers_mail`
--

DROP TABLE IF EXISTS `containers_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `containers_mail` (
  `oid` int(11) NOT NULL,
  `language_code` varchar(255) DEFAULT NULL,
  `text` text,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `containers_mail`
--

LOCK TABLES `containers_mail` WRITE;
/*!40000 ALTER TABLE `containers_mail` DISABLE KEYS */;
INSERT INTO `containers_mail` (`oid`, `language_code`, `text`, `alias`) VALUES (1,'en','<p>New User Registration ---</p>\r\n','Header Email Registration');
/*!40000 ALTER TABLE `containers_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamified_application`
--

DROP TABLE IF EXISTS `gamified_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamified_application` (
  `oid` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamified_application`
--

LOCK TABLES `gamified_application` WRITE;
/*!40000 ALTER TABLE `gamified_application` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamified_application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamifiedapplication_thematic_a`
--

DROP TABLE IF EXISTS `gamifiedapplication_thematic_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamifiedapplication_thematic_a` (
  `gamified_application_oid` int(11) NOT NULL,
  `thematic_area_oid` int(11) NOT NULL,
  PRIMARY KEY (`gamified_application_oid`,`thematic_area_oid`),
  KEY `fk_gamifiedapplication_themati` (`gamified_application_oid`),
  KEY `fk_gamifiedapplication_thema_2` (`thematic_area_oid`),
  CONSTRAINT `fk_gamifiedapplication_thema_2` FOREIGN KEY (`thematic_area_oid`) REFERENCES `thematic_area` (`oid`),
  CONSTRAINT `fk_gamifiedapplication_themati` FOREIGN KEY (`gamified_application_oid`) REFERENCES `gamified_application` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamifiedapplication_thematic_a`
--

LOCK TABLES `gamifiedapplication_thematic_a` WRITE;
/*!40000 ALTER TABLE `gamifiedapplication_thematic_a` DISABLE KEYS */;
/*!40000 ALTER TABLE `gamifiedapplication_thematic_a` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `geographical_area`
--

DROP TABLE IF EXISTS `geographical_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geographical_area` (
  `oid` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geographical_area`
--

LOCK TABLES `geographical_area` WRITE;
/*!40000 ALTER TABLE `geographical_area` DISABLE KEYS */;
INSERT INTO `geographical_area` (`oid`, `name`) VALUES (1,'North America'),(2,'Latin America'),(3,'Europe'),(4,'Asia, Africa & Oceania');
/*!40000 ALTER TABLE `geographical_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_moduletable`
--

DROP TABLE IF EXISTS `group_moduletable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_moduletable` (
  `groupoid` int(11) NOT NULL,
  `moduleoid` int(11) NOT NULL,
  PRIMARY KEY (`groupoid`,`moduleoid`),
  KEY `idx_group_moduletable_grouptab` (`groupoid`),
  KEY `idx_group_moduletable_siteview` (`moduleoid`),
  CONSTRAINT `fk_group_moduletable_grouptabl` FOREIGN KEY (`groupoid`) REFERENCES `grouptable` (`oid_2`),
  CONSTRAINT `fk_group_moduletable_siteviewt` FOREIGN KEY (`moduleoid`) REFERENCES `siteviewtable` (`oid_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_moduletable`
--

LOCK TABLES `group_moduletable` WRITE;
/*!40000 ALTER TABLE `group_moduletable` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_moduletable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grouptable`
--

DROP TABLE IF EXISTS `grouptable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grouptable` (
  `oid_2` int(11) NOT NULL,
  `groupname` varchar(255) DEFAULT NULL,
  `siteviewoid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid_2`),
  KEY `idx_grouptable_siteviewtable` (`siteviewoid`),
  CONSTRAINT `fk_grouptable_siteviewtable` FOREIGN KEY (`siteviewoid`) REFERENCES `siteviewtable` (`oid_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grouptable`
--

LOCK TABLES `grouptable` WRITE;
/*!40000 ALTER TABLE `grouptable` DISABLE KEYS */;
INSERT INTO `grouptable` (`oid_2`, `groupname`, `siteviewoid`) VALUES (1,'Administrator',1),(2,'Community User',2);
/*!40000 ALTER TABLE `grouptable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `headquarter_user_partecipation`
--

DROP TABLE IF EXISTS `headquarter_user_partecipation`;
/*!50001 DROP VIEW IF EXISTS `headquarter_user_partecipation`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `headquarter_user_partecipation` (
  `oid` tinyint NOT NULL,
  `partecipation` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `headquarter_user_participation_monthly`
--

DROP TABLE IF EXISTS `headquarter_user_participation_monthly`;
/*!50001 DROP VIEW IF EXISTS `headquarter_user_participation_monthly`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `headquarter_user_participation_monthly` (
  `oid` tinyint NOT NULL,
  `participation_monthly` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `headquarter_user_participation_seven_days`
--

DROP TABLE IF EXISTS `headquarter_user_participation_seven_days`;
/*!50001 DROP VIEW IF EXISTS `headquarter_user_participation_seven_days`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `headquarter_user_participation_seven_days` (
  `oid` tinyint NOT NULL,
  `participation_seven_days` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `job_blob_triggers`
--

DROP TABLE IF EXISTS `job_blob_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `JOB_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_blob_triggers`
--

LOCK TABLES `job_blob_triggers` WRITE;
/*!40000 ALTER TABLE `job_blob_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_blob_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_calendars`
--

DROP TABLE IF EXISTS `job_calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_calendars`
--

LOCK TABLES `job_calendars` WRITE;
/*!40000 ALTER TABLE `job_calendars` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_calendars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_cron_triggers`
--

DROP TABLE IF EXISTS `job_cron_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `JOB_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_cron_triggers`
--

LOCK TABLES `job_cron_triggers` WRITE;
/*!40000 ALTER TABLE `job_cron_triggers` DISABLE KEYS */;
INSERT INTO `job_cron_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `CRON_EXPRESSION`, `TIME_ZONE_ID`) VALUES ('_job.scheduler','trg1q','WEBRATIO','00 00 1 * * ?','Europe/Berlin');
/*!40000 ALTER TABLE `job_cron_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_fired_triggers`
--

DROP TABLE IF EXISTS `job_fired_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_JOB_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_JOB_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_JOB_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_JOB_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_JOB_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_JOB_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_fired_triggers`
--

LOCK TABLES `job_fired_triggers` WRITE;
/*!40000 ALTER TABLE `job_fired_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_fired_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_job_details`
--

DROP TABLE IF EXISTS `job_job_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_JOB_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_JOB_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_job_details`
--

LOCK TABLES `job_job_details` WRITE;
/*!40000 ALTER TABLE `job_job_details` DISABLE KEYS */;
INSERT INTO `job_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`, `DESCRIPTION`, `JOB_CLASS_NAME`, `IS_DURABLE`, `IS_NONCONCURRENT`, `IS_UPDATE_DATA`, `REQUESTS_RECOVERY`, `JOB_DATA`) VALUES ('_job.scheduler','job17x','WEBRATIO','Reset Participation 7 Days','com.webratio.rtx.jobs.QuartzJob','0','0','0','0','��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\rjobInstanceIdt\0job17xx\0');
/*!40000 ALTER TABLE `job_job_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_locks`
--

DROP TABLE IF EXISTS `job_locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_locks`
--

LOCK TABLES `job_locks` WRITE;
/*!40000 ALTER TABLE `job_locks` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_locks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_paused_trigger_grps`
--

DROP TABLE IF EXISTS `job_paused_trigger_grps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_paused_trigger_grps`
--

LOCK TABLES `job_paused_trigger_grps` WRITE;
/*!40000 ALTER TABLE `job_paused_trigger_grps` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_paused_trigger_grps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_scheduler_state`
--

DROP TABLE IF EXISTS `job_scheduler_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_scheduler_state`
--

LOCK TABLES `job_scheduler_state` WRITE;
/*!40000 ALTER TABLE `job_scheduler_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_scheduler_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_simple_triggers`
--

DROP TABLE IF EXISTS `job_simple_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `JOB_SIMPLE_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_simple_triggers`
--

LOCK TABLES `job_simple_triggers` WRITE;
/*!40000 ALTER TABLE `job_simple_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_simple_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_simprop_triggers`
--

DROP TABLE IF EXISTS `job_simprop_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `JOB_SIMPROP_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_simprop_triggers`
--

LOCK TABLES `job_simprop_triggers` WRITE;
/*!40000 ALTER TABLE `job_simprop_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_simprop_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_triggers`
--

DROP TABLE IF EXISTS `job_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_JOB_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_JOB_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_JOB_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_JOB_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_JOB_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_JOB_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_JOB_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_JOB_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_JOB_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_JOB_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_JOB_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_JOB_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `JOB_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `job_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_triggers`
--

LOCK TABLES `job_triggers` WRITE;
/*!40000 ALTER TABLE `job_triggers` DISABLE KEYS */;
INSERT INTO `job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `JOB_NAME`, `JOB_GROUP`, `DESCRIPTION`, `NEXT_FIRE_TIME`, `PREV_FIRE_TIME`, `PRIORITY`, `TRIGGER_STATE`, `TRIGGER_TYPE`, `START_TIME`, `END_TIME`, `CALENDAR_NAME`, `MISFIRE_INSTR`, `JOB_DATA`) VALUES ('_job.scheduler','trg1q','WEBRATIO','job17x','WEBRATIO',NULL,1407538800000,-1,5,'WAITING','CRON',1407506294000,0,NULL,0,'');
/*!40000 ALTER TABLE `job_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `max_date_action_instance`
--

DROP TABLE IF EXISTS `max_date_action_instance`;
/*!50001 DROP VIEW IF EXISTS `max_date_action_instance`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `max_date_action_instance` (
  `action_type_oid` tinyint NOT NULL,
  `rank_oid` tinyint NOT NULL,
  `maxDate` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `mostimportant_badge`
--

DROP TABLE IF EXISTS `mostimportant_badge`;
/*!50001 DROP VIEW IF EXISTS `mostimportant_badge`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `mostimportant_badge` (
  `oid` tinyint NOT NULL,
  `rankoid` tinyint NOT NULL,
  `area` tinyint NOT NULL,
  `title` tinyint NOT NULL,
  `importance` tinyint NOT NULL,
  `checked_image_2` tinyint NOT NULL,
  `checked_imageblob` tinyint NOT NULL,
  `hd_checked_image_2` tinyint NOT NULL,
  `hd_checked_imageblob` tinyint NOT NULL,
  `sort_number` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `oid` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `code` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `delivery_date` timestamp NULL DEFAULT NULL,
  `language_code` varchar(255) DEFAULT NULL,
  `rank_oid` int(11) DEFAULT NULL,
  `reward_type_oid` int(11) DEFAULT NULL,
  `text_mail_oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `idx_notification_rank` (`rank_oid`),
  KEY `idx_notification_reward_type` (`reward_type_oid`),
  KEY `idx_notification_text_mail` (`text_mail_oid`),
  CONSTRAINT `fk_notification_rank` FOREIGN KEY (`rank_oid`) REFERENCES `community_user` (`oid`),
  CONSTRAINT `fk_notification_reward_type` FOREIGN KEY (`reward_type_oid`) REFERENCES `reward_type` (`oid`),
  CONSTRAINT `fk_notification_text_mail` FOREIGN KEY (`text_mail_oid`) REFERENCES `text_mail` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reward_instance`
--

DROP TABLE IF EXISTS `reward_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reward_instance` (
  `oid` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `score` decimal(19,2) DEFAULT NULL,
  `rank_oid` int(11) DEFAULT NULL,
  `reward_type_oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `idx_reward_instance_rank` (`rank_oid`),
  KEY `idx_reward_instance_reward_typ` (`reward_type_oid`),
  CONSTRAINT `fk_reward_instance_rank` FOREIGN KEY (`rank_oid`) REFERENCES `community_user` (`oid`),
  CONSTRAINT `fk_reward_instance_reward_type` FOREIGN KEY (`reward_type_oid`) REFERENCES `reward_type` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reward_instance`
--

LOCK TABLES `reward_instance` WRITE;
/*!40000 ALTER TABLE `reward_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `reward_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reward_type`
--

DROP TABLE IF EXISTS `reward_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reward_type` (
  `oid` int(11) NOT NULL,
  `needed_points` decimal(19,2) DEFAULT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `language_code` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `image_2` varchar(255) DEFAULT NULL,
  `imageblob` longblob,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reward_type`
--

LOCK TABLES `reward_type` WRITE;
/*!40000 ALTER TABLE `reward_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `reward_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteviewtable`
--

DROP TABLE IF EXISTS `siteviewtable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteviewtable` (
  `oid_2` int(11) NOT NULL,
  `moduledomainname` varchar(255) DEFAULT NULL,
  `siteviewid` varchar(255) DEFAULT NULL,
  `modulename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteviewtable`
--

LOCK TABLES `siteviewtable` WRITE;
/*!40000 ALTER TABLE `siteviewtable` DISABLE KEYS */;
INSERT INTO `siteviewtable` (`oid_2`, `moduledomainname`, `siteviewid`, `modulename`) VALUES (1,'community','sv2x','Community Administration'),(2,'community','sv1x','Community Private Area');
/*!40000 ALTER TABLE `siteviewtable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_chunk`
--

DROP TABLE IF EXISTS `text_chunk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_chunk` (
  `oid` int(11) NOT NULL,
  `languagecode` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_chunk`
--

LOCK TABLES `text_chunk` WRITE;
/*!40000 ALTER TABLE `text_chunk` DISABLE KEYS */;
INSERT INTO `text_chunk` (`oid`, `languagecode`, `key`, `message`) VALUES (1,'en','HomePage.GettingStarted.Intro','<h3>What is the Community?</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"background-color:rgb(255, 255, 255); color:rgb(102, 102, 102); font-family:avenir lt,lucida sans unicode,lucida grande,sans-serif; font-size:15px\">Our </span><strong>Community </strong><span style=\"background-color:rgb(255, 255, 255); color:rgb(102, 102, 102); font-family:avenir lt,lucida sans unicode,lucida grande,sans-serif; font-size:15px\">is the place where you can connect and share knowledge with users around the world. With our innovative game system you can challenge other users and rise Leaderboards, earn Points, and build your Reputation.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n'),(2,'en','HomePage.GettingStarted','<h3><span style=\"color:#78a02d\"><span style=\"font-size:13px\">You can enjoy the gaming experience and earn Points immediately.</span></span></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>What are the meanings of Participation and Reputation?</h3>\r\n\r\n<p>Participation is based on the Points gained doing activities and is related to your engagement with the Community. There are 2 participation leaderboards: 7 days and overall.</p>\r\n\r\n<p>Reputation is based on the Badges won and is related to the experience gained in the Community.</p>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<h3>What happens if I set my Community profile to private?</h3>\r\n\r\n<p>If you set your profile to private, you will no longer be visible in user rankings, your Points will be suspended, and you will not gain new Badges or Points until you will make your profile public again.</p>\r\n\r\n<p>&nbsp;</p>\r\n'),(3,'en','HomePage.PrivatePublicMessage','Set your profile public'),(4,'en','HQ.MonthlyWarning','No monthly action'),(5,'en','HQ.CongratulationsBadges','Congratulation!'),(6,'en','HQ.SessionOrNotAccess','Session expired'),(7,'en','HQ.NicknameNull','Nickname null'),(8,'en','NotPublicProfile.Message','<p>This profile is private. You can&#39;t see any user information.</p>\n'),(9,'en','Session expired','Session expired'),(10,'en','User not authorized or invalid credentials','User not authorized or invalid credentials'),(11,'en','Unable to compute the authorization signature','Unable to compute the authorization signature'),(12,'en','HomePage.SomethingMonthyMessage','<p>\n	No action yet. Do you want to become first?</p>\n'),(13,'it','HomePage.GettingStarted.Intro','<h3>Che cos&#39;&egrave; la Community ?</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"background-color:rgb(255, 255, 255); color:rgb(102, 102, 102); font-family:avenir lt,lucida sans unicode,lucida grande,sans-serif; font-size:15px\">La nostra&nbsp;</span><strong>Community</strong><span style=\"background-color:rgb(255, 255, 255); color:rgb(102, 102, 102); font-family:avenir lt,lucida sans unicode,lucida grande,sans-serif; font-size:15px\">&nbsp;&egrave; il luogo dove potrai entrare in contatto e condividere informazioni con utenti di tutto il mondo!&nbsp;Attraverso un innovativo sistema di </span><em>gamification</em><span style=\"background-color:rgb(255, 255, 255); color:rgb(102, 102, 102); font-family:avenir lt,lucida sans unicode,lucida grande,sans-serif; font-size:15px\">potrai sfidare gli altri utenti, scalare le classifiche, guadagnare Punti e costruire la tua reputazione.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n'),(14,'it','HomePage.GettingStarted','<h3 style=\"color:rgb(0, 51, 102); font-style:normal; text-align:start\"><span style=\"color:rgb(120, 160, 45)\">Partecipa alla nuova gaming experience e ottieni Punti immediatamente.</span></h3>\r\n\r\n<h3 style=\"color:rgb(0, 51, 102); font-style:normal; text-align:start\">&nbsp;</h3>\r\n\r\n<h3 style=\"color:rgb(0, 51, 102); font-style:normal; text-align:start\"><span style=\"color:#000000\">Che cosa si intende per Partecipazione e Reputazione?</span></h3>\r\n\r\n<p style=\"text-align:start\">La Partecipazione &egrave; basata sui Punti guadagnati svolgendo diverse attivit&agrave; all&rsquo;interno della Community, come la condivisione sui social network, o pi&ugrave; semplicemente l&rsquo;accesso all&rsquo;interno del proprio Account. Esistono due tipi di classifiche: classifica basata sugli ultimi 7 giorni e classifica generale.</p>\r\n\r\n<p style=\"text-align:start\">La Reputazione &egrave; basata sui badge accumulati ed &egrave; relativa alla propria esperienza all&rsquo;interno della Community.</p>\r\n\r\n<h3 style=\"color:rgb(0, 51, 102); font-style:normal; text-align:start\">&nbsp;</h3>\r\n\r\n<h3 style=\"color:rgb(0, 51, 102); font-style:normal; text-align:start\"><span style=\"color:#000000\">Che cosa accade se imposto il mio profilo privato?</span></h3>\r\n\r\n<p style=\"text-align:start\">Se imposti il tuo profilo privato, non sarai pi&ugrave; visibile nelle classifiche, i tuoi Punti saranno sospesi e non avrai pi&ugrave; la possibilit&agrave; di guadagnare nuovi badge fino a quando non imposterai nuovamente il tuo profilo pubblico.</p>\r\n\r\n<h3>&nbsp;</h3>\r\n'),(15,'it','HomePage.PrivatePublicMessage','<p>\n	Imposta il tuo profilo pubblico</p>\n'),(16,'it','HQ.MonthlyWarning','Nessuna azione mensile'),(17,'it','HQ.CongratulationsBadges','Congratulazioni!'),(18,'it','HQ.SessionOrNotAccess','<p>\n	Sessione scaduta</p>\n'),(19,'it','HQ.NicknameNull','<p>\n	Nickname non valido</p>\n'),(20,'it','NotPublicProfile.Message','<p>Questo &egrave; un profilo privato. Non &egrave; possibile visualizzare le informazioni dell&#39;utente.</p>\n'),(21,'it','Session expired','Sessione scaduta'),(22,'it','User not authorized or invalid credentials','<p>\n	User non autorizzato o credenziali non valide</p>\n'),(23,'it','Unable to compute the authorization signature','Unable to compute the authorization signature'),(24,'it','HomePage.SomethingMonthyMessage','Nessuna azione mensile. Vuoi essere il primo?'),(25,'fr','HomePage.GettingStarted.Intro','Introduction to the community'),(26,'fr','HomePage.GettingStarted','Introduction to the community'),(27,'fr','HomePage.PrivatePublicMessage','Set your profile public'),(28,'fr','HQ.MonthlyWarning','No monthly action'),(29,'fr','HQ.CongratulationsBadges','Congratulation!'),(30,'fr','HQ.SessionOrNotAccess','Session expired'),(31,'fr','HQ.NicknameNull','Nickname null'),(32,'fr','NotPublicProfile.Message','Private profile'),(33,'fr','Session expired','Session expired'),(34,'fr','User not authorized or invalid credentials','User not authorized or invalid credentials'),(35,'fr','Unable to compute the authorization signature','Unable to compute the authorization signature'),(36,'fr','HomePage.SomethingMonthyMessage','No action yet.Do you want become first?'),(37,'es','HomePage.GettingStarted.Intro','<h3>&iquest;Qu&eacute; es Community?</h3>\r\n\r\n<div>&nbsp;</div>\r\n'),(38,'es','HomePage.GettingStarted','<h3><span style=\"color:#78a02d\">La comunidad est&aacute; actualmente en versi&oacute;n Beta.</span></h3>\r\n\r\n<h3><span style=\"color:#78a02d\">Disfruta jugando y gana Puntos inmediatamente,</span></h3>\r\n\r\n<h3><span style=\"color:#78a02d\">Si encuentras bugs u otros problemas, por favor env&iacute;anos un correo a:&nbsp;</span></h3>\r\n\r\n<p>&nbsp;</p>\r\n'),(39,'es','HomePage.PrivatePublicMessage','<p>\n	Configures tu perfil p&uacute;blico</p>\n'),(40,'es','HQ.MonthlyWarning','<p>\n	Ninguna acci&oacute;n mensual</p>\n'),(41,'es','HQ.CongratulationsBadges','<p>\n	Felicitaciones!</p>\n'),(42,'es','HQ.SessionOrNotAccess','<p>\n	La sesi&oacute;n ha expirado</p>\n'),(43,'es','HQ.NicknameNull','<p>\n	Nickname&nbsp;no es v&aacute;lido</p>\n'),(44,'es','NotPublicProfile.Message','<p>Este es un perfil privado. No se puede mostrar la informaci&oacute;n del usuario.</p>\n'),(45,'es','Session expired','<p>\n	La sesi&oacute;n ha expirado</p>\n'),(46,'es','User not authorized or invalid credentials','<p>\n	Usuario no autorizado o credenciales no v&aacute;lidas</p>\n'),(47,'es','Unable to compute the authorization signature','Unable to compute the authorization signature'),(48,'es','HomePage.SomethingMonthyMessage','<p>\n	Ninguna acci&oacute;n! Quieres ser el primer?</p>\n'),(49,'it','community.text.rewards','<p>Qui puoi spendere i punti guadagnati per ottenere i tuoi premi!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"BootstrapStyle/img/profile_big.jpg\" /></p>\r\n'),(50,'en','community.text.rewards','<p>Here you can spend your earned points and get your rewards!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"BootstrapStyle/img/profile_big.jpg\" /></p>\r\n'),(51,'es','community.text.rewards',''),(52,'fr','community.text.rewards',''),(53,'it','KB_1',''),(54,'it','KB_2',''),(55,'it','KB_3',''),(56,'it','KB_4',''),(57,'it','KB_5',''),(58,'it','KB_6',''),(59,'it','KB_7',''),(60,'it','KB_8',''),(61,'it','KB_9',''),(62,'en','KB_1',''),(63,'en','KB_2',''),(64,'en','KB_3',''),(65,'en','KB_4',''),(66,'en','KB_5',''),(67,'en','KB_6',''),(68,'en','KB_7',''),(69,'en','KB_8',''),(70,'en','KB_9',''),(71,'es','KB_1',''),(72,'es','KB_2',''),(73,'es','KB_3',''),(74,'es','KB_4',''),(75,'es','KB_5',''),(76,'es','KB_6',''),(77,'es','KB_7',''),(78,'es','KB_8',''),(79,'es','KB_9',''),(80,'it','Forum_1',''),(81,'it','Forum_2',''),(82,'it','Forum_3',''),(83,'it','Forum_4',''),(84,'it','Forum_5',''),(85,'it','Forum_6',''),(86,'it','Forum_7',''),(87,'it','Forum_8',''),(88,'it','Forum_9',''),(89,'en','Forum_1',''),(90,'en','Forum_2',''),(91,'en','Forum_3',''),(92,'en','Forum_4',''),(93,'en','Forum_5',''),(94,'en','Forum_6',''),(95,'en','Forum_7',''),(96,'en','Forum_8',''),(97,'en','Forum_9',''),(98,'es','Forum_1',''),(99,'es','Forum_2',''),(100,'es','Forum_3',''),(101,'es','Forum_4',''),(102,'es','Forum_5',''),(103,'es','Forum_6',''),(104,'es','Forum_7',''),(105,'es','Forum_8',''),(106,'es','Forum_9',''),(107,'it','Store_1',''),(108,'it','Store_2',''),(109,'it','Store_3',''),(110,'it','Store_4',''),(111,'it','Store_5',''),(112,'it','Store_6',''),(113,'it','Store_7',''),(114,'it','Store_8',''),(115,'it','Store_9',''),(116,'en','Store_1',''),(117,'en','Store_2',''),(118,'en','Store_3',''),(119,'en','Store_4',''),(120,'en','Store_5',''),(121,'en','Store_6',''),(122,'en','Store_7',''),(123,'en','Store_8',''),(124,'en','Store_9',''),(125,'es','Store_1',''),(126,'es','Store_2',''),(127,'es','Store_3',''),(128,'es','Store_4',''),(129,'es','Store_5',''),(130,'es','Store_6',''),(131,'es','Store_7',''),(132,'es','Store_8',''),(133,'es','Store_9',''),(134,'it','Certification_1',''),(135,'it','Certification_2',''),(136,'it','Certification_3',''),(137,'it','Certification_4',''),(138,'it','Certification_5',''),(139,'it','Certification_6',''),(140,'it','Certification_7',''),(141,'it','Certification_8',''),(142,'it','Certification_9',''),(143,'en','Certification_1',''),(144,'en','Certification_2',''),(145,'en','Certification_3',''),(146,'en','Certification_4',''),(147,'en','Certification_5',''),(148,'en','Certification_6',''),(149,'en','Certification_7',''),(150,'en','Certification_8',''),(151,'en','Certification_9',''),(152,'es','Certification_1',''),(153,'es','Certification_2',''),(154,'es','Certification_3',''),(155,'es','Certification_4',''),(156,'es','Certification_5',''),(157,'es','Certification_6',''),(158,'es','Certification_7',''),(159,'es','Certification_8',''),(160,'es','Certification_9',''),(161,'it','actions_KB',''),(162,'it','actions_Forum',''),(163,'it','actions_Store',''),(164,'it','actions_Certification',''),(165,'en','action_KB',''),(166,'en','actions_Forum',''),(167,'en','actions_Store',''),(168,'en','actions_Certification',''),(169,'es','actions_KB',''),(170,'es','actions_Forum',''),(171,'es','actions_Store',''),(172,'es','actions_Certification','');
/*!40000 ALTER TABLE `text_chunk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_mail`
--

DROP TABLE IF EXISTS `text_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_mail` (
  `oid` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `language_code` varchar(255) DEFAULT NULL,
  `body` text,
  `description` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `containers_oid_header` int(11) DEFAULT NULL,
  `containers_oid_footer` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `idx_text_mail_containers_mail` (`containers_oid_header`),
  KEY `idx_text_mail_containers_mai_2` (`containers_oid_footer`),
  CONSTRAINT `fk_text_mail_containers_mail` FOREIGN KEY (`containers_oid_header`) REFERENCES `containers_mail` (`oid`),
  CONSTRAINT `fk_text_mail_containers_mail_2` FOREIGN KEY (`containers_oid_footer`) REFERENCES `containers_mail` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_mail`
--

LOCK TABLES `text_mail` WRITE;
/*!40000 ALTER TABLE `text_mail` DISABLE KEYS */;
INSERT INTO `text_mail` (`oid`, `code`, `language_code`, `body`, `description`, `subject`, `containers_oid_header`, `containers_oid_footer`) VALUES (1,'community.registration','it','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>congratulations, you are now a member of the Community!</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Registration into the community','Welcome into the Community!',NULL,NULL),(2,'community.registration','en','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>congratulations, you are now a member of the &nbsp;Community!</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Now you can:</p>\r\n\r\n<ul>\r\n	<li>earn Points&nbsp;for your participation and spend them to receive gift</li>\r\n	<li>earn Badges&nbsp;for you reputation</li>\r\n	<li>appear in the Leaderboards</li>\r\n	<li>access to all of your dashboard&nbsp;features</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Registration into the community','Welcome into the Community!',NULL,NULL),(3,'community.registration','es','','Registration into the community','Welcome into the Community!',NULL,NULL),(4,'community.registration','fr','','Registration into the community','Welcome into the Community!',NULL,NULL),(5,'community.publicProfile','it','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>congratulations, you are now a member of the Community!</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Now you can:</p>\r\n\r\n<ul>\r\n	<li>earn Points&nbsp;for your participation and spend them to receive gift</li>\r\n	<li>earn Badges&nbsp;for you reputation</li>\r\n	<li>appear in the Leaderboards</li>\r\n	<li>access to all of your Dashboard&nbsp;features</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Change on profile visibility in public','Welcome into the Community!',NULL,NULL),(6,'community.publicProfile','en','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>congratulations, you are now a member of the Community!</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Now you can:</p>\r\n\r\n<ul>\r\n	<li>earn Points&nbsp;for your participation and spend them to receive gift</li>\r\n	<li>earn Badges&nbsp;for you reputation</li>\r\n	<li>appear in the Leaderboards</li>\r\n	<li>access to all of your Dashboard&nbsp;features</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Change on profile visibility in public','Welcome into the Community!',NULL,NULL),(7,'community.publicProfile','es','','Change on profile visibility in public','Welcome into the Community!',NULL,NULL),(8,'community.publicProfile','fr','','Change on profile visibility in public','Welcome into the Community!',NULL,NULL),(9,'community.privateProfile','it','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>we are sorry you decided to leave our Community.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We remember you that <strong>your Points will be suspended</strong>, and you will not gain new Badges or Points until you will make your profile public again.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We hope to see you come back again.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Change on profile visibility in private','Leave the Community',NULL,NULL),(10,'community.privateProfile','en','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>we are sorry you decided to leave our Community.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We remember you that <strong>your Points will be suspended</strong>, and you will not gain new Badges or Points until you will make your profile public again.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We hope to see you come back again.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Change on profile visibility in private','Leave the Community',NULL,NULL),(11,'community.privateProfile','es','','Change on profile visibility in private','Leave the Community',NULL,NULL),(12,'community.privateProfile','fr','','Change on profile visibility in private','Leave the Community',NULL,NULL),(13,'community.newBadge','it','','New badge acquired','Got New Badge',NULL,NULL),(14,'community.newBadge','en','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Congratulations for your Badge!</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>$$badge$$</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"margin-left:150px\">Get more Badges doing different actions, for instance:</p>\r\n\r\n<p>$$other_actions$$</p>\r\n\r\n<p>&nbsp;</p>\r\n','New badge acquired','Got New Badge',NULL,NULL),(15,'community.newBadge','es','','New badge acquired','Got New Badge',NULL,NULL),(16,'community.newBadge','fr','','New badge acquired','Got New Badge',NULL,NULL),(17,'community.newReward','it','','New reward available','New Reward Available',NULL,NULL),(18,'community.newReward','en','','New reward available','New Reward Available',NULL,NULL),(19,'community.newReward','es','','New reward available','New Reward Available',NULL,NULL),(20,'community.newReward','fr','','New reward available','New Reward Available',NULL,NULL),(21,'community.newReward.onlyOne','it','','New reward available','New Reward Available',NULL,NULL),(22,'community.newReward.onlyOne','en','','New reward available','New Reward Available',NULL,NULL),(23,'community.newReward.onlyOne','es','','New reward available','New Reward Available',NULL,NULL),(24,'community.newReward.onlyOne','fr','','New reward available','New Reward Available',NULL,NULL),(25,'community.monthlySummary','it','<p>Hi $$firstname$$!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>here you find what happend in the Community during the last month!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>You earned $$seven_days_points$$ Points<br />\r\nYour total number of Points is $$overall_points$$<br />\r\nYou are $$seven_days_position$$&deg; in 7 Days Leaderboard<br />\r\nYou are $$overall_position$$&deg; in Overall Leaderboard</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>LEADERBOARDS</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>$$leaderboards$$</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n','Monthly summary','Monthly Summary',NULL,NULL),(26,'community.monthlySummary','en','<p>Hi $$firstname$$!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>here you find what happend in the Community during the last month!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>You earned $$seven_days_points$$ Points<br />\r\nYour total number of Points is $$overall_points$$<br />\r\nYou are $$seven_days_position$$&deg; in 7 Days Leaderboard<br />\r\nYou are $$overall_position$$&deg; in Overall Leaderboard</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>LEADERBOARDS</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>$$leaderboards$$</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n','Monthly summary','Monthly Summary',NULL,NULL),(27,'community.monthlySummary','es','','Monthly summary','Monthly Summary',NULL,NULL),(28,'community.monthlySummary','fr','','Monthly summary','Monthly Summary',NULL,NULL),(29,'community.monthlySummary.noSevenDays','it','<p>Hi $$firstname$$!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>here you find what happend in the Community during the last month!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>You earned $$seven_days_points$$ Points<br />\r\nYour total number of Points is $$overall_points$$<br />\r\nYou are $$overall_position$$&deg; in Overall Leaderboard</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>LEADERBOARDS</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>$$leaderboards$$</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n','Monthly summary','Monthly Summary',NULL,NULL),(30,'community.monthlySummary.noSevenDays','en','<p>Hi $$firstname$$!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>here you find what happend in the Community during the last month!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>You earned $$seven_days_points$$ Points<br />\r\nYour total number of Points is $$overall_points$$<br />\r\nYou are $$overall_position$$&deg; in Overall Leaderboard</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>LEADERBOARDS</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>$$leaderboards$$</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Monthly summary','Monthly Summary',NULL,NULL),(31,'community.monthlySummary.noSevenDays','es','<p>Hi $$firstname$$!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>here you find what happend in the Community during the last month!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>You earned $$seven_days_points$$ Points<br />\r\nYour total number of Points is $$overall_points$$<br />\r\nYou are $$overall_position$$&deg; in Overall Leaderboard</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>LEADERBOARDS</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>$$leaderboards$$</p>\r\n\r\n<p>&nbsp;</p>\r\n','Monthly summary','Monthly Summary',NULL,NULL),(32,'community.monthlySummary.noSevenDays','fr','','Monthly summary','Monthly Summary',NULL,NULL);
/*!40000 ALTER TABLE `text_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thematic_area`
--

DROP TABLE IF EXISTS `thematic_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thematic_area` (
  `oid` int(11) NOT NULL,
  `area_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thematic_area`
--

LOCK TABLES `thematic_area` WRITE;
/*!40000 ALTER TABLE `thematic_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `thematic_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `internal` tinyint(1) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `groupoid` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `idx_usertable_grouptable` (`groupoid`),
  CONSTRAINT `fk_usertable_grouptable` FOREIGN KEY (`groupoid`) REFERENCES `grouptable` (`oid_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `email`, `password`, `internal`, `username`, `groupoid`) VALUES (1,'admin@admin.com','admin123admin',1,'admin',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_grouptable`
--

DROP TABLE IF EXISTS `user_grouptable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_grouptable` (
  `useroid` int(11) NOT NULL,
  `groupoid` int(11) NOT NULL,
  PRIMARY KEY (`useroid`,`groupoid`),
  KEY `idx_user_grouptable_usertable` (`useroid`),
  KEY `idx_user_grouptable_grouptable` (`groupoid`),
  CONSTRAINT `fk_user_grouptable_grouptable` FOREIGN KEY (`groupoid`) REFERENCES `grouptable` (`oid_2`),
  CONSTRAINT `fk_user_grouptable_usertable` FOREIGN KEY (`useroid`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_grouptable`
--

LOCK TABLES `user_grouptable` WRITE;
/*!40000 ALTER TABLE `user_grouptable` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_grouptable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `user_information`
--

DROP TABLE IF EXISTS `user_information`;
/*!50001 DROP VIEW IF EXISTS `user_information`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `user_information` (
  `oid` tinyint NOT NULL,
  `country` tinyint NOT NULL,
  `area_geografica` tinyint NOT NULL,
  `small_photo` tinyint NOT NULL,
  `big_photo` tinyint NOT NULL,
  `first_name` tinyint NOT NULL,
  `last_name` tinyint NOT NULL,
  `twitter` tinyint NOT NULL,
  `linkedin` tinyint NOT NULL,
  `website` tinyint NOT NULL,
  `bio` tinyint NOT NULL,
  `city` tinyint NOT NULL,
  `company_name` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `internal` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `action_instance_action_area_vi`
--

/*!50001 DROP TABLE IF EXISTS `action_instance_action_area_vi`*/;
/*!50001 DROP VIEW IF EXISTS `action_instance_action_area_vi`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `action_instance_action_area_vi` AS select `al1`.`oid` AS `oid`,`al2`.`area` AS `der_attr` from (`action_instance` `al1` left join `action_type` `al2` on((`al1`.`action_type_oid` = `al2`.`oid`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `action_instance_daily_vi`
--

/*!50001 DROP TABLE IF EXISTS `action_instance_daily_vi`*/;
/*!50001 DROP VIEW IF EXISTS `action_instance_daily_vi`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `action_instance_daily_vi` AS select `action_instance`.`action_type_oid` AS `action_type_oid`,cast(`action_instance`.`date` as date) AS `date`,count(0) AS `daily_occurrence` from `action_instance` group by `action_instance`.`action_type_oid`,cast(`action_instance`.`date` as date) order by `action_instance`.`action_type_oid`,cast(`action_instance`.`date` as date) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `action_instance_name_view`
--

/*!50001 DROP TABLE IF EXISTS `action_instance_name_view`*/;
/*!50001 DROP VIEW IF EXISTS `action_instance_name_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `action_instance_name_view` AS select `al1`.`oid` AS `oid`,`al2`.`name` AS `der_attr` from (`action_instance` `al1` left join `action_type` `al2` on((`al1`.`action_type_oid` = `al2`.`oid`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `badgeimportancebyuser`
--

/*!50001 DROP TABLE IF EXISTS `badgeimportancebyuser`*/;
/*!50001 DROP VIEW IF EXISTS `badgeimportancebyuser`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `badgeimportancebyuser` AS select `m`.`oid` AS `badge_instance`,`q`.`oid` AS `user`,`c`.`area` AS `nickname_area`,max(`c`.`importance`) AS `importance` from ((`badge_type` `c` join `badge_instance` `m`) join `community_user` `q`) where ((`m`.`badge_type_oid` = `c`.`oid`) and (`q`.`oid` = `m`.`rank_oid`)) group by `q`.`oid`,`c`.`area` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `badgetype_sortco`
--

/*!50001 DROP TABLE IF EXISTS `badgetype_sortco`*/;
/*!50001 DROP VIEW IF EXISTS `badgetype_sortco`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `badgetype_sortco` AS select `al1`.`oid` AS `oid`,(`al2`.`sort_number` or `al3`.`sort_number`) AS `der_attr` from ((`badge_type` `al1` join `badge_type` `al2`) join `badge_type` `al3`) where ((`al2`.`key` = 'area') and (`al1`.`area` = `al2`.`area`) and (`al3`.`key` = 'level') and (`al1`.`importance` = `al3`.`sort_number`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `community_user_credits_availab`
--

/*!50001 DROP TABLE IF EXISTS `community_user_credits_availab`*/;
/*!50001 DROP VIEW IF EXISTS `community_user_credits_availab`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `community_user_credits_availab` AS select `al1`.`oid` AS `oid`,(case when isnull((`al1`.`credit` - `al2`.`der_attr`)) then 0 else (`al1`.`credit` - `al2`.`der_attr`) end) AS `der_attr` from (`community_user` `al1` left join `community_user_credits_spent_v` `al2` on((`al1`.`oid` = `al2`.`oid`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `community_user_credits_spent_v`
--

/*!50001 DROP TABLE IF EXISTS `community_user_credits_spent_v`*/;
/*!50001 DROP VIEW IF EXISTS `community_user_credits_spent_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `community_user_credits_spent_v` AS select `al1`.`oid` AS `oid`,(case when isnull(sum(`al2`.`score`)) then 0 else sum(`al2`.`score`) end) AS `der_attr` from (`community_user` `al1` left join `reward_instance` `al2` on((`al1`.`oid` = `al2`.`rank_oid`))) group by `al1`.`oid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `headquarter_user_partecipation`
--

/*!50001 DROP TABLE IF EXISTS `headquarter_user_partecipation`*/;
/*!50001 DROP VIEW IF EXISTS `headquarter_user_partecipation`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `headquarter_user_partecipation` AS select `al1`.`oid` AS `oid`,sum(`al2`.`score`) AS `partecipation` from ((`community_user` `al1` join `action_instance` `al2`) join `action_type` `al3`) where ((`al3`.`participation` = 1) and (`al1`.`oid` = `al2`.`rank_oid`) and (`al2`.`action_type_oid` = `al3`.`oid`)) group by `al1`.`oid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `headquarter_user_participation_monthly`
--

/*!50001 DROP TABLE IF EXISTS `headquarter_user_participation_monthly`*/;
/*!50001 DROP VIEW IF EXISTS `headquarter_user_participation_monthly`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `headquarter_user_participation_monthly` AS select `r`.`oid` AS `oid`,sum(`al2`.`score`) AS `participation_monthly` from (`action_instance` `al2` join `community_user` `r` on((`r`.`oid` = `al2`.`rank_oid`))) where ((month(`al2`.`date`) = month(now())) and (year(`al2`.`date`) = year(now()))) group by `r`.`oid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `headquarter_user_participation_seven_days`
--

/*!50001 DROP TABLE IF EXISTS `headquarter_user_participation_seven_days`*/;
/*!50001 DROP VIEW IF EXISTS `headquarter_user_participation_seven_days`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `headquarter_user_participation_seven_days` AS select `r`.`oid` AS `oid`,sum(`al3`.`score`) AS `participation_seven_days` from ((`community_user` `r` left join `action_instance` `al3` on((`r`.`oid` = `al3`.`rank_oid`))) left join `action_type` `al4` on((`al3`.`action_type_oid` = `al4`.`oid`))) where ((`al3`.`date` <= now()) and (`al3`.`date` >= (now() - interval 7 day)) and (`al4`.`participation` = 1)) group by `r`.`oid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `max_date_action_instance`
--

/*!50001 DROP TABLE IF EXISTS `max_date_action_instance`*/;
/*!50001 DROP VIEW IF EXISTS `max_date_action_instance`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `max_date_action_instance` AS select `action_instance`.`action_type_oid` AS `action_type_oid`,`action_instance`.`rank_oid` AS `rank_oid`,max(`action_instance`.`date`) AS `maxDate` from `action_instance` group by `action_instance`.`action_type_oid`,`action_instance`.`rank_oid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `mostimportant_badge`
--

/*!50001 DROP TABLE IF EXISTS `mostimportant_badge`*/;
/*!50001 DROP VIEW IF EXISTS `mostimportant_badge`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `mostimportant_badge` AS select `badge`.`badge_instance` AS `oid`,`rr`.`oid` AS `rankoid`,`dict`.`area` AS `area`,`dict`.`title` AS `title`,`badge`.`importance` AS `importance`,`dict`.`checked_image_2` AS `checked_image_2`,`dict`.`checked_imageblob` AS `checked_imageblob`,`dict`.`hd_checked_image_2` AS `hd_checked_image_2`,`dict`.`hd_checked_imageblob` AS `hd_checked_imageblob`,`dict`.`sort_number` AS `sort_number` from ((`badgeimportancebyuser` `badge` join `community_user` `rr`) join `badge_type` `dict`) where ((`dict`.`area` = `badge`.`nickname_area`) and (`dict`.`importance` = `badge`.`importance`) and (`rr`.`oid` = `badge`.`user`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_information`
--

/*!50001 DROP TABLE IF EXISTS `user_information`*/;
/*!50001 DROP VIEW IF EXISTS `user_information`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */

/*!50001 VIEW `user_information` AS select `r1`.`oid` AS `oid`,`r1`.`country` AS `country`,`r1`.`geographical_area` AS `area_geografica`,`r1`.`small_photo` AS `small_photo`,`r1`.`big_photo` AS `big_photo`,`r1`.`first_name` AS `first_name`,`r1`.`last_name` AS `last_name`,`r1`.`twitter` AS `twitter`,`r1`.`linkedin` AS `linkedin`,`r1`.`website` AS `website`,`r1`.`bio` AS `bio`,`r1`.`city` AS `city`,`r1`.`company_name` AS `company_name`,`c1`.`email` AS `email`,`c1`.`internal` AS `internal` from (`community_user` `r1` join `user` `c1` on((`c1`.`user_id` = `r1`.`oid`))) where (`r1`.`public_profile` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-08 17:01:16

-- Goal_Badge Type [rel3z]
alter table `goal`  add column  `badge_type_oid`  integer;
alter table `goal`   add index fk_goal_badge_type (`badge_type_oid`), add constraint fk_goal_badge_type foreign key (`badge_type_oid`) references `badge_type` (`oid`);



#input Integer[] ids, String[] titles, RTXBLOBData[] images , Double[] consumptions, String[] badge_titles
#output String json

import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.io.ByteArrayOutputStream
import org.apache.commons.io.IOUtils
import org.apache.commons.codec.binary.Base64OutputStream

def json = null

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);

    jGenerator.writeStartArray(); 
	titles.eachWithIndex{obj, idx ->
	    jGenerator.writeStartObject();
	    
	    
	    def bytes = null
		def inStream = null
		
		try {
		    inStream = images[idx].openFileInputStream()
		    bytes = org.apache.commons.io.IOUtils.toByteArray(inStream)
		} finally {
		    org.apache.commons.io.IOUtils.closeQuietly(inStream)
		}

		def base64 = new org.apache.commons.codec.binary.Base64()
		def base64String = new String(base64.encode(bytes), "US-ASCII") 
	    jGenerator.writeStringField("image", base64String);
	    jGenerator.writeNumberField("id", ids[idx]);
	    jGenerator.writeStringField("title", titles[idx]); 
	    jGenerator.writeNumberField("score", consumptions[idx]);
	    jGenerator.writeStringField("badge_title", badge_titles[idx]);
	    jGenerator.writeEndObject(); 
	}
	 jGenerator.writeEndArray();

    jGenerator.close();
 
    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

return ["json": json]
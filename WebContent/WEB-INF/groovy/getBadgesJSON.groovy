#input user, Integer[] areasId, String[] areasName, Integer[] badge_ids, String[] badge_area_names, String[] badge_titles, Double[] badge_scores, RTXBLOBData[] badge_images, Integer[] badge_area_ids
#output String json

import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import java.io.OutputStream
import java.io.ByteArrayOutputStream


Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query1 = null
/*
query1 = "select a.area_name, t.oid, t.title, t.needed_score, t.hd_checked_imageblob, a.oid from badge_type as t join thematic_area as a on t.thematic_area_oid = a.oid where t.title is not null and t.area <> 'Consumption Reduction' order by a.oid asc, t.area asc, t.needed_score asc"
def result=null

def jsonString = null

def ris=0
result = session.createSQLQuery(query1).list()
OutputStream out = null;
JsonFactory jfactory = null;
JsonGenerator jGenerator = null;
try {
	out = new ByteArrayOutputStream()
	jfactory = new JsonFactory();
	jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);

	String str = "";
	String img64str = "null";
	String area = "";
	Integer areaId = null;
	String prev_area="";
	Double prev_score=0.0;
	def rs = null;
	jGenerator.writeStartObject();
	jGenerator.writeFieldName("badges_list");
    jGenerator.writeStartArray();
	int i = 0;
	for (Iterator iter = result.iterator(); iter.hasNext();) 
    {
    	rs = iter.next(); 
		area = rs[0];
		areaId = rs[5];
		
		if(!area.equals(prev_area)){
			if(i!=0){
				jGenerator.writeEndArray();
				jGenerator.writeNumberField("max", prev_score) 
				jGenerator.writeEndObject();
			}

			jGenerator.writeStartObject();
			jGenerator.writeStringField("area", area) 
			jGenerator.writeNumberField("area_id", areaId)
			prev_area = area;
			
			jGenerator.writeFieldName("badges");
			jGenerator.writeStartArray();
		}
		
		jGenerator.writeStartObject();
		jGenerator.writeNumberField("id", rs[1]) 
		jGenerator.writeStringField("title", rs[2])
		prev_score = rs[3]
		jGenerator.writeNumberField("score", prev_score)
		def bytes = null
		def inStream = null
		
		try {
		println rs[4].getClass()
		    inStream = rs[4].openFileInputStream()
		    println "ok1"
		    bytes = org.apache.commons.io.IOUtils.toByteArray(inStream)
		    println "ok2"
		} finally {
		    org.apache.commons.io.IOUtils.closeQuietly(inStream)
		    println "ok3"
		}

		def base64 = new org.apache.commons.codec.binary.Base64()
		println "ok4"
		def base64String = new String(base64.encode(bytes), "US-ASCII") 
		println "ok5"
	    jGenerator.writeStringField("icon", base64String);
	    println "ok6"
	
	//	jGenerator.writeStringField("icon", "iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAnqklEQVR4nO19eZxcR3Xud6rq3l5n14w02mXJWi0sxZYN2PIzWF4D2GATzPslEEjCC8QkYJYHAh558MBJDARwQjCER17gkR9gg41XWd5tDN6EjI1ky9pH0kij2ad7uvveqjrvj6rb3SMbAy9G0/LMp99VTy/39r33fHXq1HdOVRMzYxpTF2KyT2Aak4tpAkxxTBNgimOaAFMc0wSY4pgmwBTHNAGmOKYJMMUxTYApjmkCTHFME2CKY5oAUxzTBJjimCbAFMc0AaY4pgkwxTFNgCmOaQJMcUwTYIpjmgBTHNMEmOKYJsAUxzQBpjimCTDFMU0AB/LblMMUIwD/uutlv/0u+7wi8Iq+uBeCLMD1Ld21/Lh4MkzUVffaMfu8cvFKJAAd02qdQa2ei2jsnQAxkutmKwCw2XPP3/Ghx98BMMBWut2YACYcfPw9sFq94FuYBV4B3cYrkQAMkPUGApJrJJqlH/q7r/N4/yowW7BVABkAM8wvv/cG/cz3rwSIABj/Htu9D747fvxfvgQSCQGcwdkKEFngxJ9Z+woigDf4aM9FKA+fDiJvZG8jXVlsnr81rX/2j98EkQQbASLw4M7/ag//MjQHHl2L4pEzQIIBGMSlOfqnX7jW9m/PoTI2330FE9gqkLB86IlLeXjfsurrJyhOZAJMdPXsr0VHs+PNH93EgztPBQkNqwMAsKM9a9laNr/87mvs3vs/CRFEAELz3M1/jqgAFI4I8/wd73FHFmyevP4b5vCWNugy82jPSvcdJgAJbffef2V836dvJKHC2rkk52FPqHt6Qp3sMfCu3sjacwBB5qg98nR7dNOf3sUjPa+GCMpgFhjctQpRgSDDOL73U5/i8vDJMJVXmR13rIZKGwgJs/2mtwJQfPCxP42f/OYllG6tcGWUeHj/KWCWEKpin7v1ndFP3vMfiIoxZDhcdzoEthIkTqig8cQjgHe3XB6eg/H+uSBpXH/vW55KG8p1sh3c1RXf/K5NPLjzLBBZe3TbSrYaCJtgh/ZI8+h137b7Ht7IQ7uAIAUEGbYDzzbxrs1f1Y9e93noIkMGAjaGHdzxKhAZ++zNfx3f8/F/A2tApQ2s9udkJEAMEoYHdqxAebTJv9HwXcOJRwDvbok5qx/50iNm2w3vAZEFyRhsJVSmDGaisCm2g7ubo5vedTsP734PF4+0gwDocUVSsd76b2fF92x8M9gCcUlClwkm4uieT7zX9DzSDVIEE0kGgLFDi+yO266O7v7vX2HWBiQYYV4j01YBs4BQMXQ5qx+65h/MI1/+DoJ0hBNkhPDC4U2jg8gCIGTanqd060B821XX2z33Xa7O+sjHqXXhFgAlCAmwUci0Mo8eaI5+9M7rYWNLIgTluojy3aDcDCaVA6XyBBJgawBdIi4NW0RjxKUB4tKQgIlh9v30NL3r7tPBzJBpgo4IQgEqUwBg7Z773qh//pXP2wM/OyU459PXQ4YVHyzqSb5bvxEnDgGYpev2yYJZgoSmWac+gPzM1fq5my+wB35+rjrjqs/IlVc8SEHegg2hNEhQWaZUM8ScdULMPBUi2wHIEGBLsBqsy4CJwFYDNgasFjAxoMtAZQxc7Icd3kN2tMeyrgiYCgAGhblRjB2ap3/+1U/oZ3/8J7AalJ9lxMzV9/sTTkYNEkJaAA0ZG9AJsUiUiSVkYKrPbZwCyZgHdl4R3/aX37eF3gjMIeIi5IJz++1YbxuP7pdy8YWQSy6A6FgKFgJUKQJRETBlsIkBEzujm9gTQAMmAlgD1rjNx5YclcCFQ7ADz8MO74FoXVQAkbZHt7ci3WxgNImmOTq8/DsrkOvcV+0aqtcQScjQHHtpk43G9gDMAkSWxw6eZJ+/4+M0c/X3xOx1D0ClKgBAzd2PUfO8CMN7Q6SaGSCr99w3Qy3eAHnOx0DtSwBTAUcFQEfeqNrpN+zl/+QRFmDrem6Gf88ARjtCsAXlZkFmZkC0LYbpezrPhV4g3WpARLBlQc3zdiE3c6+LSWBQ7Gsxex+8hI88vV6e8b6PUH7muDs4NUyra2wCOHcvqHneTowPLo5uevdmMWPpdrnk4pvFovNuoM7lT4rutVtMzyOvRqVgKdcpg7M+wmLheoKJgfF+ZzhmwFpn/IQAQgDkLp+JvAhIzlGzrZGEEo4wYCOADSjTDjX3LNjRfWz6n5WwsQaREN1rN0GX2R587EK7+94rzL4HLuKh3XOD1374GsrPLDZiXND4XYAbWxsMPL8uuvODP7d9T7uRS6oJcs6ZD1HzvBn66e+tEF2rOTjjfUT5ma7FAwAIxL7VEwEkAB2BK6Pg0gBQGgZHBbCNndEhQCoEZAiSXuNhA9ZRHXms7zosWAhweRi275fMukJq/tkP2ZH9rbZ/+2q2GkQKomPp4fDN/7YU2Y6C8zCioW54g3oAJq/le7HHSrQvflwuet1mO7z7fAgVw5qU2XP/ejYx1LI/RHDqHxNAwHi/k+6ZnbGEBGQAjA/A9u9wfXjhsAvwEm8A+LygdPvKFCjMgdItoEwbEGS9h4AnCsBgQFdAQRZy5h+QGXwWetem9RASCLJMQkSISim57E3XI9sxBjYKEMYlmxgg2RDxQON7AAcJZvDRba+P7/jAXXZ0v4XKEOJxqxZfINSqPyKYCAA7gyf6v8qAi0dhDz4G07sVXB5275Fy3oBeYqhOBCIJqAwo5YhAKa/vmBjMPkhk7UMIAzu009rxPkAogqkQ5ecWw7f8n5Mp39XnE08Nh8byAMwEIubhvV08tOcsapr9HDXNPgyVHoIMmLpWbZbLL/2JfeLrb0JcNHL+2VKuuBQcjQIggKRz00IBRLB77ofZ96AzvHCuve7Lan8S+dgMyX/+dCwQF8G6DIpGQek2ULq11j2AfdbYB4mti4SwGlwZ1mBSas07PkP5mb1gC+hSwKXhNh7vm8uDu+bKRa+/C5m2ct0XTwoaiwAEgFlQuq1gdn3jvXrHbedTruMohS191DLvEOVnPUthPqJUCyjdSmr5pUBlFIBwLd/GgMoAlVHo526BPbodkClAZeGju9oXkaj/UgAMgqyzRb1RLBCN+6FjBE63glSqbgThyQIGtS4A95clpdKWRw/M13d99F+50LeEi4fncFzs4LHDbWrtu6/B8jfdWo1vJhGN1wUkXmC8v0lv/viTZu99JzulLgb5Vk75LgRr3wWkml1UTgoAAyoNHh+A3vZj8HifN3x9tReBiHyb85G/1UCmHWLmatg9DwBVw6K6T5ULLogD0m0QmXb3N1vAGkcA9rpBVIId3QtUxmr7yADQZYhF5/0ouOSrl0NIURt3Th4aLxdAxGArKTtjTL326j8SM1aMQ4Yxpds00q0aMrRy4blAmAPicXfzTQQmAS4cgX76+y7CV1kfsHlzkwCRAPu/XQwgAWsgO1dCdq8Fsh2uXyfp30/iBL+RdI+lIXBpyL/H4KonYLe/SoPSHUCYNUi3aKSaY5Bg6li2Q63/2LshpABbnmzjA41GALYCbCSYAROlqXPFVnXan3+AwqYANhbQZSVmLBWi42SgUnBjexMDsEB5BHr7zeBozLl9WBfIoWZ4F9gJuMsWzliZdlDnCrCpQHat8gQQL70JCS4PuthCplEVlBLPYWNQugVQGQm2EjYOIEKt1n/8j6ll3hhMFIDZX+vk1g80FgFIWJA0ENJAhmUAEMsv/aY67T3/QCoroEIjZ5/uVD32Cp11sq3ZdY9r+d745Ft6IvLUDO9dv48ZxMzVQJAGKgVQ8xznBTjxAi+xiRA8PgDoEijM19x/VUUEKN3uxv0iiNRrr75SzHv14wAsZFiBkAYkzWTXDzROEBiPS3v02fWojGS40NcGohmIxjp4eG87jJ6FMMciPUdSph3Q4y7ShwGCDOyRZ2CH9gBhxqt3dS2+vuS/3p0nfX/nMpcf8Ll92X4STO9T7vh+zP8CT10NKQh2rBdyxlIgyIArY7WPsAVkmqEyAjKM7KEnLrGHtpwjOpcOgNQAwH3UuqiIMHdEdK/ZApJUd+TjhsYgAFsBkswHHl2rt3zrS1wedC00CaBIADKEmnMGoCuutTE7I5WGYA4/BajQd/fOwDSh766f9+FbfxxBdK0CROBGEmwAHYHyXUCmFYjLdZrCsZXidXayGrZ4BKJ5PrhSqJ2bsyVRkAOXBvJ2111/BmbYHbc4gpoKSATj6vWfuRzdawlsaTJUwkYaBQgA1u594ML4/s9+l0t9M3w3oMhoomynVIs3wPlW78pVGubwU7B920BBBk6fS9z9i7T6RA5mBoI01NJLahp/MsQDwKOHYAd3ASp9zIjgWCR9voboXA7E4+Cxw2CSnghuZMClIVRTiyQ1KqNpaup+Sr3+s1eI7rU7nZgwOQmiRooBLNgqsfC/bArP//vXi/YVzyMqpomtBFtJTd3OOEbX0rTRKOzwfkCFPgYXNeMnxoaP9qVyHkMEzmAdS9z7puRSwDZ2RovLoHQzKMg6uwhV2ybEAWLCcy72Q+Q6AchaMglcTyAJQKE0mBYzV98ZXPyV14nutTthfTnZJKGBCMAAYGGiFM057eng4n98jVryhpshUhYysJTpcK2UDdjEruGO9gLxuNPvqX5450nAPs1rtQscTeSCtnQzRPNcICoA2tcFmNgJPT4xRJkOF82zAbF1PkQIkJB+U+6RBEiGrv8nAWSafTzhbWpiF6yStAAq8uSLrw3e9I2LqX3xCEwUuiBw8rxwY8QAAHyrdREyAEo1jYvF533DDjy3nqORNlIZhomo2rqthi0cqRrbVYoaNzQk6VpskAKpjNPzwywQ5kAq46L2uuweJ3UC1tRqAIIMRNNMsK7UjMgWSZdJiUbgFUWyGhwVQbkucOGIO4eke7HWQoAo075DnHzRd7gyFlC2I4YMo0m62VU0TgxgIvDwvkX24OOvswefuJCP/mo9j/d3AwDluiBnn1YXFBJgGfrQ44CJQDIAjAGybRBNswERuGxekHb6P6mqaOOM4ozNsLXKH67VC7C13hOwf65rhDGxIwy0IxtbJ/wIBcrOALUthNn3cPW7uDwE6DIozLt2HheBdOvzYtaaB8SsNZvE/Nc8SE1z+lzAefyFocklgJtixfbgEyfph/7umzy483UwlVpfrQKG0SRaF0J0LPeuVABCgaMiTO8vnPETryBDiI6TQVVFry7yn/jFtXRxIuHaRNKt8wZWO2PXlYhx8llo132oDCjldQAZQs7+A5h9D4ErRR8ADji5Osi5hBSx62p0udrliM4V3w4u/MLfUNOcQlLzfLxMMLkxgBdBxKxX7VNnf/SjYvEF30GqJXIVtykDSAYEIFO1G5+01ngcSRFHNdI3MWzfNvDIAd/vV1y/b+IX2epf1y6usPpFjJ98r/V6v5s5BqNBqTxEps13OYHbz9UDuHMz5ZpABF+FBGFIpRkkQEG6X6284prg3E9/lpq6i8fb+EBDxADEkCkt5p75hJh75jvs3of+Sf/i25/k3i1vhPB9OYla6wOBrATHZd8HHzO+JwE70gNhDSg3AzC27jMJatItV72A9wQ+z8/W+pZf93rVW2iXFk61uNadzFAzFYAUSGXBNnbnCPj9AIiAURmVrMtazFl3vVzzjmvFnHX7js99fnE0AAE8nCZOYuH6x8KF699ke35+hn7861/hgefOBAmG1cLFAAQmb5DE9TN86/KeQAawhSMgq0HZGd5l139XXfKmngDe2NXMXjU2qH8tBmVmOK2fjRsqJhJwUnqmQnBccgEgkAhUluMyic7l31Nnvn+j6FrlDM/GzVKepFKxxiEAmPxQCSiPpHns0BLoUrMzsKWaoQCQrsm01XRtXfaOCFApl6xhA0q3oWokV3OAiXEA+wjfHEMIU+chXCwg8t2gVIv7LNHEY/lyMgBAXACkrI5uARCpAFwpdGGsdxE6luyDTLlcAScsOv5oHAKQNIjHYZ75wRVm+48/wUN71iDIuJua9M2JBW1d668+1g/LXERNMgXEZbAdqJVzVYPeGgEYE4mQuPtqn++HgKJ5nmv5VoOQ1B36jRgsDSjMgSujrvXLbPW4TEQU5IDS4Ib43k9voLaFd8pVb/2cXHHZwy9ZmvZ7xiQTwOnlXB7O2F33XGJ+9cOruf+51yDIAtn2GMwSbIUbp1tQ/eQacYzUS/5vr9JVx+ki5dx6ZbQWnCXf/Ws8QXW8zxaA0/ZF20JQqtWRQQaoZf1q+xBlXLlYaQCAl5yTSiSh4OVrQ815i9LIRfqn115kd955k1x5+ZfEgvU/c4Hv8Q0CJ3cUwEwAgUf2zzLbbnwrjx5YBiFdtk+XA4CFCwC1M6KPxNmaurG9D/6El2XrdX+hXGwQeE3f1JV3m2Me6yJ/5qTYM/E8AIXN7lgi8FudRCwDR7p0E5gItlSXzGJ2GQoZJqMFCV0OWI+DVAZ89Nl1ZtuNb+HySNbflOPqDhpHCALAxb60PfjEa/jw1gvswccv5MroCoBDUhlBua66Tzre2rEDrnIXcMYDXBaQaoYilXL9sol891Cvz3PVQC4mTKbwcV1QyAAbyO5TQZlW3z3A71PnOXTZ1RPkuxA/8LlqOpm915KtiwERWC4PFynf9TjNWnOH6F57j5h92i+Qaj4et/dF0RgEYHalO8eUTptnbz5bP/LFmyjIt1OmAyBBtfG0hB3eA8RjriqHyBmcZG3WTyIJEzndQKVAIgQnBaH1SZvE6PXVPclzXXEEaJnnZwclMVvSwgGMD4LmvQZ2aCfME/8KpJq8JzEgmbaiZRFxZehpde6n3iDmr++ZeP1WgshMhhLYGMkgIgbIuBm7cQoA7KEnTjFb//06ItXuRJWIqqMAHTmJldlp9d7QtWRQnfFF4D4XF8HlIdjxo27fuOjkXvhWz7UhX63evy6l6wnkysuTbiB0j8apgNQ0E3zwSVcylngSa4EgK5BqYQRNq83W73yF+59tBwCYyBUxkJgU4wONQgAAPqoSEEHFbP/xRfEt730QpYE1cMvwENsYAIP1uNfXS64GQLi5AJT0/9URgdtI+Nx8tZ+PgGgMXB4AlwbApUGgPOIKQPzU7wnBnTckRwVneJXym+/T2QKmAupYAi72wR7dBlap2pAV7IJHYkHpVnCh783xbX99v+19cglkGKG6LN3koEEI4HpvkDD64X94n37wc7dQdkYrgnwEUgYyMLAWHBWBykhNoAFcIKVLqBqfpO8OkloAAT/vv24/3zLjIhAXQOkWiPZFgAzA4wNOPzARqmIQ2Jd7US2aT4aLJEAqDdG2GHbnXWBTrukDbF1+ItMBIqVBwlC+u0L52avjOz/8kNn+o9e7JW4mrzC0AXQABhgC0SjF9336k+a5W/6WcjOB8jCYbQihQDIDpJrB8ZgfbpHrh5lBQc65ZxO5UnHUjE/CB4TGl5EBzjC+8pdyMyHaF4NyXYAKoVoXwPpWbEf2u8+qtDNkVHC1g2EOgB+FKOk8UftJ4NGD0PsfdkNN67/LalBuJhA2AeN9yi1EESuQBIVNs/Tdn7jVHt3+zmD9x25wibHjXyDaAAQggGAhAyFXvOU/5Kv+5AcAt3LhcAuP9HRSkG23Q3sW2z33/RWlW4krw8QskEzjIhKgMA8bjUAEOV/HV1f3DwKbqDpWBwBKt4PaFrrUsUq54zCq2UTRsRQ8dgjm0JOwAzscEYQE6xIo1wlQGUjmAIgAlJ2B6LF/diRUmZrWQMSiZT7Z4d0jYtkl1wqVGWZdHhAdSweQahqCNUUQjQOCXRx0/NEABPBQGSsWrN/xYm/ZW977v0mlBFTGICpIl3Rx2j/D+DRrGVwZBOW6vRCUqIIWrMedhp9qgWie76qBgoz3FABJVRvfAy6gnLEMqmsVeHgvzP5HYA4+Ci4cgehc6aeBAUAEys2E3rkJ9uivgCBXS0PrCkTTXBK5mcZGxTxXxgbV6X/5L7//G/m7oXEIAABsvYrDBGsUhKrE9336f9rDW99FLQsNmCWlmsHFI8fu5zJwcQEoD4Kys9xhSIKNiw9E62KI5rmuGkj4uCuZDp4YXwa1CB9eUuo6BXL26TC9W8BWg0TgW7kBghxs7xbo5293ryWun51nEJ3LAauFaFsE8/ymr+lsR49a++5bYaIQQvlpxcSTuWZAY+gAx8IaCSGNefp7F+qffvF26lgWwxrpjKUkjx8lLg/5gCw5fz82j8eBdDtEvhsQAdiUQVK5Gj9y9XzO0KErJvF/17YAVB3qqWpXQqlml+WrjLo6g7gEc3Qb9LYbfXzhvA1AgC5BzvoDUOdKcFyAYMRMgmzvk4Pq/L8/Xcw+rWey+vxj0XgE8DfG9j29ML7pz7dS0+wWGA3nGLRrwTIFO9oD6NKLkABuWBY2g5rnuepe4Y1YZ2TIoPZcBU4tlIFLICWvi8AXnCZ5hQDQJXDhCMzhp6B3bUJ1LiE8CXQZonkOxLyzoLQLNq0pwcKVl9mh3Y8Fb/n3synboR1hJnfFkAYjABMA4tEDTdH3r7gdqbaVlGndS01zD0GIPWLWqc/bXZtXcWnoLyjdZm3/9mQl8GOOQ0imiovmuaDMDK8CCq/d+2VgVEIIb3QVegKk6jxEys36AQNjveDBXTCHnoDp3YLqiMP6WkUTgYIs1Io3g4f386NyJ9kgc2SdXH1DKNIrdLFvFkZ6FiPd9K3wsm//DWTIk6UAJmisGAAAQBYkRXDBF95PM5buhEqPUqrFvWU1zPYf/ZRSzaAgy6JtMezgDsebY0lAEjBl2KHdoPIwqGmOqxWsuvn6GcACEMLLyIEjgsoCQdrJQsUj4IFd4P7nYPqeARd6a4tNWF9xZCNABlBLLgKn8lC52ZQa32NvbS+2H8xVht689FPnpaIotJWhFoweWMDWCFIiqnmvyUGDeYBfA6tDkLT6sesus7/60Q+pc5UlGwkOMkBpCLZ/m7uP9cvDVOFFGRm4FT5yXa56N9PiCjWDrBvrq5TrEoRC4sq5PAQePQQe2Qc70gNbPOxcvqyrBSByOoNKQy2/1B0/KgAiBX1kK37Q1sM9qZgW5lZ/7S2rv/xXKUod//v3EmhQArCbKwfBIAaYiHWJox9c8SilO05HkLWSpJQsYJWCqYzA9j3jWqEIMYEEfqm3KhJhKcyDwiYgzAJB1i8yAZdV1CWgMupWE6uMOBmY/dIzyTEBn2QqgjIdUMveCJmdCRgDRgzNMYLiGLYU7sf985q0LQ2q2c2v+ufLV157VUrmAgY0NcCKYQ1KgDqwUUxSm2dvusj+7Lo7wvZlFroiytKiJwc0D/ehK78chjXM4V9Up4g7tdAeO0bw/yfFJD5fUJ1B7ItEq8kgW/tcIv8CXurVQFyCmLEMcsXlEDrG7t7bgdaFWIAuKJkHghTKvY/iu+37UMy16qjcr2a3nPpPV6y49v0plZfM1k42CRqcAAxmq8gYje+/7YGKFOf0N+XM/jzkoXyIMcW4ZEcfuppWQAchCAQz8KxT72zsJ4VQrcVWD1tXD1B94AncqP1RVz9Awg354nEgzEMueh3U/PWwMJClcTx95Gbc1VVGd0VhQZTFSZiF7koaT5Uew+Z5GWSs1MV4SM1pIBI0YBBYA7MJiFTct/8n5+9rKpxzaPYiOxxCWh+0dYwMos2mYVToCjhVCuHsM2FaF8H0PeP1fO2VQi8fJ8bkaoWo/7a6CbpJIWd9uZmNwXEJpLKQ88+COukCUPM82PIAoDWQzmMuzUCzPoSBbApH0uPYop/DTBVgrhHIxxZlCZUNWvWBkaeuumH7h3HFii9MOgka1gNYNkqQ1DsHHlr78P5v3WGDbKe0mqRlIjYoBwonH+rFOWMzELXNQcCuDrB/fDdaghlQbYthRw/A9D8HO7zXJXMmlI+9SAIu8QRwVUBuRdDIpQKyMyBmnw45/2yIzpUo778PpWgIbZ2vBiOGZQP0/gI3yMfR39QMZTUMGBUyYAChsbC+ixGk9Hg8pOa2nPpPk02ChvQAifGfH3hwzYN7v35nINNdMi4zE/uSIYblGDOKFSDVgtAKjJsR/Cw/gBEcwWXjeTAbUPM8qPbFgC7DDu+HHdoFLhx2al5U9BU7te6A/KObWJoFNc+F6FgCMXsdxMxTQEHeiU8mRhyP4Q5swfLBIZyaehVkfg44Owudo8CRFkD5wuWUJbCrCnYOhgiWtcoEbfrAyFNX/XD7h/HWSSRBwxEgMf6OgQfWPLD3a5sCke5iWGPBsr4vl1GEWZECUgrbaR+ebClgIBtiUUVCUABLAmwjSGRgykNA6zwEC8526/1FBdhoDBSPOzKIRAsIQE3djFQzKD+LKNcJynVB9z7pVgZLt4GjUQiZRSBzYNmEx9Jj2Fe8G6cPLMa8cAXmmGY84yeIEAALAk9I9JG/Tq0yvjv44fYP4a0rvjgpJGgoAlSN33/fmvv3/vOmQGa6AGusZVnrqxmxEOgYLyEmg9taDmBvJkbAEqFlPztHwBKgZBaj5YO4v3w3zotOR65pHliFoNwCKGuB8rBB1yqZzOZBmGd74HGi7rWADF0ewVoMDj2DHYWteC39iasLNBWQkAisRZpSGMgSbo93YGXlCBZVYmQ1I5aBqwckQvKrcuT0xOpEZcu6GhP8cNuH8NaVx58EDVIRVDP+c/33rrl3z1c2SRF0MRtjWEsLA8sGhg2Mn8ZdDAPcvjCHvVmDFBOE1TAwUJbBQkBRGiOlHtyMn2N3c4CyHgMgYGFBrLCr725+cs+3JMoFmLgI1hW2Y4fIHHh0o91z/w4EWYYMLUihzBXs6uywD/ffDDO0CwibQTKAsm7RycACocrj6VwJ93YLSBH4gWVieI/qolX11+09wejWq36w7UPXVXTBEAnBLjP6e0dDECAx/rP996y5Z/c/blIi1cXMzvjsjO9CKAOGAbFGSQGRFAiNI4UFw1o3y4dEFoOlvfixegKDIYFIYcAOea0+x/HYXnunfYxGO+Z8B0N7yhTmgTBHfHT7UbXufdcg0/ZBPrqdKNvFMDEPlXuRopQ42Jy3Dx69CfHAdqiwGS5uENWhZoYlymEasRDJz5XWViSdAJrwaNn4mOD4k2DSCVAz/t1r7tn9xU2BTPk+P5Yuw+aMzn7SJrOBrZu8afwMX2YLQxY5Qxiq7MePwqcwHAiEhhFLwiCNgU3MQluzaeA/xKJ5b974utOue4ce3fsLAhiVMSAqfZea55BcfMHt9vDWb4NZcmWE57ae9pfZVOcWpWPR29qmHxi5C/HgDqSgXHCXrEkIguSk5VOdmekFryUxQu0+aJUNW487CSaVAInxt/dvXrN51xc2KZHuYmZjWUtmhvVbYmDLtvrcMnuv4Fq9BUMag4GMxE9aD2FMMkKGc/kk0C9KTLExzxy9Vcn55268ZMEHrkEqT8h1Xc9jh4gLhyPqOuVbADNkKOX8sz/Auzfv52hYdHSsvfGc+X/2h4HKPh8Yo45kU/reVA/iMOuSudVlamrzFclPVTvW5SeGr3+VqvcjCQy3XvWDbVcfFxJMGgEsGylI6u1HN6/ZvPPaO120D23ZCGd3nrgh2SzXTO4eLVtYWEhrcCAXYiSdQmgtjP8EWeZCKjT9h25The4VGy86eeM1bE0AMMS8s27ko9uLKI/8TMxc/Su4RfxAnStHAXGVffYnR2zzzPHWoOPwuYvetyFQ2R1SR2q4pVMXUhlI5omGr2vtCSY+J0sQFqDqRrVHtqxlNmyLD4w+ddUPt139VU8C+n2RYFIIwGyFIGm2H9182l27rr1biHCmYQ1tK0pzTJojMXGLheZYGI6EZUOJR0i8Qo0MgGQLYQ1sbeYvS5A5ysNqaN4pnzxzyV9fQ2wVCaHBLETn8gIP7/sem/j/IsyT+1FpacBM4qTzbkG28y9EprPCYNGambv/3EXv2xDI7A7WZaVIaKDO8PVrEh4bAlJy7UYY1sKyEdY/mtojWTYUm1KQknn0jG59/w3bPvy1yIxbt/zdyz9v8LgrgcxWEAn+5ZFbT7lj5+duCUW2Oxu0FgUpFyq56tgiasq8BqPE7uJtbEvzLOsmr9tSfeYvuRauDRmZIE0xGlCnz7ly43knffAaYqtApH1yh0ACtn97F6VbS5TvHsXERRvrtWIwW0kkzFCpZ949u796d0WPLSWS2nCkLBswa7Afrbi4xZM06abYIpCpvQQxzszEYMngrD9jC9gUM6q/alHRBRqrHGmd33ratW9b9eWNGdVqiWBfzgKSSZCC3Q3uLWzr0raSJ4hKa3p2JZAZ7cU4BuB//6WqpUTMVhEJfduOz9y4d/jxy1IqZ9haWWfseiqAGSxImGI0qE6fc+XGDSd98Bp3DG/83x5JsZ87sifBYKln3j27vnx3WY8uFUJqYxMSGCTD1oQE1t1jrpgiXbrsf716dtOqxxlWkosg6n5+BAqA8uudYqh8IB3bsoxNKdudX7krE7SYYzJW/2lMghDkWld3fmUfgL7fYUcNwApIm4wIbNUuL/ACLEiaQjyo1s152282PluX8Xnx2vwJhZtEwjAb2Z6Z13Pe4r/ZsHnnl+4u69GlUkhtYVR9KQKBvCkTcjJCleOUytu6Y//an5GblV/+Iq++vL3ApAWBDEvMVjBb4dz7S2/MhgAmCxsy6v75xRyqG8BE0hTiAbVu9ts2bjjp6t/c8ul3m5hBJD0J5vecv+SDG0KV3aFtpARJPTEQrN/Jp5rY5Pz1iF97rdVrrt6jCV3dy4lJIwBBMJGwRMK6NMlvtzFzPjF2MjXbbY4OgoQpxAPq9NlX/nbG//89/yoJFvRcuOQjG0KZ3aFNpIR48R+GnPjtL32Nyf0gqt4jfrnPP8GkC0G/O5IfaEni/mSb2PLP/z0aP8FEEnzMeQITKSJZJcFEMVhAkGqo/PsJRwAXONWHey7kIxKmGA+odbOv3Hj+SR/6vRu/ej6eBB3ZBT0XL9noPUF5AgmSM2fWGCn3hi9+pMnBCUIABhEhNiUMVnrSQqiqHASwC/iiAbWu+/gaP0GNBAt7Ll76yQ0plduh7bEkILZsMFLpzbsragxHcIIQAHA3UKOix0JKCjjBTBCmUBlQ62a/feP5i4+/8atnR9JYNnJGdlHPJUv/x4ZQ5uo8Qc3Y013AfwoEQFTjPwFpitGgOmPO2zdeMInGTyCqJDip5w3L/nZDKHM74hd0B3aaAC8HJElbjAfVGXOunNSWfywSEnRmF/e8cfln67qD6uggO6kneAxOFAIkCqEAOC1I2kI8EJ4x+8qN5y/+cMMYP0E9Cd607HNJd5AiNxs44z/WECd7ohCgCkFKFuMBccact3+sEY2foEqC3OKey5Z/fkNK5bdrWxZEsjLZ51aPE4oARIRiNNC6bvbbP3v+SR/6+0Y1foIaCZb0XLb88+eHMtcTm1L7ZJ9XPRp2XsBEuARSIerPb+m98aJzFvy3Gxgsya3WNNkn9xvhax9Mz+jWVYPj+xacOuvS231WdHqBiN8FzNb/LNzENO2JgLqZAQ2FE6wLEGCwX/D3xAKB2J97Q7msE8oDTOPlxwnlAabx8mOaAFMc0wSY4pgmwBTHNAGmOKYJMMUxTYApjmkCTHFME2CKY5oAUxzTBJjimCbAFMc0AaY4pgkwxTFNgCmOaQJMcUwTYIpjmgBTHP8Pi5CmymmL/LkAAAAASUVORK5CYII=")

		jGenerator.writeEndObject();
		i++;
    }
    
    if(i!=0){
		jGenerator.writeEndArray();
		jGenerator.writeNumberField("max", prev_score) 
		jGenerator.writeEndObject();
	}
	jGenerator.writeEndArray();
	

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}
*/
def json = null
OutputStream out = null;
JsonFactory jfactory = null;
JsonGenerator jGenerator = null;

try {
	out = new ByteArrayOutputStream()
	jfactory = new JsonFactory();
	jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);

	String str = "";
	String img64str = "null";
	String area = "";
	Integer areaId = null;
	String prev_area="";
	Double prev_score=0.0;
	def rs = null;
	jGenerator.writeStartObject();
	jGenerator.writeFieldName("badges_list");
    jGenerator.writeStartArray();
	int i = 0;
	badge_ids.eachWithIndex{obj, idx ->
		area = badge_area_names[idx]
		areaId = badge_area_ids[idx]
		
		if(!area.equals(prev_area)){
			if(idx!=0){
				jGenerator.writeEndArray();
				jGenerator.writeNumberField("max", prev_score) 
				jGenerator.writeEndObject();
			}

			jGenerator.writeStartObject();
			jGenerator.writeStringField("area", area) 
			jGenerator.writeNumberField("area_id", areaId)
			prev_area = area;
			
			jGenerator.writeFieldName("badges");
			jGenerator.writeStartArray();
		}
		
		jGenerator.writeStartObject();
		jGenerator.writeNumberField("id", badge_ids[idx])
		jGenerator.writeStringField("title", badge_titles[idx])
		prev_score = badge_scores[idx]
		jGenerator.writeNumberField("score", prev_score)
		def bytes = null
		def inStream = null
		
		try {

		    inStream = badge_images[idx].openFileInputStream()
		    bytes = org.apache.commons.io.IOUtils.toByteArray(inStream)
		} finally {
		    org.apache.commons.io.IOUtils.closeQuietly(inStream)
		}

		def base64 = new org.apache.commons.codec.binary.Base64()
		def base64String = new String(base64.encode(bytes), "US-ASCII") 
	    jGenerator.writeStringField("icon", base64String);
	
		jGenerator.writeEndObject();
		i++;
    }
    
    if(i!=0){
		jGenerator.writeEndArray();
		jGenerator.writeNumberField("max", prev_score) 
		jGenerator.writeEndObject();
	}
	jGenerator.writeEndArray();
	

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}
//second part

try {

	jGenerator.writeFieldName("data");
	jGenerator.writeStartArray();
	def rs = null;
    areasId.eachWithIndex{obj, idx ->
	    jGenerator.writeStartObject();
		jGenerator.writeNumberField("id", areasId[idx])
		jGenerator.writeStringField("name", areasName[idx])
		def query = null
		query = "select sum(t.score) from action_type as t join action_instance as i on i.action_type_oid = t.oid where rank_oid = '"+user[0]+"' and t.thematic_area_oid = '"+areasId[idx]+"' group by i.rank_oid"
		def res = null
		res = session.createSQLQuery(query).list()
		Iterator iter = res.iterator();
		if(iter.hasNext()){
			rs = iter.next();
			jGenerator.writeNumberField("score", rs)
		}
		else{
			jGenerator.writeNumberField("score", 0)
		}
		jGenerator.writeEndObject();
	}
	jGenerator.writeEndArray();
	jGenerator.writeEndObject();
	jGenerator.close();
	
 	json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)
return ["json": json]